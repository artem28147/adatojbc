#include "write_to_dot.h"
#include <string.h>
FILE* file;



int varnum = 0;
int argumentsnum = 0;
int casenum = 0;
int whennum = 0;
int whenlistnum = 0;
int whilenum = 0;
int fornum = 0;
int varlistnum = 0;
int elsiflistnum = 0;

void write_tree(NStatementList*Root)
{
	char str[100];
	strcpy(str,"digraph {\n");
	char* end = "}";
	file = fopen("tree.gv", "wt");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_stmt_list(Root);
	fwrite(end, sizeof(char)* strlen(end),1, file);
	fclose(file);
}

char* write_type(enum NVarType type)
{
	char result[100];
	strcpy(result,"");
	if (type == Integer)
	{
		strcat (result, "int");
	}
	else if (type == String)
	{
		strcat (result,"string");
	}
	else if (type == Root_Integer)
	{
		strcat (result,"root_integer");
	}
	else if (type == Boolean)
	{
		strcat (result,"boolean");
	}
	else if (type == Array)
	{
		strcat (result,"array");
	}
	else if (type == Character)
	{
		strcat (result, "char");
	}
	else if (type == ENum)
	{
		strcat (result,"int");
	}
	else if (type == Float)
	{
		strcat (result,"float");
	}
	return result;
}

int arrvarnum = 1;
int stmtlistnum = 0;
int exprlistnum = 0;
int exprnum = 0;
int stmtnum = 0;
int ifnum = 0;
int elsifnum=0;


void write_var(struct NVariable*var)
{
	if(var!=NULL)
	{	varnum++;
	char str1[100];
	char buffer1[10];
	strcpy(str1, "\"Var_List(");
	strcat(str1, itoa(varlistnum, buffer1, 10));
	strcat(str1, ")\"->");
	fwrite(str1, sizeof(char)* strlen(str1),1, file);
	char str[100];
	char name[50];
	strcpy(str,"");
	strcpy(name, "\"Variable(");
	char buffer[10];
	strcat(name, var->var_name);
	strcat(name,",");
	strcat(name, write_type(var->type));
	strcat(name,",");
	strcat(name,itoa(varnum, buffer1, 10));
	strcat(name, ")\"");
	fwrite(name, sizeof(char)* strlen(name),1, file);
	if(var->value != NULL)
	{
		strcpy(str, "->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_expr(var->value,"no");
	}

	strcpy(str,";\n");
	if(var->list!=NULL)
	{
		strcat(str, "\"Variable(");
		char buffer[10];
		strcat(str, var->var_name);
		strcat(str,",");
		strcat(str, write_type(var->type));
		strcat(str,",");
		strcat(str,itoa(varnum, buffer1, 10));
		strcat(str, ")\"");
		strcat(str, "->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_names(var->list);
	}
	write_var(var->next);
	}
}

int arrvarlistnum = 0;
void write_arr_var(struct NArrayVar*var)
{
	if(var!=NULL)
	{
		char str[100];
		char name[50];
		strcpy(str,"");
		strcpy(name, "\"Array_var(");
		char buffer[10];
		strcat(name, var->arr_name);
		strcat(name,",");
		strcat(name, write_type(var->elem_type));
		strcat(name, ")\"");
		fwrite(name, sizeof(char)* strlen(name),1, file);
		if(var->expression != NULL)
		{
			strcpy(str, "->");
			fwrite(str, sizeof(char)* strlen(str),1, file);
			write_expr_list(var->expression);
		}
		strcpy(str,";\n");
		fwrite(str, sizeof(char)* strlen(str),1, file);
	}
}

void write_array_var(struct NArrayVar*arr)
{
	char str[100];
	char name[50];
	strcpy(str,"");
	strcpy(name, "\"Array_variable(");
	char buffer[10];
	arrvarnum++;
	strcat(name, itoa(arrvarnum, buffer, 10));
	strcat(name, ")\"-> ");
	strcat(str, name);
	strcat(str,"name_is");
	strcat(str, arr->arr_name);
	strcat(str,";\n");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	strcpy(str,name);
	strcat(str ,"elements_type_is");
	strcat(str, write_type(arr->elem_type));
	strcat(str,";\n");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	if(arr->expression != NULL)
	{
		strcpy(str, name);
		strcat(str, "values(");
		strcat(str, itoa(arrvarnum, buffer, 10));
		strcat(str, ")->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_expr_list(arr->expression);
	}
}

void write_expr_list(struct NExprList*list)
{
	exprlistnum++;

	if(list!=NULL)
		write_expr(list->first,NULL);
}

void write_exception(struct NException * exc)
{
	write_exc_body(exc->list->first);
}

void write_exc_body(struct NExcBody * body)
{

}

void write_if(struct NIfStruct* if_)
{
	char str[100];
	char name[50];
	strcpy(name, "\"If_(");
	char buffer[10];
	ifnum++;
	int localexprnum = ifnum;
	strcat(name, itoa(localexprnum, buffer, 10));
	strcat(name, ")\"->");
	strcpy(str, name);
	strcat(str, "\"condition-if_(");
	strcat(str, itoa(localexprnum, buffer, 10));
	strcat(str, ")\"->");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_expr(if_->ifExpr,NULL);
	strcpy(str, name);
	strcat(str, "\"if-stmt(");
	strcat(str, itoa(localexprnum, buffer, 10));
	strcat(str, ")\"->");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_stmt_list(if_->ifStmt);
	strcpy(str, name);
	strcat(str, "\"elsif(");
	strcat(str, itoa(localexprnum, buffer, 10));
	strcat(str, ")\"->");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_elsif_list(if_->elsifList);
	strcpy(str, name);
	strcat(str, "\"else(");
	strcat(str, itoa(localexprnum, buffer, 10));
	strcat(str, ")\"->");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_stmt_list(if_->elseStmt);
}

void write_elsif_list(struct NElsifList* elsif)
{
	if(elsif!=NULL)
	{
	elsiflistnum++;

	write_elsif(elsif->first);
	}
}

void write_elsif(struct NElsif*elsif)
{
	if(elsif!=NULL)
	{
		char str1[100];
		char name1[50];
		strcpy(str1, "\"Elsif_List(");
		char buffer[10];
		strcat(str1, itoa(elsiflistnum, buffer, 10));
		strcat(str1, ")\"->");
		fwrite(str1, sizeof(char)* strlen(str1),1, file);
		char str[100];
		char name[50];
		strcpy(name, "\"Elsif(");
		elsifnum++;
		int localexprnum = elsifnum;
		strcat(name, itoa(localexprnum, buffer, 10));
		strcat(name, ")\"->");
		strcpy(str, name);
		strcat(str, "\"condition-elsif(");
		strcat(str, itoa(localexprnum, buffer, 10));
		strcat(str, ")\"->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_expr(elsif->elsifExpr,NULL);
		strcpy(str, name);
		strcat(str, "\"elsif-stmt(");
		strcat(str, itoa(localexprnum, buffer, 10));
		strcat(str, ")\"->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_stmt_list(elsif->body);
		write_elsif(elsif->next);
	}
}

void write_when(struct NWhenStruct* when_)
{
	if(when_!=NULL)
	{
		char str[100];
		char name[50];
		char buffer[10];
		strcpy(str,"");
		if(when_->whenExpr!=NULL)
		{

			strcpy(str, "\"When_List(");
			strcat(str, itoa(whenlistnum, buffer, 10));
			strcat(str, ")\"->");
			fwrite(str, sizeof(char)* strlen(str),1, file);
			write_expr(when_->whenExpr->first,"Variable");
		}
		else 
		{
			strcpy(name, "When OTHERS");
		}
		whennum++;
		int localnum = whennum;
		strcpy(str, "\"When_List(");
		strcat(str, itoa(whenlistnum, buffer, 10));
		strcat(str, ")\"->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_stmt_list(when_->body);
		write_when(when_->next);
	}

}

void write_when_list(struct NWhenList*list)
{
	whenlistnum++;

	write_when(list->first);
}

void write_case(struct NCaseStruct* case_)
{
	char str[100];
	char name[50];
	casenum++;
	char buffer[10];
	strcpy(name, "\"Case(");
	strcat(name, itoa(casenum, buffer, 10));
	strcat(name, ")\"->");
	strcpy(str, name);
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_when_list(case_->body);
}


void write_func_proc(struct NFuncProc* func_proc)
{
	char str[100];
	char str1[100];
	char buffer[10];
	if(!func_proc->isPrototype)
	{
		if(func_proc->localVars!=NULL)
		{
			strcpy(str, "\"Func/proc(");
			strcat(str, func_proc->name);
			strcat(str, ")\"");
			strcat(str, "->");
			fwrite(str, sizeof(char)* strlen(str),1, file);
			write_stmt_list(func_proc->localVars);
		}
		if(func_proc->returnValue!=NULL)
		{
			strcpy(str, "\"Func/proc(");
			strcat(str, func_proc->name);
			strcat(str, ")\"");
			strcat(str, "->");
			fwrite(str, sizeof(char)* strlen(str),1, file);
			write_expr(func_proc->returnValue,NULL);
		}
	}
	if (func_proc->arguments!= NULL)
	{	
		strcpy(str, "\"Func/proc(");
		strcat(str, func_proc->name);
		strcat(str, ")\"");
		strcat(str, "->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_args(func_proc->arguments);
	}
	strcpy(str1, str);
	if (func_proc->body != NULL)
	{
		strcpy(str, "\"Func/proc(");
		strcat(str, func_proc->name);
		strcat(str, ")\"");
		strcat(str, "->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_stmt_list(func_proc->body);
	}
}

int argsnum = 0;
void write_args(struct NFuncArgs*args)
{
	char str[100];
	char buffer[10];
	strcpy(str, "\"Func_args(");
	argsnum++;
	strcat(str, itoa(argsnum, buffer, 10));
	strcat(str, ")\"->");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_varlist_assign(args->first);
}

void write_varlist(struct NVariableList*list)
{
	varlistnum++;

	write_var(list->first);
}

int varlistassnum =  0;
void write_varlist_assign(struct NVarListAssign*list)
{
	char str[100];
	char buffer[10];
	strcpy(str, "\"Var_List_assign(");
	varlistassnum++;
	strcat(str, itoa(varlistassnum, buffer, 10));
	strcat(str, ")\"->");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_varlist(list->variables);
}


int arrassignnum = 0;
void write_arr_assign(struct NArrayAssign*assign)
{
	char str[100];
	char buffer[10];
	strcpy(str, "\"Arr_List_assign (");
	arrassignnum++;
	strcat(str, itoa(arrassignnum, buffer, 10));
	strcat(str, ")\"->");
	varnum++;
	fwrite(str, sizeof(char)* strlen(str),1, file);
	strcpy(str, "\"left_array_(");
	strcat(str, itoa(varnum, buffer, 10));
	strcat(str, ")\"->");
	strcat(str,"\"");
	strcat(str, assign->left);
	strcat(str, "(");
	strcat(str, itoa(varnum, buffer, 10));
	strcat(str, ")");
	strcat(str,"\"");
	strcat(str,";\n");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	strcpy(str, "\"Arr_List_assign (");
	strcat(str, itoa(arrassignnum, buffer, 10));
	strcat(str, ")\"->");
	varnum++;
	fwrite(str, sizeof(char)* strlen(str),1, file);
	strcpy(str,"\"Element_[");
	strcat(str, itoa(assign->from_left, buffer, 10));
	if(assign->from_left == assign->to_left)
	{
		strcat(str,"]\"");
	}
	else
	{
		strcat(str,"-");
		strcat(str, itoa(assign->to_left, buffer, 10));
		strcat(str,"]\"");
	}
	fwrite(str, sizeof(char)* strlen(str),1, file);
	if(assign->right!=NULL)
	{
		strcpy(str, "\"Arr_List_assign (");
		strcat(str, itoa(arrassignnum, buffer, 10));
		strcat(str, ")\"->");
		varnum++;
		fwrite(str, sizeof(char)* strlen(str),1, file);
		strcpy(str, "\"right_array_(");
		strcat(str, itoa(varnum, buffer, 10));
		strcat(str, ")\"->");
		strcat(str,"\"");
		strcat(str, assign->right);
		strcat(str, "(");
		strcat(str, itoa(varnum, buffer, 10));
		strcat(str, ")");
		strcat(str,"\"");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		strcpy(str, "\"Arr_List_assign (");
		strcat(str, itoa(arrassignnum, buffer, 10));
		strcat(str, ")\"->");
		varnum++;
		fwrite(str, sizeof(char)* strlen(str),1, file);
		strcpy(str,"\"Element_[");
		strcat(str, itoa(assign->from_right, buffer, 10));
		if(assign->from_right == assign->to_right)
		{
			strcat(str,"]\"");
		}
		else
		{
			strcat(str,"-");
			strcat(str, itoa(assign->to_right, buffer, 10));
			strcat(str,"]\"");
		}
		fwrite(str, sizeof(char)* strlen(str),1, file);
	}
	if(assign->expr!=NULL)
	{
		strcpy(str, "\"Arr_List_assign (");
		strcat(str, itoa(arrassignnum, buffer, 10));
		strcat(str, ")\"->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		strcpy(str, "\"Expression(");
		strcat(str, itoa(exprnum, buffer, 10));
		strcat(str, ")\"->");
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_expr_list(assign->expr);
	}
	strcpy(str,";\n");
	fwrite(str, sizeof(char)* strlen(str),1, file);
}

int funccallnum = 0;
void write_func_call(struct NFuncCall*call)
{
	funccallnum++;
	char str[100];
	strcpy(str, "\"Procedure/function_call(");
	char buffer[10];
	strcat(str, call->func);
	strcat(str, ",");
	strcat(str, itoa(funccallnum, buffer, 10));
	strcat(str, ")\"->");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_expr_list(call->arguments);
}

void write_expr(struct NExpression*expr, char*nameExprList)
{
	if(expr!=NULL)
	{
		char str1[100];
		char name1[50];
		char buffer[10];
		if(nameExprList==NULL)
		{
			strcpy(str1, "\"Expr_List(");
			strcat(str1, itoa(exprlistnum, buffer, 10));
			strcat(str1, ")\"->");
			fwrite(str1, sizeof(char)* strlen(str1),1, file);
		}
		char str[100];
		char name[50];
		strcpy(name,"");
		strcpy(str,"");
		strcpy(buffer,"");
		strcat(str, "\"Expr(");
		exprnum++;
		strcat(str, itoa(exprnum, buffer, 10));
		strcat(str, ")\"->");
		switch(expr->type)
		{

		case not:
			{
				strcat(name,"\"!(");
				strcat(name, itoa(exprnum, buffer, 10));
				strcat(str,name);
				strcat(str, ")\";\n");
				strcat(name, ")->");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				write_expr(expr->right, name);
				break;
			}

		case uminus:
			{
				strcat(name,"\"-uminus(");
				strcat(name, itoa(exprnum, buffer, 10));
				strcat(str,name);
				strcat(str, ")\"->");
				strcat(name, ")->");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				write_expr(expr->left, name);
				break;
			}
		case c_i:
			{
				strcat(str,"\"value(");
				strcat(str,itoa(expr->const_int, buffer, 10));
				strcat(str,"),");
				strcat(str,itoa(exprnum,buffer,10));
				strcat(str,"\"");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				break;
			}
		case c_b:
			{
				strcat(str,"\"value(");
				strcat(str,itoa(expr->const_bool, buffer, 10));
				strcat(str,"),");
				strcat(str,itoa(exprnum,buffer,10));
				strcat(str,"\"");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				break;
			}
		case c_c:
			{
				str[strlen(str)-1]=expr->const_char;
				strcat(str,"\"value(");
				strcat(str,"),");
				strcat(str,itoa(exprnum,buffer,10));
				strcat(str,"\"");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				break;
			}
		case c_d:
			{
				strcpy(buffer,"");
				sprintf(buffer, "%f", expr->const_double);
				strcat(str,"\"value(");
				strcat(str,buffer);
				strcat(str,"),");
				strcat(str,itoa(exprnum,buffer,10));
				strcat(str,"\"");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				break;
			}
		case c_s:
			{
				strcat(str,"\"value(");
				strcat(str,expr->const_string);
				strcat(str,"),");
				strcat(str,itoa(exprnum,buffer,10));
				strcat(str,"\"");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				break;
			}
		case empty:
			{
				strcat(str, "\"NULL\"");
				strcat(str,",");
				strcat(str,itoa(exprnum,buffer,10));
				fwrite(str, sizeof(char)* strlen(str),1, file);
				break;
			}
		case name_list:
			{
				write_names(expr->list);
				break;
			}
		case func:
			{
				write_func_call(expr->call);
				if(expr->call->type==a_call)
					write_expr(expr->index,name);
				break;
			}
		default:
			{
				char type[50];
				strcpy(type,"");
				if(expr->type == grade)
				{
					strcpy(type,"\"**(");
				}
				else if(expr->type == greater)
				{
					strcpy(type,"\">(");
				}
				else if(expr->type == equal)
				{
					strcpy(type,"\"=(");
				}
				else if(expr->type == less)
				{
					strcpy(type,"\"<(");
				}
				else if(expr->type == ne)
				{
					strcpy(type,"\"not_equal(");
				}
				else if(expr->type == ge)
				{
					strcpy(type,"\">=(");
				}
				else if(expr->type == le)
				{
					strcpy(type,"\"<=(");
				}
				else if(expr->type == xor)
				{
					strcpy(type,"\"xor(");
				}
				else if(expr->type == and)
				{
					strcpy(type,"\"and(");
				}
				else if(expr->type == or)
				{
					strcpy(type,"\"or(");
				}
				else if(expr->type == concat)
				{
					strcpy(type,"\"&(");
				}
				else if(expr->type == assign)
				{
					strcpy(type,"\":=(");
				}
				else if(expr->type == sum)
				{
					strcpy(type,"\"+(");
				}
				else if(expr->type == mul)
				{
					strcpy(type,"\"*(");
				}
				else if(expr->type == Div)
				{
					strcpy(type,"\"/(");
				}
				else if(expr->type == sub)
				{
					strcpy(type,"\"-(");
				}
				strcat(type, itoa(exprnum, buffer, 10));
				strcat(type, ")\"");
				strcat(str,type);
				strcat(str, ";\n");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				strcpy(str,type);
				strcat(str,"->");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				write_expr(expr->left, "no");
				fwrite(str, sizeof(char)* strlen(str),1, file);
				write_expr(expr->right, "no");
				break;
			}

		}
		write_expr(expr->next,NULL);
	}
}

void write_stmt_list(struct NStatementList*list)
{
	stmtlistnum++;
	if(list!=NULL)
		write_stmt(list->first);
}

void write_stmt(struct NStatement*stmt)
{
	if(stmt!=NULL)
	{
		char str1[100];
		char name1[50];
		strcpy(str1, "\"Stmt_List(");
		char buffer[10];
		strcat(str1, itoa(stmt->Slist->number, buffer, 10));
		strcat(str1, ")\"->");
		fwrite(str1, sizeof(char)* strlen(str1),1, file);
		char str[100];
		strcpy(str,"\"Stmt(");
		stmtnum++;
		strcat(str, itoa(stmtnum, buffer, 10));
		strcat(str, ")\"->");
		strcpy(str1, str);
		fwrite(str, sizeof(char)* strlen(str),1, file);
		switch(stmt->type)
		{

		case array_assign:
			{
				write_arr_assign(stmt->array_assign);
				break;
			}
		case array_var:
			{
				write_arr_var(stmt->arr_var);
				break;
			}
		case var_list_assign:
			{
				write_varlist_assign(stmt->list);
				break;
			}
		case expr:
			{
				write_expr_list(stmt->expressions);
				break;
			}
		case func_call:
			{
				write_func_call(stmt->func_call);
				break;
			}
		case if_:
			{
				write_if(stmt->if_struct);
				break;
			}
		case case_:
			{
				write_case(stmt->case_st);
				break;
			}

		case for_:
			{
				write_for(stmt->for_loop);
				break;
			}
		case while_:
			{
				write_while(stmt->while_loop);
				break;
			}
		case func_pr:
			{
				write_func_proc(stmt->func_proc);
				break;
			}
		case en:
			{
				write_var(stmt->enu);
				break;
			}
		}
		write_stmt(stmt->next);
	}

}

void write_while(struct NWhileLoop*loop)
{
	char str[100];
	char name[50];
	strcpy(name, "\"While loop(");
	char buffer[10];
	whilenum++;
	int localnum = whilenum;
	strcat(name, itoa(whilenum, buffer, 10));
	strcat(name, ")\"->");
	strcpy(str, name);
	strcat(str, "\"Condition(");
	strcat(str, itoa(localnum, buffer, 10));
	strcat(str, ")\"->");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_expr(loop->condition,NULL);
	strcpy(str, name);
	strcat(str, "\"While_body(");
	strcat(str, itoa(localnum, buffer, 10));
	strcat(str, ")\"->");
	fwrite(str, sizeof(char)* strlen(str),1, file);
	if(loop->body)
		write_stmt_list(loop->body);
	else
	{
		strcpy(str, "\"No_body\";\n");
		fwrite(str, sizeof(char)* strlen(str),1, file);
	}
}

void write_for(struct NForLoop*for_)
{
	char str[100];
	char name[50];
	strcpy(name, "\"For(");
	char buffer[10];
	fornum++;
	int localnum = fornum;
	strcat(name, itoa(fornum, buffer, 10));
	strcat(name, ")\"->");
	strcpy(str, name);
	fwrite(str, sizeof(char)* strlen(str),1, file);
	write_names(for_->condition);
	if(for_->body != NULL)
	{
		strcpy(name, "\"For(");
		char buffer[10];
		int localnum = fornum;
		strcat(name, itoa(fornum, buffer, 10));
		strcat(name, ")\"->");
		strcpy(str, name);
		fwrite(str, sizeof(char)* strlen(str),1, file);
		write_stmt_list(for_->body);
	}
	else
	{
		strcpy(str, "No_body\n");
		fwrite(str, sizeof(char)* strlen(str),1, file);
	}
}
int namelistnum=0;

void write_names(struct NNameList*list)
{
	namelistnum++;
	write_name(list->first);
}

int namenum = 0;
void write_name(struct NName*name_)
{
	if(name_!=NULL)
	{
		char buffer[10];
		char str1[100];
		char name1[50];
		strcpy(name1, "\"Name_list(");
		strcat(name1, itoa(name_->SList->number, buffer, 10));
		strcat(name1, ")\"->");
		fwrite(name1, sizeof(char)* strlen(name1),1, file);
		char str[100];
		namenum++;
		char name[50];
		strcpy(name, "\"Name(");
		strcat(name, name_->name);
		strcat(name, ",");
		strcat(name, itoa(namenum, buffer, 10));
		strcat(name, ")\"");
		fwrite(name, sizeof(char)* strlen(name),1, file);
		write_name(name_->next);
		strcpy(str,";\n");
		fwrite(str, sizeof(char)* strlen(str),1, file);
	}
}