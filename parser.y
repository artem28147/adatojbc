%{
#include "tree_structures.h"
#include <stdio.h>
#include <malloc.h>

struct NStatementList *Root;
void yyerror(const char *s);
extern int yylex(void);

struct NExpression* create_str_expr(char*value);
struct NExpression* create_int_expr(int value);
struct NExpression* create_double_expr(double value);
struct NExpression* create_bool_expr(int value);
struct NExpression* create_char_expr(char value);
struct NExpression* create_empty_expr();
struct NExpression* create_func_call_expr(struct NFuncCall * call);
struct NExpression* create_name_expr(struct NName * name);
struct NExpression* create_binary_expression(struct NExpression* Left, struct NExpression* Right, enum NExprType type);
struct NExpression* create_binary_expression2(struct NExpression* Left, struct NExpression* Right, enum NExprType type, enum NVarType assign);
struct NExpression* create_unary_expression(struct NExpression* Left,enum NExprType type);
struct NVariable* create_variable(char*name,enum NVarType type,struct NExpression*expr);
struct NVariable* create_enum_variable(struct NExpression*exlist,struct NExprList*list);
struct NVariable* create_default_variable(char*name);
struct NExprList* create_expr_list(struct NExpression*first);
void add_expr_to_list(struct NExprList*list,struct NExpression*added);
struct NVariableList* create_var_list(char*first);
void add_var_to_list(struct NVariableList*list,char*added);
void change_varlist_const(struct NVariableList * list, int isConst);
struct NVarListAssign* create_var_list_assign(struct NExprList*list, struct NExpression* value, enum NVarType type,int  isConst);
struct NStatement * create_expr_statement(struct NExprList* exp);
struct NStatement * create_enum_statement(struct NVariable*exp);
struct NStatement * create_for_statement(struct NForLoop* exp);
struct NStatement * create_while_statement(struct NWhileLoop* exp);
struct NStatement * create_case_statement(struct NCaseStruct* exp);
struct NStatement * create_if_statement(struct NIfStruct* exp);
struct NStatement * create_func_statement(struct NFuncProc* exp);
struct NStatement * create_call_statement(struct NFuncCall* exp);

struct NStatement * create_arr_statement(struct NArrayAssign* arr);
struct NStatement * create_arr_var(struct NArrayVar* arr);
struct NStatement * create_list_statement(struct NVarListAssign* list);
struct NStatementList * create_statement_list(struct NStatement* exp);
void add_statement_to_list(struct NStatementList*list,struct NStatement*statement);

struct NName* create_name(char*name);
struct NNameList* create_name_list(struct NName* name);
void add_name_to_list (struct NNameList* list, struct NName*added);

struct NArrayVar* create_array_var(struct NExprList* name, struct NExprList* leftSize, struct NExprList* rightSize, enum NVarType type, struct NExprList * list);
struct NArrayAssign* create_array_assign1(struct NExpression*first_arr,struct NExpression* second_arr, struct NExprList*from_f, struct NExprList*to_f, struct NExprList*from_s, struct NExprList*to_s);
struct NArrayAssign* create_array_assign2(struct NExpression*first_arr, struct NExprList* from_f, struct NExprList*to_f, struct NExprList * expr);
void change_varlist_type(struct NVariableList* list, enum NVarType type,struct NExpression* value);

struct NWhenStruct* create_when(struct NExprList * whenExpr, struct NStatementList * body);
struct NWhenList* create_when_list(struct NWhenStruct* whenStr);
void add_when_to_list(struct NWhenList* list, struct NWhenStruct * added);
struct NElsif* create_elsif(struct NExpression * expr, struct NStatementList*body);
struct NElsifList * create_elsif_list(struct NElsif*elsif);
struct NIfStruct * create_if(struct NStatementList * if_stmt,struct NExpression * if_expr, struct NElsifList * elsif_list,struct  NStatementList * else_stmt);
struct NCaseStruct * create_case(struct NExprList*var, struct NWhenList*body);
void add_elsif_to_list(struct NElsifList* list, struct NElsif * elsif);
struct NWhileLoop * create_while(struct NExprList*expr, struct NStatementList*body, int doWh);
struct NForLoop * create_for(int inRev, struct NExprList* beg,struct NExprList* end, struct NExprList*expr,struct  NStatementList*body);
struct NFuncProc * create_func (struct NExpression*name,struct NFuncArgs*args,struct NStatementList * body,struct NStatementList*vars, struct NExprList * ret, enum NVarType type,int pr);
struct NFuncProc * create_proc (struct NExpression*name,struct NFuncArgs*args,struct NStatementList * body,struct NStatementList*vars,int pr);
struct NFuncArgs * create_args(struct NVarListAssign*expr);
void add_arg_to_list(struct NFuncArgs*list, struct NVarListAssign* added);
struct NFuncCall * create_func_call(enum NFuncType type, struct NExprList*args, struct NExpression*name);
struct NException * create_exc(struct NExcBodyList * body);
struct NStatement * create_exc_statement(struct NException * exc);
struct NStatement * create_return_statement(struct NExpression* return_val);
struct NExcBody * create_exc_body(struct NStatementList * body, struct NExprList*name);
void add_exc_to_list(struct NExcBodyList*list, struct NExcBody* added);
struct NExcBodyList*create_exc_body_list(struct NExcBody*body);
int listnumber = 0;
int namenumber = 0;
%}
%union{
	int ConstInt;
	double ConstDouble;
	char* ConstString;
	char * VarName;
	char ConstChar;
	int ConstBool;
	struct NStatement * Statement;
	struct NStatementList * StatementList;
	struct NExpression * Expression;
	struct NException * Exception;
	struct NNameList * NameList;
	struct NName * Name;
	struct NExcBody * ExcBody;
	struct NExcBodyList* BodyList;
	struct NFuncArgs * FuncArgs;
	struct NExprList *ExprList;
	struct NArrayVar *ArrayVar;
	struct NFuncProc * FuncProc;
	struct NArrayAssign * ArrayAssign;
	struct NVarListAssign * VarListAssign;
	struct NWhenList * WhenList;
	struct NElsifList * ElsifList;
	struct NVariable * Variable;
	struct NVariable * ENum;
	struct NForLoop *ForLoop;
	struct NFuncCall * FuncCall;
	struct NVariableList * VariableList;
	enum NVarType VarType;
	enum NArrType ArrType;
	struct NIfStruct * IfStruct;
	struct NElsif * Elseif;
	struct NWhenStruct * WhenStruct;
	struct NCaseStruct * CaseStruct;
	struct NWhileLoop * WhileLoop;
	struct NStatementList *Program;
}
%token <ConstString> CONST_STRING	
%token <ConstInt> CONST_INT
%token <ConstBool> CONST_BOOL
%token <ConstDouble> CONST_DOUBLE
%token <ConstChar> CONST_CHAR
%token <VarName> NAME
%type <Name> name
%type <Program> program
%type <WhenList> when_list
%type <ElsifList> else_if_list
%type <ArrayVar> array_var
%type <Statement> statement
%type <StatementList> statement_list
%type <Expression> expression
%type <ArrayAssign> array_assign
%type <VarListAssign> var_list_assign
%type <FuncCall> func_call
%type <FuncArgs> func_args
%type <FuncProc> func_proc
%type <ENum>enum_type
%type <VarType> var_type
%type <ArrType> arr_type
%type <Elseif> else_if
%type <ForLoop> for_loop
%type <WhileLoop> while_loop
%type <CaseStruct> case_struct
%type <WhenStruct> when_struct
%type <IfStruct> if_struct
%type <ExprList> expr_list

%token ABORT
%token ABS
%token ABSTRACT
%token ACCEPT
%token ALIASED
%token ALL
%token AND
%token AT
%token BEGGIN
%token BODY
%token CASE
%token CONSTANT
%token DECLARE
%token DELAY
%token DELTA
%token DIGITS
%token DO
%token ELSE
%token ELSIF
%token END
%token ENTRY
%token EXCEPTION
%token EXIT
%token FOR
%token FUNCTION
%token GENERIC
%token IF
%token IN
%token IS
%token LIMITED
%token LOOP
%token MOD
%token NEW
%token NOT
%token NOLL
%token OF
%token OR
%token OTHERS
%token OUT
%token PRAGMA
%token PRIVATE
%token PROCEDURE
%token FIRST
%token LAST
%token LENGTH
%token PROTECTED
%token RAISE
%token RANGE
%token RENAMES
%token REQUEUE
%token RETURN
%token REVERSE
%token SELECT
%token SEPARATE
%token SUBTYPE
%token TERMINATE
%token THEN
%token TYPE
%token UNTIL
%token USE
%token WHEN
%token WHILE
%token XOR
%token GE
%token LE
%token NE
%token ARRAYRANGE
%token ASSIGN
%token ENSUE
%token GRADE
%token REALTYPE
%token ROOTINTTYPE
%token INTTYPE
%token CHARTYPE
%token BOOLTYPE
%token ENUMTYPE
%token ARRAYTYPE
%token FLOATTYPE
%token STRINGTYPE
%token RECORDTYPE
%token FUNCTIONTYPE
%right ASSIGN
%left OR XOR
%left AND '&'
%left '=' NE
%left '>' '<' GE LE
%left '+' '-'
%left '*' '/'
%left GRADE
%left NOT UMINUS
%left '\''
%nonassoc ')' '('
%%

program : statement_list {$$=$1; Root = $1;};

var_type : INTTYPE {$$ = Integer;}
		 | CHARTYPE {$$ = Character;}
		 | BOOLTYPE {$$ = Boolean;}
		 | STRINGTYPE {$$ = String;}
		 | RECORDTYPE {$$ = Record;}
		 | ENUMTYPE {$$ = ENum;}
		 | FLOATTYPE {$$ = Float;}
		 ;

arr_type : ARRAYTYPE {$$ = Array;};

array_var : expr_list ':' arr_type'('expr_list ARRAYRANGE expr_list')' OF var_type {$$=create_array_var($1,$5,$7, $10,NULL);}
		   | expr_list ':' arr_type'('expr_list ARRAYRANGE expr_list')' OF var_type ASSIGN '('expr_list')' {$$=create_array_var($1,$5,$7, $10,$13);}
		   ;

array_assign : expression '(' expr_list ARRAYRANGE expr_list')' ASSIGN '(' expr_list ')' {$$=create_array_assign2($1, $3, $5, $9);}
			 | expression '('expr_list ARRAYRANGE expr_list')' ASSIGN expression '('expr_list ARRAYRANGE expr_list')' {$$=create_array_assign1($1, $8, $3, $5, $10, $12);}
			 | expression '('expr_list')' ASSIGN expr_list {$$=create_array_assign2($1, $3, $3, $6);}
			 ;
name : NAME {$$=create_name($1);}


var_list_assign : expr_list ':' var_type {$$=create_var_list_assign($1, NULL, $3,0);}
				| expr_list ':' var_type ASSIGN expression {$$=create_var_list_assign($1,$5,$3,0);}
				| expr_list ':' name {$$=create_var_list_assign($1, NULL, ENum,0);}
				| expr_list ':' name ASSIGN expression{$$=create_var_list_assign($1, $5, ENum,0);}
				| expr_list ':' CONSTANT var_type {$$=create_var_list_assign($1, NULL, $4,1);}
				| expr_list ':' CONSTANT var_type ASSIGN expression {$$=create_var_list_assign($1,$6,$4,1);}
				| expr_list ':' CONSTANT name {$$=create_var_list_assign($1, NULL, ENum,1);}
				| expr_list ':' CONSTANT name ASSIGN expression{$$=create_var_list_assign($1, $6, ENum,1);}
				;


enum_type : TYPE expression IS '('expr_list')'{$$=create_enum_variable($2,$5);}; 

expression : expression '+' expression {$$=create_binary_expression($1,$3,sum);}
		   | '('expression '+' expression')' {$$=create_binary_expression($2,$4,sum);}
		   | expression '-' expression {$$=create_binary_expression($1,$3,sub);}
		   | expression '*' expression {$$=create_binary_expression($1,$3,mul);}
		   | expression ASSIGN expression {$$=create_binary_expression($1,$3,assign);}
		   | var_type '('expression')' {$$=create_binary_expression2(NULL,$3,func,$1);}
		   | expression '=' expression {$$=create_binary_expression($1,$3,equal);}
		   | expression GRADE expression {$$=create_binary_expression($1,$3,grade);}
		   | expression '/' expression {$$=create_binary_expression($1,$3,Div);}
		   | expression '<' expression {$$=create_binary_expression($1,$3,less);}
		   | expression '>' expression {$$=create_binary_expression($1,$3,greater);}
		   | expression GE expression {$$=create_binary_expression($1,$3,ge);}
		   | expression LE expression {$$=create_binary_expression($1,$3,le);}
		   | expression NE expression {$$=create_binary_expression($1,$3,ne);}
		   | expression XOR expression {$$=create_binary_expression($1,$3,xor);}
		   | expression AND expression {$$=create_binary_expression($1,$3,and);}
		   | expression OR expression {$$=create_binary_expression($1,$3,or);}
		   | NOT expression {$$=create_unary_expression($2,not);}
		   | expression '&' expression {$$=create_binary_expression($1,$3,concat);}
		   | '-' expression %prec UMINUS {$$=create_unary_expression($2,uminus);}
		   | CONST_INT {$$=create_int_expr($1);}
		   | CONST_STRING {$$=create_str_expr($1);}
		   | CONST_BOOL {$$=create_bool_expr($1);}
		   | CONST_DOUBLE {$$=create_double_expr($1);}
		   | CONST_CHAR {$$=create_char_expr($1);}
		   | NOLL {$$=create_empty_expr();}
		   | name {$$=create_name_expr($1);}
		   | '('expression '-' expression')' {$$=create_binary_expression($2,$4,sub);}
		   | '('expression '*' expression')' {$$=create_binary_expression($2,$4,mul);}
		   | '('expression '<' expression')' {$$=create_binary_expression($2,$4,less);}
		   | '('expression '>' expression')' {$$=create_binary_expression($2,$4,greater);}
		   | '('expression GE expression')' {$$=create_binary_expression($2,$4,ge);}
		   | '('expression LE expression')' {$$=create_binary_expression($2,$4,le);}
		   | '('expression NE expression')' {$$=create_binary_expression($2,$4,ne);}
		   | '('expression XOR expression')' {$$=create_binary_expression($2,$4,xor);}
		   | '('expression AND expression')' {$$=create_binary_expression($2,$4,and);}
		   | '('expression OR expression')' {$$=create_binary_expression($2,$4,or);}
		   | '('NOT expression')' {$$=create_unary_expression($3,not);}
		   | '('expression '&' expression')' {$$=create_binary_expression($2,$4,concat);}
		   | '(''-' expression')' %prec UMINUS {$$=create_unary_expression($3,uminus);}
		   | func_call {$$=create_func_call_expr($1);}
		   ;  
expr_list : expression {$$=create_expr_list($1);}
		  | expr_list ',' expression {add_expr_to_list($1,$3);}
		  ;
statement : expr_list ';' {$$=create_expr_statement($1);}
          | var_list_assign ';' {$$=create_list_statement($1);}
		  | array_var ';' {$$=create_arr_var($1);}
		  | array_assign ';' {$$=create_arr_statement($1);}
		  | if_struct ';' {$$=create_if_statement($1);}
		  | func_proc ';' {$$=create_func_statement($1);}
		  | while_loop ';' {$$=create_while_statement($1);}
		  | for_loop ';' {$$=create_for_statement($1);}
		  | case_struct ';' {$$=create_case_statement($1);}
		  | enum_type ';' {$$=create_enum_statement($1);}
		  | RETURN expr_list ';' {$$=create_return_statement($2);}
          ;

statement_list : statement {$$=create_statement_list($1);}
			   | statement_list statement {add_statement_to_list($1,$2);}
			   ;

else_if : ELSIF expression THEN statement_list {$$=create_elsif($2,$4);};

else_if_list : else_if {$$=create_elsif_list($1);}
		| else_if_list else_if {add_elsif_to_list($1,$2);}
		;
if_struct : 
		    IF expression THEN statement_list else_if_list END IF  {$$=create_if($4,$2, $5, NULL);}
		  | IF expression THEN statement_list else_if_list ELSE statement_list END IF  {$$=create_if($4,$2, $5, $7);}
		  | IF expression THEN statement_list END IF  {$$=create_if($4,$2, NULL, NULL);}
		  | IF expression THEN statement_list ELSE statement_list END IF  {$$=create_if($4,$2, NULL, $6);}
		  ;

when_struct : WHEN expr_list ENSUE statement_list {$$=create_when($2,$4);}
			| WHEN OTHERS ENSUE statement_list {$$=create_when(NULL,$4);}
			;

when_list : when_struct {$$=create_when_list($1);}
		  | when_list when_struct {add_when_to_list($1,$2);}
		  ;

case_struct : CASE expr_list IS when_list END CASE  {$$=create_case($2,$4);};

while_loop : WHILE expr_list LOOP statement_list END LOOP  {$$=create_while($2,$4,0);}
			| LOOP statement_list EXIT WHEN expr_list ';' END LOOP  {$$=create_while($5,$2,1);}
			;

for_loop : FOR expr_list IN expr_list ARRAYRANGE expr_list LOOP statement_list END LOOP {$$=create_for(0, $4,$6, $2,$8);}
		 | FOR expr_list IN REVERSE expr_list ARRAYRANGE expr_list LOOP statement_list END LOOP {$$=create_for(1, $5,$7, $2,$9);}
		 ; 

func_proc : PROCEDURE expression '('func_args')' IS BEGGIN statement_list END expression  {$$=create_proc($2,$4,$8,NULL,0);}
		  | PROCEDURE expression '('func_args')' IS statement_list BEGGIN statement_list END expression  {$$=create_proc($2,$4,$9,$7,0);}

		  | PROCEDURE expression IS BEGGIN END expression  {$$=create_proc($2,NULL,NULL, NULL,0);}
		  | PROCEDURE expression IS BEGGIN statement_list END expression  {$$=create_proc($2,NULL,$5, NULL,0);}
		  | PROCEDURE expression IS statement_list BEGGIN statement_list END expression  {$$=create_proc($2,NULL,$6, $4,0);}
		  | PROCEDURE expression IS statement_list BEGGIN END expression  {$$=create_proc($2,NULL,NULL, $4,0);}

		  | FUNCTION expression '('func_args')' RETURN var_type IS BEGGIN statement_list END expression  {$$=create_func($2,$4,$10,NULL, NULL,$7,0);}
		  | FUNCTION expression '('func_args')' RETURN var_type IS statement_list BEGGIN statement_list END expression  {$$=create_func($2,$4,$11,$9, NULL,$7,0);}

		  | FUNCTION expression RETURN var_type IS BEGGIN statement_list END expression  {$$=create_func($2,NULL,$7, NULL, NULL,$4,0);}
		  | FUNCTION expression RETURN var_type IS statement_list BEGGIN statement_list END expression  {$$=create_func($2,NULL,$8,$6, NULL,$4,0);}

		  
		  | PROCEDURE expression {$$=create_proc($2,NULL,NULL, NULL,1);}
		  | PROCEDURE expression '('func_args')'  {$$=create_proc($2,$4,NULL, NULL,1);}

		  | FUNCTION expression '('func_args')' RETURN var_type  {$$=create_func($2,$4,NULL,NULL,NULL,$7,1);}
		  | FUNCTION expression RETURN var_type  {$$=create_func($2,NULL,NULL,NULL,NULL,$4,1);}
		  ;

func_args : var_list_assign {$$=create_args($1);}
		  | func_args ';' var_list_assign{add_arg_to_list($1,$3);}
		  ;
func_call : expression'('expr_list')'  {$$=create_func_call(func_proc, $3, $1);}
          | expression '('')'{$$=create_func_call(func_proc, NULL, $1);}
		  | expression'\''FIRST {$$=create_func_call(first, create_expr_list($1), $1);}
		  | expression '\'' LAST {$$=create_func_call(last, create_expr_list($1), $1);}
		  | expression '\'' LENGTH {$$=create_func_call(length, create_expr_list($1), $1);}
		  ;
%%

void yyerror(const char *s)
{
    printf("%s",s);
}

struct NExpression* create_str_expr(char*value)
{
	char* buf=(char *)malloc(strlen(value)+1);
	strcpy(buf, value);
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_s;
	result->const_string = buf;
	result->next = NULL;
	printf("%s - cnst string\n",result->const_string);
    result->index = NULL;
	return result;
}

struct NStatement * create_arr_var(struct NArrayVar* arr)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->arr_var = arr;
	result->type = array_var;
	result->next = NULL;
	return result;
}

struct NName* create_name(char*name)
{
	char* buf=(char *)malloc(strlen(name)+1);
	strcpy(buf, name);
	struct NName * result = (struct NName*) malloc (sizeof(struct NName));
	result->next = NULL;
	result->name = buf;
	printf("%s - cn\n",result->name);
	return result;
}


struct NExpression* create_func_call_expr(struct NFuncCall * call)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression)); 
	result->type = func;
	result->next = NULL;
	result->call = call;
	result->index = NULL;
	return result;
}


struct NExpression* create_name_expr(struct NName * name)
{
	struct NNameList * temp = create_name_list(name);
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = name_list;
	result->next = NULL;
	result->list = temp;
	result->index = NULL;
	printf("%s - ce\n",result->list->first->name);
	return result;
}

struct NNameList* create_name_list(struct NName* name)
{
	struct NNameList * result = (struct NNameList*) malloc (sizeof(struct NNameList));
	result->first = name;
	
	namenumber++;
	result->number = namenumber;
	result->last = name;
	name->SList = result;
	printf("name-list %s\n",result->first->name);
	return result;
}

void add_name_to_list (struct NNameList*list, struct NName*added)
{
	printf("added name - %s\n",added->name);
	list->last->next = added;
	list->last = added;
	added->SList = list;
}

struct NExpression* create_int_expr(int value)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_i;
	result->list = NULL;
	result->left = NULL;
	result->right = NULL;
	result->next = NULL;
	result->index = NULL;
	result->const_int = value;
	return result;
}

struct NExpression* create_double_expr(double value)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_d;
	result->list = NULL;
	result->left = NULL;
	result->right = NULL;
	result->next = NULL;
	result->index = NULL;
	result->const_double = value;
	return result;
}

struct NExpression* create_bool_expr(int value)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_b;
	result->list = NULL;
	result->left = NULL;
	result->right = NULL;
	result->next = NULL;
	result->index = NULL;
	result->const_bool = value;
	return result;
}

struct NExpression* create_char_expr(char value)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_c;
	result->list = NULL;
	result->left = NULL;
	result->right = NULL;
	result->next = NULL;
	result->index = NULL;
	result->const_char = value;
	return result;
}

struct NExpression* create_empty_expr()
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = empty;
	result->left = NULL;
	result->right = NULL;
	result->index = NULL;
	result->list = NULL;
	result->next = NULL;
	return result;
}

struct NExpression* create_binary_expression(struct NExpression* Left,struct  NExpression* Right,enum NExprType type)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->left = Left;
	result->right = Right;
	result->next = NULL;
	result->type=type;
	printf("binary exp left\n");
	
	result->index = NULL;
	return result;
}

struct NExpression* create_binary_expression2(struct NExpression* Left, struct NExpression* Right, enum NExprType type, enum NVarType valueType)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	struct NName*name;
	struct NExpression* expr;
	struct NFuncCall* call;
	
    if(valueType == Float)
	name = create_name("Float");
	else name = create_name("Integer");
	expr = create_name_expr(name);
	return create_func_call_expr(create_func_call(func_proc,create_expr_list(Right),expr));
}

struct NExpression* create_unary_expression(struct NExpression* Left, enum NExprType type)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->left = Left;
	result->next = NULL;
	result->index = NULL;
	result->type=type;
	return result;
}

struct NVariable* create_variable(char*name,enum NVarType type,struct NExpression*expr)
{
	char* buf=(char *)malloc(strlen(name)+1);
	strcpy(buf, name);
	struct NVariable * result = (struct NVariable*) malloc (sizeof(struct NVariable));
	result->var_name = buf;
	printf("var name %s\n ",name);
	result->type=type;
	result->value=expr;
	result->list = NULL;
	result->next = NULL;
	return result;
}

struct NVariable* create_enum_variable(struct NExpression* exlist,struct NExprList*list)
{
	struct NVariable * result = (struct NVariable*) malloc (sizeof(struct NVariable));
	struct NNameList * names = create_name_list(create_name(list->first->list->first->name));
	struct NExpression * temp = list->first->next;
	while(temp!=NULL)
	{
		add_name_to_list(names,temp->list->first);
		temp = temp->next;
	}
	result->var_name = exlist->list->first->name;
	result->type=ENum;
	result->next = NULL;
	result->value = NULL;
	result->list=names;
	return result;
}

struct NVariable* create_default_variable(char*name)
{
	char* buf=(char *)malloc(strlen(name)+1);
	strcpy(buf, name);
	struct NVariable * result = (struct NVariable*) malloc (sizeof(struct NVariable));
	result->var_name = buf;
	result->next = NULL;
	return result;
}

struct NExprList* create_expr_list(struct NExpression*first)
{
	struct NExprList * result = (struct NExprList*) malloc (sizeof(struct NExprList));
	result->first = first;
	result->last = first;
	if(first->type == name_list)
	{
	printf("expr_list %s\n",result->first->list->first->name);
	}
	return result;
}

void add_expr_to_list(struct NExprList*list,struct NExpression*added)
{
	list->last->next = added;
	list->last = added;
}

struct NVariableList* create_var_list(char*first)
{
	char* buf=(char *)malloc(strlen(first)+1);
	strcpy(buf, first);
	struct NVariableList * result = (struct NVariableList*) malloc (sizeof(struct NVariableList));
	struct NVariable* var = create_variable(buf,Root_Integer,NULL);
	result->first = var;
	result->last = var;
	var->next = NULL;
	return result;
}

void add_var_to_list(struct NVariableList*list,char*added)
{
	char* buf=(char *)malloc(strlen(added)+1);
	strcpy(buf, added);
    struct NVariable* var = create_variable(buf,Root_Integer,NULL);
	list->last->next = var;
	list->last = var;
}

struct NArrayVar* create_array_var(struct NExprList* name, struct NExprList* leftSize, struct NExprList* rightSize, enum NVarType type,struct NExprList * list)
{
	struct NArrayVar * result = (struct NArrayVar*) malloc (sizeof(struct NArrayVar));
	result->arr_name = name->first->list->first->name;
	result->elem_type = type;
	result->leftSizeExpr = leftSize;
	result->rightSizeExpr = rightSize;
	if (leftSize->first->type==uminus)
		result->leftSize = -(leftSize->first->left->const_int);
	else
		result->leftSize = leftSize->first->const_int;
	if (rightSize->first->type==uminus)
		result->rightSize = -(rightSize->first->left->const_int);
	else
		result->rightSize = rightSize->first->const_int;
	result->expression = list;
	printf ("array var\n");
	return result;
}

struct NArrayAssign* create_array_assign1(struct NExpression*first_arr,struct NExpression* second_arr, struct NExprList* from_f, struct NExprList* to_f, struct NExprList*from_s, struct NExprList*to_s)
{
	struct NArrayAssign * result = (struct NArrayAssign*) malloc (sizeof(struct NArrayAssign));
	result->left = first_arr->list->first->name;
	result->right = second_arr->list->first->name;
	result->expr = NULL;
	result->from_left = from_f->first->const_int;
	result->to_left = to_f->first->const_int;;
	result->from_right = from_s->first->const_int;;
	result->to_right = to_s->first->const_int;;
	printf ("array assign\n");
	return result;
}


struct NArrayAssign* create_array_assign2(struct NExpression*first_arr, struct NExprList*from_f, struct NExprList* to_f,struct  NExprList * expr)
{
	struct NArrayAssign * result = (struct NArrayAssign*) malloc (sizeof(struct NArrayAssign));
	result->left = first_arr->list->first->name;
	result->right = NULL;
	result->expr = expr;
	result->from_left = from_f->first->const_int;
	result->to_left = to_f->first->const_int;
	result->from_right = -1;
	result->to_right = -1;
	return result;
}

struct NVarListAssign* create_var_list_assign(struct NExprList*list, struct NExpression* value, enum NVarType type, int isConst)
{
	struct NVarListAssign * result = (struct NVarListAssign*) malloc (sizeof(struct NVarListAssign));
	struct NName*n;
	struct NVariableList* varlist;
	struct NExpression * temp = list->first;
	printf("varlist assign %s \n",list->first->list->first->name);
	n=list->first->list->first;
	varlist = create_var_list(list->first->list->first->name);
	temp=temp->next;
	while(temp!=NULL)
	{

	    add_var_to_list(varlist,temp->list->first->name);
		temp=temp->next;
	} 
	change_varlist_type(varlist,type,value);
	change_varlist_const(varlist,isConst);
	result->variables = varlist;
	result->value = value;
	result->next = NULL;
	return result;
}

void change_varlist_type(struct NVariableList * list, enum NVarType type,struct NExpression* value)
{
	struct NVariable * temp;
	if(list!=NULL)
	{
	    temp = list->first;
		do
		{
			temp->type = type;
			temp->value = value;
			temp=temp->next;
		} while(temp!=NULL);
	}
}

void change_varlist_const(struct NVariableList * list, int isConst)
{
	struct NVariable * temp;
	if(list!=NULL)
	{
	    temp = list->first;
		do
		{
			temp->isConstant = isConst;
			temp=temp->next;
		} while(temp!=NULL);
	}
}

struct NStatement * create_expr_statement(struct NExprList* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = expr;
	result->next = NULL;
	result->expressions = exp;
	printf("expr_stmt \n");
	return result;
}

struct NStatement * create_exc_statement(struct NException * exc)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = exception;
	result->next = NULL;
	result->except = exc;
	return result;
}

struct NStatement * create_for_statement(struct NForLoop* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = for_;
	result->next = NULL;
	result->for_loop = exp;
	return result;
}
struct NStatement * create_while_statement(struct NWhileLoop* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = while_;
	result->next = NULL;
	result->while_loop = exp;
	return result;
}

struct NStatement * create_enum_statement(struct NVariable*exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = en;
	result->next = NULL;
	result->enu = exp;
	return result;
}

struct NStatement * create_return_statement(struct NExprList* return_val)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = return_;
	result->next = NULL;
	result->return_st=(struct NReturn*) malloc (sizeof(struct NReturn));
	result->return_st->returnValue=return_val->first;
	return result;
}

struct NStatement * create_case_statement(struct NCaseStruct* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = case_;
	result->next = NULL;
	result->case_st = exp;
	return result;
}
struct NStatement * create_if_statement(struct NIfStruct* exp)
{
	struct NStatement * 
	result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = if_;
	result->next = NULL;
	result->if_struct = exp;
	printf ("if_statement\n");
	return result;
}
struct NStatement * create_func_statement(struct NFuncProc* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = func_pr;
	result->next = NULL;
	result->func_proc = exp;
	return result;
}
struct NStatement * create_call_statement(struct NFuncCall* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = func_call;
	result->next = NULL;
	result->func_call = exp;
	return result;
}

struct NStatement * create_arr_statement(struct NArrayAssign* arr)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = array_assign;
	result->next = NULL;
	result->array_assign = arr;
	return result;
}

struct NStatement * create_list_statement(struct NVarListAssign* list)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = var_list_assign;
	result->next = NULL;
	result->list = list;
	return result;
}

struct NStatementList * create_statement_list(struct NStatement* exp)
{
	
	struct NStatementList * result = (struct NStatementList*) malloc (sizeof(struct NStatementList));
	result->first = exp;
	listnumber++;
	result->number = listnumber;
	result->last = exp;
	exp->Slist = result;
	printf("stmt list\n");
	return result;
}

void add_statement_to_list(struct NStatementList*list,struct NStatement*statement)
{
	statement->Slist = list;
	list->last->next = statement;
	list->last = statement;
}

struct NElsif* create_elsif(struct NExpression * expr, struct NStatementList*body)
{
	struct NElsif * result = (struct NElsif*) malloc (sizeof(struct NElsif));
	result->elsifExpr = expr;
	result->body = body;
	result->next = NULL;
	return result;
}

struct NElsifList* create_elsif_list(struct NElsif*elsif)
{
	struct NElsifList * result = (struct NElsifList*) malloc (sizeof(struct NElsifList));
	result->first = elsif;
	result->last = elsif;
	return result;
}

void add_elsif_to_list(struct NElsifList* list, struct NElsif * elsif)
{
	
	list->last->next = elsif;
	list->last = elsif;
}

struct NIfStruct * create_if(struct NStatementList * if_stmt,struct NExpression * if_expr, struct NElsifList * elsif_list, struct NStatementList * else_stmt)
{
	struct NIfStruct * result = (struct NIfStruct*) malloc (sizeof(struct NIfStruct));
	result->ifStmt = if_stmt;
	result->ifExpr=if_expr;
	result->elsifList = elsif_list;
	result->elseStmt = else_stmt;
	return result;
}

struct NWhenStruct* create_when(struct NExprList * whenExpr, struct NStatementList * body)
{
	struct NWhenStruct * result = (struct NWhenStruct*) malloc (sizeof(struct NWhenStruct));
	result->whenExpr = whenExpr;
	result->next = NULL;
	result->body = body;
	return result;
}

struct NWhenList* create_when_list(struct NWhenStruct* whenStr)
{
	struct NWhenList * result = (struct NWhenList*) malloc (sizeof(struct NWhenList));
	result->first = whenStr;
	result->last = whenStr;
	return result;
}

void add_when_to_list(struct NWhenList* list, struct NWhenStruct * added)
{
	list->last->next = added;
	list->last = added;
}

struct NCaseStruct * create_case(struct NExprList*var, struct NWhenList*body)
{
	struct NCaseStruct * result = (struct NCaseStruct*) malloc (sizeof(struct NCaseStruct));
	result->caseVar = var->first->list;
	result->body = body;
	return result;
}

struct NWhileLoop * create_while(struct NExprList*expr, struct NStatementList*body, int doWh)
{
	struct NWhileLoop * result = (struct NWhileLoop*) malloc (sizeof(struct NWhileLoop));
	result->condition = expr->first;
	result->body = body;
	result->doWhile=doWh;
	return result;
}

struct NForLoop * create_for(int inRev,struct  NExprList* beg,struct NExprList* end, struct NExprList*expr,struct  NStatementList*body)
{
	struct NForLoop * result = (struct NForLoop*) malloc (sizeof(struct NForLoop));
	result->inReverse = inRev;
	result->begin = beg->first;
	result->end = end->first;
	result->condition = expr->first->list;
	result->body=body;
	return result;
}

struct NFuncProc * create_func (struct NExpression*name,struct NFuncArgs*args,struct NStatementList * body,struct NStatementList*vars, struct NExprList * ret, enum NVarType type,int pr)
{
	struct NFuncProc * result = (struct NFuncProc*) malloc (sizeof(struct NFuncProc));
	result->isFunc = 1;	
	if(ret!=NULL)
	result->returnValue = ret->first;
	else result->returnValue = NULL;
	result->name = name->list->first->name;
	result->arguments = args;
	result->body = body;
	result->localVars = vars;
	result->isPrototype = pr;
	result->type = type;
	printf("Func created\n");
	return result;
}

struct NFuncProc * create_proc (struct NExpression*name,struct NFuncArgs*args,struct NStatementList * body,struct NStatementList*vars,int pr)
{
	struct NFuncProc * result = (struct NFuncProc*) malloc (sizeof(struct NFuncProc));
	result->isFunc = 0;
	result->name=name->list->first->name;
	result->arguments = args;
	result->body = body;
	result->type = Root_Integer;
	result->isPrototype = pr;
	result->localVars = vars;
	result->returnValue = NULL;
	return result;
}

struct NFuncArgs * create_args(struct NVarListAssign*expr)
{
	struct NFuncArgs * result = (struct NFuncArgs*) malloc (sizeof(struct NFuncArgs));
	result->first = expr;
	result->first->next = NULL;
	result->last = expr;
	return result;
}

void add_arg_to_list(struct NFuncArgs*list, struct NVarListAssign* added)
{
	list->last->next = added;
	list->last = added;
}

struct NFuncCall * create_func_call(enum NFuncType type, struct NExprList*args, struct NExpression*name)
{
	struct NFuncCall * result = (struct NFuncCall*) malloc (sizeof(struct NFuncCall));
	if(type == length)
	{
		result->func = "LENGTH";
	}
	else if(type == first)
	{
		result->func = "FIRST";
	}
	else if(type == last)
	{
		result->func = "LAST";
	}
	else
	{
		result->func = name->list->first->name;
	}
	result->type = type;
	result->arguments = args;
		return result;
}

struct NException * create_exc(struct NExcBodyList * body)
{
	struct NException * result = (struct NException*) malloc (sizeof(struct NException));
	result->list = body;
	return result;
}

struct NExcBody * create_exc_body(struct NStatementList * body, struct NExprList*name)
{
	struct NExcBody * result = (struct NExcBody*) malloc (sizeof(struct NExcBody));
	if(name == NULL)
	{
	result->name = "Others";
	}
	else 
	{
	result->name = name->first->list->first->name;
	}
	result->body = body;
	return result;
}

void add_exc_to_list(struct NExcBodyList*list, struct NExcBody* added)
{
	list->last->next = added;
	list->last = added;
}

struct NExcBodyList*create_exc_body_list(struct NExcBody*body)
{
	struct NExcBodyList * result = (struct NExcBodyList*) malloc (sizeof(struct NExcBodyList));
	result->first = body;
	result->last = body;
	return result;
}