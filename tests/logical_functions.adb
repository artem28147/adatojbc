procedure logic is
    a: Integer := -7;
    b: Integer := 12;
    eq1, eq2: Integer :=  0;
    t: Boolean := true;
    f: Boolean := false;
begin
    if a>b or eq1=eq2 then
        printLnString("correct");
    else
        printLnString("err");
    end if;
    
    if not false then
    printLnString("not f is true");
    else
        printLnString("err");
    end if;
    
    if eq1>=a and eq2<=b then
    printLnString("eq1>=a and eq2<=b");
    else
        printLnString("err");
    end if;
    
    if eq1/=eq2 and (b<a and t=f) then
        printLnString("err");
    else
        printLnString("eq1/=eq2 and (b<a and t=f)");
    end if;
    
    if f or eq1/=eq2 or (a<=eq1 and t) then
        printLnString("f or eq1/=eq2 or (a<=eq1 and t)");
    else
        printLnString("err");
    end if;
end logic;