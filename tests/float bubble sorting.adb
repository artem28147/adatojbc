procedure main is
    Temp : Float;
	I,J,n,k,leftSize,rightSize:Integer;
begin
	printLnString("Введите границы массива для сортировки пузырьком");
	leftSize:=scanInt();	
	rightSize:=scanInt();	
	A : array (leftSize..rightSize) of Float ;
	printLnString("Введите массив:");

	for k in A'FIRST..A'LAST loop
		A(k):=scanFloat();
	end loop;

    for I in A'FIRST..A'LAST loop
       for J in I .. A'LAST loop
          if A(I) > A(J) then
             Temp := A(J);
             A(J) := A(I);
             A(I) := Temp;
          end if;
       end loop;
    end loop;
	
	printLnString("Отсортированный массив:");
	for i in A'FIRST..A'LAST loop
		printLnFloat(A(i));
	end loop;
end main;