#include "SemanticalFunctions.h"


std::vector<struct SemanticalElement*> table_of_const;
std::vector<struct ClassElement> table_of_classes;
struct SemanticalElement* newClass = new SemanticalElement();
struct ClassElement* parent_class = new ClassElement();
struct ClassElement* current_class = new ClassElement();
struct MethodElement* current_method = new MethodElement();
struct MethodElement* method1 = new struct MethodElement();
struct SemanticalElement* curClass = new SemanticalElement();
FILE* fileTable;
unsigned long int findMethodRef(char * name)
{
	for (SemanticalElement* el : table_of_const)
	{
		if (el->second != NULL && el->second->type == CONST_NameAndType && strcmp(name, el->second->first->str.c_str()) == 0)
			return (unsigned long)(el->id);
	}
	return -1;
}

unsigned long int findType(SemanticalConst type)
{
	for (SemanticalElement* el : table_of_const)
	{
		if (el->type == type)
			return (unsigned long)(el->id);
	}
	return -1;
}

bool isArray(const char * method, char * name)
{
	int i;
	bool isArray = false;
	std::vector<struct LocalElement> *vars;
	for (i = 0; i < table_of_classes[1].methods->size(); i++)
	{
		if (strcmp(table_of_classes[1].methods->at(i).name->str.c_str(), method) == 0)
			vars = table_of_classes[1].methods->at(i).localvars;
	}
	for (i = 0; i < vars->size(); i++)
	{
		if (stricmp(name, vars->at(i).name.c_str()) == 0 && vars->at(i).type == NVarType::Array)
			isArray = true;
	}
	for (i = 0; i < table_of_classes[1].fields->size(); i++)
	{
		if (strcmp(table_of_classes[1].fields->at(i).name->str.c_str(), name) == 0)
			if (table_of_classes[1].fields->at(i).leftSizeExpr!=NULL)
				isArray = true;
	}
	return isArray;
}

NVarType getArrayType(const char * method, char * name)
{
	int i;
	std::vector<struct LocalElement> *vars;
	for (i = 0; i < table_of_classes[1].methods->size(); i++)
	{
		if (strcmp(table_of_classes[1].methods->at(i).name->str.c_str(), method) == 0)
			vars = table_of_classes[1].methods->at(i).localvars;
	}
	for (i = 0; i < vars->size(); i++)
	{
		if (stricmp(name, vars->at(i).name.c_str()) == 0 && vars->at(i).type == NVarType::Array)
			return vars->at(i).elem_type;
	}
	return Integer;
}

void print_table_of_consts()
{
	char str[500];
	strcpy(str, "<html>\n");
	fileTable = fopen("tables.html", "wt");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	strcpy(str, "<head>\n<title>Tables</title>\n</head>\n<body>\n\"������� ��������\"<table border=\"1\">\n");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	for (int i = 0; i < table_of_const.size(); i++)
	{
		strcpy(str, "<tr>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		itoa(i + 1, str, 10);
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, translate_descriptor(table_of_const[i]->type));
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, translate_value_to_str(table_of_const[i]));
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n</tr>\n");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	}
	strcpy(str, "</table>\n\"������� �������\"<table border=\"1\">\n");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	for (int i = 0; i < table_of_classes.size(); i++)
	{
		strcpy(str, "<tr>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		itoa(i + 1, str, 10);
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, translate_value_to_str((table_of_classes[i].classname->first)));
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");

	}
	strcpy(str, "</table>\n");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	for (int i = 0; i < table_of_classes.size(); i++)
	{
		print_table_of_field_vars(table_of_classes[i].fields, table_of_classes[i].classname->first->str);
	}
	for (int i = 0; i < table_of_classes.size(); i++)
	{
		print_table_of_methods(table_of_classes[i].methods, table_of_classes[i].classname->first->str);
	}
	strcpy(str, "</body>\n</html>");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	fclose(fileTable);

}

void print_table_of_classes()
{

}

void print_table_of_methods(std::vector<struct MethodElement> * methodtable, std::string name)
{
	char str[500];
	strcpy(str, "\"������� ������� ������ ");
	strcat(str, name.c_str());
	strcat(str, "\"<table border=\"1\">\n");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	for (int i = 0; i < methodtable->size(); i++)
	{
		strcpy(str, "<tr>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		itoa(i + 1, str, 10);
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, translate_value_to_str((methodtable->at(i).name)));
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, translate_value_to_str((methodtable->at(i).desc)));
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	}
	strcpy(str, "</table>\n");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	for (int i = 0; i < methodtable->size(); i++)
	{
		print_table_of_local_vars(methodtable->at(i).localvars, methodtable->at(i).name->str);
	}
}

void print_table_of_field_vars(std::vector<struct FieldElement> * fieldtable, std::string name)
{
	char str[500];
	strcpy(str, "\"������� ����� ������ ");
	strcat(str, name.c_str());
	strcat(str, "\"<table border=\"1\">\n");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	for (int i = 0; i < fieldtable->size(); i++)
	{
		strcpy(str, "<tr>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		itoa(i + 1, str, 10);
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, translate_value_to_str((fieldtable->at(i).name)));
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, translate_value_to_str((fieldtable->at(i).desc)));
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		if (fieldtable->at(i).is_constant)
		{
			strcpy(str, "Is constant field");
		}
		else
		{
			strcpy(str, "Is not constant field");
		}
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	}
	strcpy(str, "</table>\n");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
}

void print_table_of_local_vars(std::vector<struct LocalElement>* localvars, std::string name)
{
	char str[500];
	strcpy(str, "\"������� ��������� ���������� ������ ");
	strcat(str, name.c_str());
	strcat(str, "\"<table border=\"1\">\n");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	for (int i = 0; i < localvars->size(); i++)
	{
		strcpy(str, "<tr>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		itoa(i + 1, str, 10);
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, localvars->at(i).name.c_str());
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, write_type(localvars->at(i).type));
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);

		strcpy(str, "</p>\n</td>\n<td>\n<p>");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		if (localvars->at(i).isConst)
		{
			strcpy(str, "is constant var");
		}
		else
		{
			strcpy(str, "is not constant var");
		}
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
		strcpy(str, "</p>\n</td>\n</tr>\n");
		fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
	}
	strcpy(str, "</table>\n");
	fwrite(str, sizeof(char)* strlen(str), 1, fileTable);
}


char * translate_descriptor(enum SemanticalConst type)
{
	char * result = (char *)malloc(100);

	switch (type)
	{
	case CONST_Utf8:
	{
		return ("UTF-8 (1)");
	}
	case  CONST_Integer:
	{
		return ("Integer (3)");
	}
	case CONST_Float:
	{
		return ("Float (4)");
	}
	case CONST_Class:
	{
		return ("Class (7)");
	}
	case CONST_String:
	{
		return ("String (8)");
	}
	case CONST_Fieldref:
	{
		return ("Fieldref (9)");
	}
	case CONST_Methodref:
	{
		return ("Methodref (10)");
	}
	case CONST_NameAndType:
	{
		return ("Name and Type (12)");
	}
	}
}

char * translate_value_to_str(struct SemanticalElement * element)
{
	char buffer[100];
	char str[100];
	switch (element->type)
	{
	case CONST_Utf8:
	{
		strcpy(str, element->str.c_str());
		return str;
	}
	case  CONST_Integer:
	{
		return itoa(element->const_int, buffer, 10);
	}
	case CONST_Float:
	{
		sprintf(buffer, "%17.2lf", element->const_float);
		return buffer;
	}
	case CONST_Class:
	{
		return itoa(element->first->id, buffer, 10);
	}
	case CONST_String:
	{
		return itoa(element->first->id, buffer, 10);
	}
	case CONST_Fieldref:
	{
		strcpy(str, itoa(element->first->id, buffer, 10));
		strcat(str, ", ");
		strcat(str, itoa(element->second->id, buffer, 10));
		return str;
	}
	case CONST_Methodref:
	{
		strcpy(str, itoa(element->first->id, buffer, 10));
		strcat(str, ", ");
		strcat(str, itoa(element->second->id, buffer, 10));
		return str;
	}
	case CONST_NameAndType:
	{
		strcpy(str, itoa(element->first->id, buffer, 10));
		strcat(str, ", ");
		strcat(str, itoa(element->second->id, buffer, 10));
		return str;
	}
	}
}

//���������� ���� utf-8 � ������� ��������
struct SemanticalElement* addUtf8ToConstTable(std::string name)
{
	struct SemanticalElement* result = new SemanticalElement();
	result->type = CONST_Utf8;
	result->str = name;
	result->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), result);
	return result;
}

//���������� ���� int � ������� ��������
struct SemanticalElement* addIntToConstTable(int name)
{
	struct SemanticalElement* result = new SemanticalElement();
	result->type = CONST_Integer;
	result->const_int = name;
	result->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), result);
	return result;
}

//���������� ���� float � ������� ��������
struct SemanticalElement* addFloatToConstTable(float name)
{
	struct SemanticalElement* result = new SemanticalElement();
	result->type = CONST_Float;
	result->const_float = name;
	result->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), result);
	return result;
}

//���������� ���� string � ������� ��������
struct SemanticalElement* addStringToConstTable(std::string name)
{
	struct SemanticalElement* result = new  SemanticalElement();
	struct SemanticalElement* utf8 = addUtf8ToConstTable(name);
	result->type = CONST_String;
	result->first = utf8;
	result->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), result);
	return result;
}

struct SemanticalElement* addNameTypeToConstTable(std::string name, std::string desc)
{
	struct SemanticalElement* element = new  SemanticalElement();
	struct SemanticalElement* utf8name = addUtf8ToConstTable(name);
	struct SemanticalElement* utf8desc = addUtf8ToConstTable(desc);
	element->type = CONST_NameAndType;
	element->first = utf8name;
	element->second = utf8desc;
	element->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), element);
	return element;
}

struct SemanticalElement* addFieldToTableConst(struct SemanticalElement* classname, std::string name, std::string desc) throw(char*)
{
	struct SemanticalElement* element = new SemanticalElement();
	struct SemanticalElement* NameType = addNameTypeToConstTable(name, desc);
	for (int i = 0; i < table_of_const.size(); i++)
	{
		if (table_of_const[i]->type == CONST_Fieldref && table_of_const[i]->first->first->str == classname->first->str && table_of_const[i]->second->first->str == name)
		{
			throw ("Same names of elements");
		}
	}
	element->type = CONST_Fieldref;
	element->first = classname;
	element->second = NameType;
	element->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), element);
	return element;
}

// ���������� ������ � ������� ��������
struct SemanticalElement* addMethodToConstTable(struct SemanticalElement* classname, std::string name, std::string desc) throw(char*)
{
	struct SemanticalElement* element = new SemanticalElement();
	struct SemanticalElement* NT = addNameTypeToConstTable(name, desc);
	for (int i = 0; i < table_of_const.size(); i++)
	{
		if (table_of_const[i]->type == CONST_Methodref && table_of_const[i]->first->first->str == classname->first->str && table_of_const[i]->second->first->str == name)
		{
			throw ("Two methods are the same");
		}
	}
	element->type = CONST_Methodref;
	element->first = classname;
	element->second = NT;
	element->id = table_of_const.size() + 1;
	table_of_const.insert
	(table_of_const.end(), element);
	return element;
}

void add_var_to_table(struct NVariable* var)
{
	struct FieldElement* field;
	struct SemanticElement * element;
}

//���������� ���������� � ������� ��������� ����������
void add_to_local_table(struct NVariable* var) throw (char*)
{
	struct LocalElement* element = new LocalElement();
	if (strcmp(current_method->name->str.c_str(), "main") == 0)
		element->id = current_method->localvars->size() + 1;
	else
		element->id = current_method->localvars->size();
	element->isConst = var->isConstant;
	element->name = var->var_name;
	element->type = var->type;
	element->var = var;
	int i;
	for (i = 0; i < current_method->localvars->size(); i++)
	{
		if (stricmp(current_method->localvars->at(i).name.c_str(), element->name.c_str()) == 0)
		{
			throw ("Repeated local variable");
		}
	}

	current_method->localvars->insert(current_method->localvars->end(), *element);
}

void add_to_field_table(struct FieldElement* element) throw (char*)
{
	int i;
	for (i = 0; i < parent_class->fields->size(); i++)
	{
		if (parent_class->fields->at(i).name->str == element->name->str) {
			throw ("Repeated global variables");
		}
	}
	parent_class->fields->insert(parent_class->fields->end(), *element);
}

void add_to_class_table(struct ClassElement* added) throw(char*)
{
	table_of_classes.insert(table_of_classes.end(), *added);
}

void add_to_method_table(struct MethodElement* element) throw (char*)
{
	for (int i = 0; i < parent_class->methods->size(); i++)
	{
		if (parent_class->methods->at(i).name->str == element->name->str) {
			throw ("Method repeats");
		}
	}
	parent_class->methods->insert(parent_class->methods->end(), *element);
}

//������ ������ �� ������ ����������� ����� �������� ��������
void first_sem_stmt_list(struct NStatementList* list)
{
	struct NStatement* stmt;
	stmt = list->first;
	while (stmt != NULL)
	{
		first_sem_stmt(stmt);
		stmt = stmt->next;
	}
}

//������ ������ �� ����������� ����� �������� ��������
void first_sem_stmt(struct NStatement * stmt)
{
	switch (stmt->type)
	{
	case expr:
	{
		expr_list_sem(stmt->expressions);
		break;
	}
	case func_pr:
	{
		func_sem(stmt->func_proc);
		break;
	}
	case var_list_assign:
	{
		break;
	}
	case if_:
	{
		if_sem(stmt->if_struct);
		break;
	}
	case while_:
	{
		while_sem(stmt->while_loop);
		break;
	}
	case for_:
	{
		for_sem(stmt->for_loop);
		break;
	}
	case en:
	{
		break;
	}
	case return_:
	{
		expr_sem(stmt->return_st->returnValue);
		break;
	}
	}
}


void for_sem(struct NForLoop*loop)
{
	stmt_list_sem(loop->body);
}
//������� ��� ���������� ������
void fill()throw (char*)
{
	if (Root->first->type != func_pr)
	{
		throw("All code should be put into the main procedure");

	}
	if (Root->first->func_proc->body == NULL)
	{
		throw("Statements expected in the main function");
	}

	struct SemanticalElement* element2 = new struct SemanticalElement();
	struct SemanticalElement* utf82 = addUtf8ToConstTable(*(new std::string("java/lang/Object")));
	element2->first = utf82;
	element2->type = CONST_Class;
	element2->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), element2);
	curClass = element2;
	parent_class->classname = curClass;
	parent_class->parentname = NULL;
	table_of_classes.insert(table_of_classes.end(), *parent_class);

	struct SemanticalElement* element3 = new struct SemanticalElement();
	struct SemanticalElement* utf83 = addUtf8ToConstTable(*(new std::string("java/lang/String")));
	element3->first = utf83;
	element3->type = CONST_Class;
	element3->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), element3);
	curClass = element3;
	parent_class->classname = curClass;
	parent_class->parentname = NULL;
	table_of_classes.insert(table_of_classes.end(), *parent_class);

	struct SemanticalElement* element4 = new struct SemanticalElement();
	struct SemanticalElement* utf84 = addUtf8ToConstTable(*(new std::string("RTL")));
	element4->first = utf84;
	element4->type = CONST_Class;
	element4->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), element4);
	curClass = element4;
	parent_class->classname = curClass;
	parent_class->parentname = NULL;
	table_of_classes.insert(table_of_classes.end(), *parent_class);

	struct SemanticalElement* added = new SemanticalElement();
	char  desc[100];
	addMethodToConstTable(curClass, "printInt", "(I)V");
	addMethodToConstTable(curClass, "printFloat", "(F)V");
	addMethodToConstTable(curClass, "printBoolean", "(I)V");
	addMethodToConstTable(curClass, "printChar", "(I)V");
	addMethodToConstTable(curClass, "printString", "(Ljava/lang/String;)V");
	addMethodToConstTable(curClass, "printLnInt", "(I)V");
	addMethodToConstTable(curClass, "printLnFloat", "(F)V");
	addMethodToConstTable(curClass, "printLnBoolean", "(I)V");
	addMethodToConstTable(curClass, "printLnChar", "(I)V");
	addMethodToConstTable(curClass, "printLnString", "(Ljava/lang/String;)V");
	addMethodToConstTable(curClass, "scanInt", "()I");
	addMethodToConstTable(curClass, "scanBoolean", "()I");
	addMethodToConstTable(curClass, "scanFloat", "()F");
	addMethodToConstTable(curClass, "scanChar", "()I");
	addMethodToConstTable(curClass, "scanString", "()Ljava/lang/String;");
	addMethodToConstTable(curClass, "cloneIntArray", "([I)[I");
	addMethodToConstTable(curClass, "cloneFloatArray", "([F)[F");
	addMethodToConstTable(curClass, "cloneStringArray", "([Ljava/lang/String;)[Ljava/lang/String;");


	struct SemanticalElement* element = new struct SemanticalElement();
	struct SemanticalElement* utf8 = addUtf8ToConstTable(*(new std::string("Main_Class")));
	element->first = utf8;
	element->type = CONST_Class;
	element->id = table_of_const.size() + 1;
	table_of_const.insert(table_of_const.end(), element);
	curClass = element;
	parent_class->classname = curClass;
	parent_class->parentname = element2;
	table_of_classes.insert(table_of_classes.end(), *parent_class);

	fill_stmt_list_table(Root);
	first_sem_stmt_list(Root);
	addUtf8ToConstTable(*(new std::string("Code")));
}

//������� ���������� ���������
void semantics()throw(char *)
{

}

void stmt_sem(struct NStatement * stmt)
{
	switch (stmt->type)
	{
	case array_var:
	{
		break;
	}
	case array_assign:
	{
		break;
	}
	case var_list_assign:
	{
		expr_sem(stmt->list->value);
		break;
	}
	case expr:
	{
		expr_list_sem(stmt->expressions);
		break;
	}
	case func_call:
	{
		expr_sem(stmt->list->value);
		break;
	}
	case if_:
	{
		if_sem(stmt->if_struct);
		stmt_list_sem(stmt->if_struct->ifStmt);
		break;
	}
	case return_:
	{
		expr_sem(stmt->return_st->returnValue);
		break;
	}
	case case_:
	{
		break;
	}
	case for_:
	{
		for_sem(stmt->for_loop);
		stmt_list_sem(stmt->for_loop->body);
		expr_sem(stmt->for_loop->begin);
		expr_sem(stmt->for_loop->end);
		break;
	}
	case while_:
	{
		while_sem(stmt->while_loop);
		stmt_list_sem(stmt->while_loop->body);
		expr_sem(stmt->while_loop->condition);
		break;
	}
	case func_pr:
	{
		break;
	}
	case en:
	{
		break;
	}
	}
	if (stmt->next != NULL)
		stmt_sem(stmt->next);
}

void stmt_list_sem(struct NStatementList* list)
{
	if (list != NULL)
		stmt_sem(list->first);
}

void var_sem(struct NVariable * var)
{
	if (var->value != NULL)
	{
		expr_sem(var->value);
		if (var->type != var->value->type)
			throw("Var type and type of expression mismatch");

	}
	if (var->next != NULL)
	{
		var_sem(var->next);
	}

}

void expr_list_sem(struct NExprList * list)
{
	if (list != NULL)
		expr_sem(list->first);
}

bool match_types(struct NExpression * expr)
{
	enum NVarType type1 = Root_Integer;
	enum NVarType type2 = Root_Integer;
	if (expr->type == name_list)
	{
		for (int i = 0; i < current_method->localvars->size(); i++)
		{
			if (stricmp(current_method->localvars->at(i).name.c_str(), expr->right->list->first->name) == 0)
			{
				type1 = current_method->localvars->at(i).type;

			}

			for (int j = 0; j < current_method->localvars->size(); j++)
			{
				if (stricmp(current_method->localvars->at(j).name.c_str(), expr->left->list->first->name) == 0)
				{
					type2 = current_method->localvars->at(j).type;
				}
			}
		}
	}
	return (type1 == type2);
}

void expr_sem(struct NExpression * expr)throw(char *)
{
	bool found;
	char error[100];
	if (expr != NULL)
	{
		switch (expr->type)
		{
		case func:
		{
			if (is_in_local_vars(expr->call->func) != NULL)
				expr->valueType = is_in_local_vars(expr->call->func)->type;
			else if (is_in_field_vars(expr->call->func) != NULL)
				expr->valueType = make_type_from_description(is_in_field_vars(expr->call->func)->desc);
			else if (is_in_methods(expr->call->func) != NULL)
				expr->valueType = make_type_from_description(is_in_methods(expr->call->func)->desc);
			else
			{
				if (strcmp(expr->call->func, "Float") == 0)
					expr->valueType = Float;
				else if (strcmp(expr->call->func, "Integer") == 0)
					expr->valueType = Integer;
				else if (strcmp(expr->call->func, "printInt") == 0 || strcmp(expr->call->func, "printFloat") == 0 || strcmp(expr->call->func, "printBoolean") == 0
						|| strcmp(expr->call->func, "printChar") == 0 || strcmp(expr->call->func, "printString") == 0 || strcmp(expr->call->func, "printLnInt") == 0 ||
						strcmp(expr->call->func, "printLnFloat") == 0 || strcmp(expr->call->func, "printLnBoolean") == 0 || strcmp(expr->call->func, "printLnChar") == 0 ||
						strcmp(expr->call->func, "printLnString") == 0 )
					expr->valueType = Void;
				else if (strcmp(expr->call->func, "scanInt") == 0 || strcmp(expr->call->func, "LENGTH") == 0 || strcmp(expr->call->func, "FIRST") == 0 || strcmp(expr->call->func, "LAST") == 0)
					expr->valueType = Integer;
				else if (strcmp(expr->call->func, "scanBoolean") == 0)
					expr->valueType = Boolean;
				else if (strcmp(expr->call->func, "scanFloat") == 0)
					expr->valueType = Float;
				else if (strcmp(expr->call->func, "scanChar") == 0)
					expr->valueType = Character;
				else if (strcmp(expr->call->func, "scanString") == 0)
					expr->valueType = String;
			}
			expr_list_sem(expr->call->arguments);
			break;
		}
		case name_list:
		{
			name_list_sem(expr->list);
			expr->valueType = expr->list->first->type;
			break;
		}
		case assign:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (expr->left->valueType != expr->right->valueType)
				throw ("attempt to assign different types");
			else
				expr->valueType = expr->left->valueType;
			break;
		}
		case array_call:
		{
			if (is_in_local_vars(expr->call->func) != NULL)
				expr->valueType = is_in_local_vars(expr->call->func)->type;
			else if (is_in_field_vars(expr->call->func) != NULL)
				expr->valueType = make_type_from_description(is_in_field_vars(expr->call->func)->desc);
			else
			{
				if (strcmp(expr->call->func, "Float") == 0)
					expr->valueType = Float;
				else if (strcmp(expr->call->func, "Integer") == 0)
					expr->valueType = Integer;
				else if (strcmp(expr->call->func, "printInt") == 0 || strcmp(expr->call->func, "printFloat") == 0 || strcmp(expr->call->func, "printBoolean") == 0
						|| strcmp(expr->call->func, "printChar") == 0 || strcmp(expr->call->func, "printString") == 0 || strcmp(expr->call->func, "printLnInt") == 0 ||
						strcmp(expr->call->func, "printLnFloat") == 0 || strcmp(expr->call->func, "printLnBoolean") == 0 || strcmp(expr->call->func, "printLnChar") == 0 ||
						strcmp(expr->call->func, "printLnString") == 0 )
					expr->valueType = Void;
				else if (strcmp(expr->call->func, "scanInt") == 0 || strcmp(expr->call->func, "LENGTH") == 0 || strcmp(expr->call->func, "FIRST") == 0 || strcmp(expr->call->func, "LAST") == 0)
					expr->valueType = Integer;
				else if (strcmp(expr->call->func, "scanBoolean") == 0)
					expr->valueType = Boolean;
				else if (strcmp(expr->call->func, "scanFloat") == 0)
					expr->valueType = Float;
				else if (strcmp(expr->call->func, "scanChar") == 0)
					expr->valueType = Character;
				else if (strcmp(expr->call->func, "scanString") == 0)
					expr->valueType = String;
			}
			break;
			break;
		}
		case sum:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to sum different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case mul:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to mul different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case Div:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to div different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case sub:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to sub different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case grade:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to grade different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case greater:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to greater different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case equal:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to equal different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case less:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to less different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case not:
		{

			expr_sem(expr->left);
			operation_types_matches(expr);
			break;
		}
		case ne:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to not equal different types");;
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case ge:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to great equal different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case le:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to less equal different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case xor:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to xor different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case and:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to and different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case or :
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to or different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case concat:
		{
			expr_sem(expr->right);
			expr_sem(expr->left);
			if (!match_types(expr))
			{
				throw("attempt to concat different types");
			}
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case uminus:
		{
			expr_sem(expr->left);
			operation_types_matches(expr);
			expr->valueType = expr->left->valueType;
			break;
		}
		case c_i:
		{
			expr->valueType = Integer;
			break;
		}
		case c_d:
		{
			expr->valueType = Float;
			break;
		}
		case c_s:
		{
			expr->valueType = String;
			break;
		}
		case c_c:
		{
			expr->valueType = Character;
			break;
		}
		case c_b:
		{
			expr->valueType = Boolean;
			break;
		}
		}

		expr_sem(expr->next);

	}
}

void while_sem(struct NWhileLoop* loop)
{
	stmt_list_sem(loop->body);
	expr_sem(loop->condition);
}

void if_sem(struct NIfStruct * If)
{
	stmt_list_sem(If->ifStmt);
	expr_sem(If->ifExpr);
	if (If->elsifList != NULL)
		elsif_list_sem(If->elsifList);
	if (If->elseStmt != NULL)
		stmt_list_sem(If->elseStmt);
}

void elsif_list_sem(struct NElsifList * list)
{
	elsif_sem(list->first);
}

void elsif_sem(struct NElsif *elsif)
{
	if (elsif != NULL)
	{
		stmt_list_sem(elsif->body);
		expr_sem(elsif->elsifExpr);
		elsif_sem(elsif->next);
	}
}

void when_sem(struct NWhenStruct * whenS)
{
	if (whenS != NULL)
	{
		stmt_list_sem(whenS->body);
		expr_list_sem(whenS->whenExpr);
		when_sem(whenS->next);
	}
}

void when_list_sem(struct NWhenList * list)
{
	when_sem(list->first);
}

void func_sem(struct NFuncProc * func)
{
	stmt_list_sem(func->body);
}
void name_sem(struct NName*name)throw(char *)
{
	char  str[100] = "Unknown variable ";
	if (is_in_local_vars(name->name) != NULL)
		name->type = is_in_local_vars(name->name)->type;
	else if (is_in_field_vars(name->name) != NULL)
		name->type = make_type_from_description(is_in_field_vars(name->name)->desc);
	else
		throw(strcat(str, name->name));
	if (name->next != NULL)
		name_sem(name->next);
}

void name_list_sem(struct NNameList * list)
{
	if (list != NULL)
		name_sem(list->first);
}

struct LocalElement* is_in_local_vars(char*name)
{
	std::vector<struct LocalElement> *vars = current_method->localvars;
	int i;
	for (i = 0; i < vars->size(); i++)
	{
		if (stricmp(name, vars->at(i).name.c_str()) == 0)
			return &(vars->at(i));
	}
	return NULL;
}

struct FieldElement* is_in_field_vars(char*name)
{
	std::vector<struct FieldElement>* fields = parent_class->fields;
	for (int i = 0; i < fields->size(); i++)
	{
		if (stricmp(name, fields->at(i).name->str.c_str()) == 0)
			return &(fields->at(i));
	}
	return NULL;
}

//struct MethodElement* is_in_methods(char*name)
//{
//std::vector<struct MethodElement>* methods = parent_class->methods;
//for (int i = 0; i < methods->size(); i++)
//{
//	if (stricmp(name, methods->at(i).name->str.c_str()) == 0)
//		return &(methods->at(i));
//}
//return NULL;
//}

bool variable_exists(char*name)
{
	if (is_in_local_vars(name) != NULL)
	{
		return true;
	}
	else
	{
		if (is_in_field_vars(name) != NULL)
		{
			return true;
		}
		return false;
	}
}

enum NVarType return_type(struct NName*name)
{
	enum NVarType type;
	std::vector<struct FieldElement>* fields = parent_class->fields;
	for (int i = 0; i < fields->size(); i++)
	{
		if (stricmp(name->name, fields->at(i).name->str.c_str()) == 0)
			return make_type_from_description(fields->at(i).desc);
	}
	std::vector<struct LocalElement> *localvars = current_method->localvars;
	for (int i = 0; i < localvars->size(); i++)
	{
		if (stricmp(name->name, localvars->at(i).name.c_str()) == 0)
			return localvars->at(i).type;
	}
	type = Root_Integer;
	return type;
}

enum NVarType make_type_from_description(struct SemanticalElement * element)
{
	enum NVarType type;
	if (element->str == "I")
	{
		type = Integer;
	}
	else if (element->str == "Ljava/lang/String;")
	{
		type = String;
	}
	else if (element->str == "F")
	{
		type = Float;
	}
	else if (element->str == "[F")
	{
		type = Float;
	}
	else if (element->str == "[I")
	{
		type = Integer;
	}
	else if (element->str == "[Ljava/lang/String;")
	{
		type = String;
	}
	else if (element->str.find_last_of("V") == element->str.size()-1)
	{
		type = Void;
	}
	else if (element->str.find_last_of("F") == element->str.size() - 1)
	{
		type = Float;
	}
	else if (element->str.find_last_of("I") == element->str.size() - 1)
	{
		type = Integer;
	}
	else if(element->str.find_last_of("Ljava/lang/String;") == element->str.size() - 1)
	{
		type = String;
	}
	return type;
}

char* sem_type(enum NVarType type)
{
	char * result = (char *)malloc(100);
	if (type == Integer)
	{
		strcpy(result, "I");
	}
	else if (type == Root_Integer)
	{
		strcpy(result, "I");
	}
	else if (type == String)
	{
		strcpy(result, "Ljava/lang/String;");
	}
	else if (type == Float)
	{
		strcpy(result, "F");
	}
	else if (type == Boolean)
	{
		strcpy(result, "I");
	}
	else if (type == ENum)
	{
		strcpy(result, "I");
	}
	return result;
}

void types_matches(struct NName* first, struct NName* second)throw(char *)
{
	char str[100];

	if (first->type != second->type)
	{
		strcpy(str, "Type for variables mismatches: ");
		strcat(str, first->name);
		strcat(str, " and ");
		strcat(str, second->name);
		throw (str);
	}
}

void operation_types_matches(struct NExpression* expr) throw(char*)
{
	switch (expr->type)
	{
	case assign:
	{
		types_matches(expr->left->list->first, expr->right->list->first);
		break;
	}
	case mul:
	{
		if (expr->left->type == name_list &&expr->right->type == name_list)
		{
			if (expr->left->list->first->type == String || expr->right->list->first->type == String)
				throw("Operands can't be strings for operation mul");
			if (expr->left->list->first->type == ENum || expr->right->list->first->type == ENum)
				throw("Operands can't be enums for operation mul");
			if (expr->left->list->first->type == Character || expr->right->list->first->type == Character)
				throw("Operands can't be characters for operation mul");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;

	}
	case Div:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (expr->left->list->first->type == String || expr->right->list->first->type == String)
				throw("Operands can't be strings for operation div");
			if (expr->left->list->first->type == ENum || expr->right->list->first->type == ENum)
				throw("Operands can't be enums for operation div");
			if (expr->left->list->first->type == Character || expr->right->list->first->type == Character)
				throw("Operands can't be characters for operation div");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;

	}
	case sub:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (expr->left->list->first->type == String || expr->right->list->first->type == String)
				throw("Operands can't be strings for operation sub");
			if (expr->left->list->first->type == ENum || expr->right->list->first->type == ENum)
				throw("Operands can't be enums for operation sub");
			if (expr->left->list->first->type == Character || expr->right->list->first->type == Character)
				throw("Operands can't be characters for operation sub");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;

	}
	case grade:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (expr->left->list->first->type == String || expr->right->list->first->type == String)
				throw("Operands can't be strings for operation grade");
			if (expr->left->list->first->type == ENum || expr->right->list->first->type == ENum)
				throw("Operands can't be enums for operation grade");
			if (expr->left->list->first->type == Character || expr->right->list->first->type == Character)
				throw("Operands can't be characters for operation grade");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;

	}
	case sum:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (expr->left->list->first->type == String || expr->right->list->first->type == String)
				throw("Operands can't be strings for operation sum");
			if (expr->left->list->first->type == ENum || expr->right->list->first->type == ENum)
				throw("Operands can't be enums for operation sum");
			if (expr->left->list->first->type == Character || expr->right->list->first->type == Character)
				throw("Operands can't be characters for operation sum");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case greater:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (expr->left->list->first->type == ENum || expr->right->list->first->type == ENum)
				throw("Operands can't be enums for operation >");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case equal:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case less:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (expr->left->list->first->type == ENum || expr->right->list->first->type == ENum)
				throw("Operands can't be enums for operation <");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case not:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (!(expr->left->list->first->type == Boolean || expr->right->list->first->type == Boolean))
				throw("Operands can be only boolean for operation not");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case ne:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case ge:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case le:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case xor:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (!(expr->left->list->first->type == Boolean || expr->right->list->first->type == Boolean))
				throw("Operands can be only boolean for operation not");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case and:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (!(expr->left->list->first->type == Boolean || expr->right->list->first->type == Boolean))
				throw("Operands can be only boolean for operation not");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case or :
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (!(expr->left->list->first->type == Boolean || expr->right->list->first->type == Boolean))
				throw("Operands can be only boolean for operation not");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case concat:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (!(expr->left->list->first->type == String || expr->right->list->first->type == String))
				throw("Operands can be only strings for operation &&");
			types_matches(expr->left->list->first, expr->right->list->first);
		}
		break;
	}
	case uminus:
	{
		if (expr->left->type == name_list&&expr->right->type == name_list)
		{
			if (expr->left->list->first->type == String || expr->left->list->first->type == Character || expr->left->list->first->type == ENum || expr->left->list->first->type == Boolean)
				throw("Operands can be only float or integer for operation unary minus");
		}
		break;
	}
	}
}

//������� ��� ���������� ������ 

void fill_name_list_table(struct NNameList * list)
{
	if (list != NULL)
		fill_name_table(list->first);
}

void fill_name_table(struct NName* added)throw(char*)
{
	if (added != NULL)
	{
		*current_class = table_of_classes.at(1);
		bool found = false;
		char error[100];
		strcpy(error, "Undeclared variable ");
		for (int i = 0; i < current_method->localvars->size(); i++)
		{
			if (stricmp(current_method->localvars->at(i).name.c_str(), added->name) == 0)
			{
				found = true;
			}
		}
		strcpy(error, "Undeclared variable ");
		for (int i = 0; i < current_class->fields->size(); i++)
		{
			if (stricmp(current_class->fields->at(i).name->str.c_str(), added->name) == 0)
			{
				found = true;
			}
		}
		if (!found&&current_method->localvars->size() != 0)
		{
			strcat(error, added->name);
			throw(error);
		}
		fill_name_table(added->next);
	}

}

void fill_var_table(struct NVariable* var, bool isLocal)
{
	struct FieldElement* element = new struct FieldElement();
	struct SemanticalElement* added;
	if (isLocal)
	{
		add_to_local_table(var);
	}
	else
	{
		added = addFieldToTableConst(curClass, var->var_name, sem_type(var->type));
		element->name = added->second->first;
		element->desc = added->second->second;
		element->is_constant = var->isConstant;
		add_to_field_table(element);
	}
	if (var->value != NULL)
	{
		fill_expr_table(var->value);
	}
	if (var->next != NULL)
	{
		fill_var_table(var->next, isLocal);
	}
}

void fill_var_list_table(struct NVariableList * list, bool isLocal)
{
	if (list != NULL)
	{
		fill_var_table(list->first, isLocal);
	}
}

void fill_expr_table(struct NExpression* expr) throw(char *)
{
	switch (expr->type)
	{
	case name_list:
	{
		fill_name_list_table(expr->list);
		break;
	}
	case assign:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case array_call:
	{

		break;
	}
	case func:
	{
		fill_func_call_table(expr->call);
		if (expr->call->type == a_call)
			expr->index = expr->call->arguments->first;
		break;
	}
	case sum:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case mul:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case Div:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case sub:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case grade:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case greater:
	{
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case equal:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case less:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case not:
	{
		fill_expr_table(expr->left);
		break;
	}
	case ne:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case ge:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case le:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case xor:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case and:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case or :
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case concat:
	{
		expr_sem(expr->left);
		expr_sem(expr->right);
		fill_expr_table(expr->left);
		fill_expr_table(expr->right);
		break;
	}
	case uminus:
	{
		fill_expr_table(expr->left);
		break;
	}
	case c_i:
	{
		addIntToConstTable(expr->const_int);
		expr->ident = table_of_const.size();
		break;
	}
	case c_d:
	{
		addFloatToConstTable(expr->const_double);
		expr->ident = table_of_const.size();
		break;
	}
	case c_s:
	{
		addStringToConstTable(expr->const_string);
		expr->ident = table_of_const.size();
		break;
	}
	case c_c:
	{
		addIntToConstTable(expr->const_char);
		expr->ident = table_of_const.size();
		break;
	}
	case c_b:
	{
		addIntToConstTable(expr->const_bool);
		expr->ident = table_of_const.size();
		break;
	}
	}

	if (expr->next != NULL)
	{
		fill_expr_table(expr->next);
	}
}

void fill_expr_list_table(struct NExprList* list) throw(char *)
{
	if (list != NULL)
	{
		fill_expr_table(list->first);
	}
}

void fill_stmt_table(struct NStatement * stmt)
{
	switch (stmt->type)
	{
	case array_var:
	{
		fill_array_var_table(stmt->arr_var);
		expr_list_sem(stmt->arr_var->leftSizeExpr);
		expr_list_sem(stmt->arr_var->rightSizeExpr);
		fill_expr_list_table(stmt->arr_var->leftSizeExpr);
		fill_expr_list_table(stmt->arr_var->rightSizeExpr);
		break;
	}
	case return_:
	{
		
		expr_sem(stmt->return_st->returnValue);
		fill_expr_table(stmt->return_st->returnValue);
		break;
	}
	case array_assign:
	{
		fill_array_assign_table(stmt->array_assign);
		break;
	}
	case var_list_assign:
	{
		if (strcmp(current_method->name->str.c_str(), "main") == 0)
			fill_var_list_table(stmt->list->variables, false);
		else
			fill_var_list_table(stmt->list->variables, true);
		break;
	}
	case expr:
	{
		fill_expr_list_table(stmt->expressions);
		break;
	}

	case func_call:
	{
		expr_sem(stmt->return_st->returnValue);
		fill_func_call_table(stmt->func_call);
		break;
	}
	case if_:
	{
		fill_if_table(stmt->if_struct);
		break;
	}
	case case_:
	{
		fill_case_table(stmt->case_st);
		break;
	}
	case for_:
	{
		fill_for_table(stmt->for_loop);
		break;
	}
	case while_:
	{
		fill_while_table(stmt->while_loop);
		break;
	}
	case func_pr:
	{
		fill_func_proc_table(stmt->func_proc);
		break;
	}
	case en:
	{
		fill_enum_table(stmt->enu);
	}
	}
	if (stmt->next != NULL)
	{
		fill_stmt_table(stmt->next);
	}
}

void fill_enum_table(struct NVariable * var)
{
	struct LocalElement* element = new LocalElement();
	element->id = current_method->localvars->size() + 1;
	element->isConst = 0;
	struct NNameList * list;
	struct NName * expr;
	struct NName * name;
	int count = 0;
	list = var->list;
	expr = list->first;
	while (expr != NULL)
	{
		struct NExpression * val = new NExpression();
		val->type = c_i;
		val->const_int = count;
		struct NVariable * variable = new NVariable();
		variable->isConstant = true;
		variable->type = Integer;
		variable->var_name = expr->name;
		element->name = variable->var_name;
		element->type = variable->type;
		variable->value = val;
		int i;
		for (i = 0; i < current_method->localvars->size(); i++)
		{
			if (current_method->localvars->at(i).name == element->name)
			{
				throw ("Repeated local variable");
			}
		}
		current_method->localvars->insert(current_method->localvars->end(), *element);
		addIntToConstTable(count);
		count++;
		expr = expr->next;
	}
}

void fill_stmt_list_table(struct NStatementList * list)
{
	if (list != NULL)
	{
		fill_stmt_table(list->first);
	}
}

void fill_if_table(struct NIfStruct*If)
{
	fill_expr_table(If->ifExpr);
	fill_stmt_list_table(If->ifStmt);
	fill_elsif_list_table(If->elsifList);
	if (If->elseStmt != NULL)
		fill_stmt_list_table(If->elseStmt);
}

void fill_elsif_list_table(struct NElsifList*list)
{
	if (list != NULL)
	{
		fill_elsif_table(list->first);
	}
}

void fill_elsif_table(struct NElsif * elsif)
{
	if (elsif != NULL)
	{
		fill_expr_table(elsif->elsifExpr);
		fill_stmt_list_table(elsif->body);
		fill_elsif_table(elsif->next);
	}

}

void fill_for_table(struct NForLoop* for_)throw(char*)
{
	fill_name_list_table(for_->condition);
	fill_expr_table(for_->begin);
	fill_expr_table(for_->end);
	fill_stmt_list_table(for_->body);
}

void fill_while_table(struct NWhileLoop* while_)
{
	fill_expr_table(while_->condition);
	fill_stmt_list_table(while_->body);
}

void fill_array_var_table(struct NArrayVar * variable)throw (char*)
{
	struct LocalElement* lelement = new LocalElement();
	struct FieldElement* felement = new FieldElement();
	expr_list_sem(variable->leftSizeExpr);
	expr_list_sem(variable->rightSizeExpr);
	expr_list_sem(variable->expression);

	if (strcmp(current_method->name->str.c_str(), "main") == 0)
	{
		*current_class = table_of_classes.at(1);
		char desc[100];
		struct SemanticalElement* added;
		switch (variable->elem_type)
		{
		case Integer:
		{
			strcpy(desc, "[I");
			break;
		}
		case Float:
		{
			strcpy(desc, "[F");
			break;
		}
		case Character:
		{
			strcpy(desc, "[I");
			break;
		}
		case Boolean:
		{
			strcpy(desc, "[I");
			break;
		}
		case String:
		{
			strcpy(desc, "[Ljava/lang/String;");
			break;
		}
		}
		added = addFieldToTableConst(curClass, variable->arr_name, desc);
		felement->name = added->second->first;
		felement->desc = added->second->second;
		felement->leftSizeExpr = variable->leftSizeExpr;
		felement->rightSizeExpr = variable->rightSizeExpr;
		
		int i;
		for (i = 0; i < current_class->fields->size(); i++)
		{
			if (current_class->fields->at(i).name == felement->name)
			{
				throw ("Repeated global vars");
			}
		}
		add_to_field_table(felement);
	}
	else
	{
		lelement->id = current_method->localvars->size() + 1;
		lelement->name = variable->arr_name;
		lelement->type = Array;
		lelement->leftSize = variable->leftSize;
		lelement->rightSize = variable->rightSize;
		lelement->leftSizeExpr = variable->leftSizeExpr;
		lelement->rightSizeExpr = variable->rightSizeExpr;
		lelement->elem_type = variable->elem_type;
		int i;
		for (i = 0; i < current_method->localvars->size(); i++)
		{
			if (current_method->localvars->at(i).name == lelement->name)
			{
				throw ("Repeated local arrays");
			}
		}
	}
	current_method->localvars->insert(current_method->localvars->end(), *lelement);
	addIntToConstTable(1);
	addIntToConstTable(variable->rightSize - variable->leftSize);
	if (variable->expression != NULL)
	{
		fill_expr_list_table(variable->expression);
	}

}

void fill_array_assign_table(struct NArrayAssign * assign)
{

	if (assign->from_left != assign->to_left)
	{
		addIntToConstTable(assign->from_left);
		addIntToConstTable(assign->to_left);
	}
	else
	{
		addIntToConstTable(assign->from_left);
	}
	if (assign->from_right != -1 && assign->to_left != -1)
	{
		if (assign->from_right != assign->to_right)
		{
			addIntToConstTable(assign->from_right);
			addIntToConstTable(assign->to_right);
		}
		else
		{
			addIntToConstTable(assign->from_right);
		}
	}
	if (assign->expr != NULL)
	{
		fill_expr_list_table(assign->expr);
	}
}

void fill_func_call_table(struct NFuncCall * call)throw (char*)
{
	bool found = false;
	for (int i = 0; i < table_of_const.size(); i++)
	{
		if (table_of_const.at(i)->str == call->func)
		{
			found = true;
		}
	}
	for (int i = 0; i < table_of_const.size(); i++)
	{
		if (table_of_const.at(i)->str == call->func)
		{
			found = true;
		}
	}
	if (variable_exists(call->func))
	{
		call->type = a_call;
		found = true;
	}
	if (is_in_field_vars(call->func))
	{
		call->type = a_call;
		found = true;
	}
	if (!found)
	{
		if (!((stricmp(call->func, "Float") == 0) || (stricmp(call->func, "Integer") == 0)
			|| (stricmp(call->func, "printInt") == 0) || (stricmp(call->func, "printFloat") == 0) || (stricmp(call->func, "printBoolean") == 0)
			|| (stricmp(call->func, "printChar") == 0) || (stricmp(call->func, "printString") == 0) || (stricmp(call->func, "printLnInt") == 0)
			|| (stricmp(call->func, "printLnFloat") == 0) || (stricmp(call->func, "printLnBoolean") == 0) || (stricmp(call->func, "printLnChar") == 0)
			|| (stricmp(call->func, "printLnString") == 0) || (stricmp(call->func, "scanInt") == 0) || (stricmp(call->func, "scanBoolean") == 0)
			|| (stricmp(call->func, "scanFloat") == 0) || (stricmp(call->func, "scanChar") == 0) || (stricmp(call->func, "scanString") == 0)
			|| (stricmp(call->func, "LENGTH") == 0) || (stricmp(call->func, "FIRST") == 0) || (stricmp(call->func, "LAST") == 0)))
		{
			char str[100];
			strcpy(str, "Can't call undefined function");
			strcat(str, call->func);
			throw(str);
		}
	}

	char res[20];
	if (call->arguments != NULL)
	{
		if (call->type != a_call)
		{
			if (call->arguments->first->type == func)
				fill_func_call_table(call->arguments->first->call);
			make_desc_from_args(call, call->func);
		}
		fill_expr_list_table(call->arguments);
	}
}

struct MethodElement* is_in_methods(char*name)
{
	std::vector<struct MethodElement> *meths = parent_class->methods;
	int i;
	for (i = 0; i < meths->size(); i++)
	{
		if (stricmp(name, meths->at(i).name->str.c_str()) == 0)
			return &(meths->at(i));
	}
	return NULL;

}


char * make_desc_from_args(struct NFuncCall * args, char * Name) throw(char*)
{
	char result[100];
	strcpy(result, "(");
	struct NExpression * expr;
	enum NVarType type;
	struct NName * name;
	char res2[128];
	expr = args->arguments->first;
	while (expr != NULL)
	{
		if (((expr->type == c_i) || (expr->type == c_d) || (expr->type == c_b) || (expr->type == c_s) || (expr->type == c_c)))
		{
			switch (expr->type)
			{
			case c_i:
			{
				type = Integer;
				break;
			}
			case c_c:
			{
				type = Character;
				break;
			}
			case c_s:
			{
				type = String;
				break;
			}
			case c_b:
			{
				type = Boolean;
				break;
			}
			case c_d:
			{
				type = Float;
				break;
			}
			}
		}
		else if (expr->type == name_list)
		{
			type = return_type(expr->list->first);
		}
		else if (expr->type == func)
		{
			if (expr->call->type == a_call)
			{
				if (is_in_local_vars(expr->call->func) != NULL)
					type = is_in_local_vars(expr->call->func)->type;
				else if (is_in_field_vars(expr->call->func) != NULL)
					type = Array;
			}
			else
			{

			}
		}
		else
		{
			try
			{
				type = expr->valueType;
			}
			catch (...)
			{
				switch (expr->left->type)
				{
				case c_i:
				{
					type = Integer;
					break;
				}
				case c_c:
				{
					type = Character;
					break;
				}
				case c_s:
				{
					type = String;
					break;
				}
				case c_b:
				{
					type = Boolean;
					break;
				}
				case c_d:
				{
					type = Float;
					break;
				}
				}
			}
		}
		switch (type)
		{
		case Integer:
		{
			strcat(result, "I");
			strcpy(res2, "I");
			break;
		}
		case Boolean:
		{
			strcat(result, "I");
			strcpy(res2, "I");
			break;
		}
		case Character:
		{
			strcat(result, "I");
			strcpy(res2, "I");
			break;
		}
		case String:
		{
			strcat(result, "Ljava/lang/String;");
			strcpy(res2, "Ljava/lang/String;");
			break;
		}
		case Float:
		{
			strcat(result, "F");
			strcpy(res2, "F");
			break;
		}
		case ENum:
		{
			strcat(result, "I");
			strcpy(res2, "I");
			break;
		}
		}
		expr = expr->next;
	}
	strcat(result, ")");
	char desc[200];
	bool found = false;
	strcpy(desc, "");
	char temp[200];
	for (int i = 0; i < parent_class->methods->size(); i++)
	{
		if (stricmp(parent_class->methods->at(i).name->str.c_str(), args->func) == 0)
		{
			found = true;
			strcpy(desc, parent_class->methods->at(i).desc->str.c_str());
		}
	}
	if (!found)
	{
		return "";
	}
	if (stricmp(Name, "Float") == 0)
	{
		strcat(result, "F");
	}
	else if (stricmp(Name, "Integer") == 0)
	{
		strcat(result, "I");
	}
	else
	{
		strcpy(temp, strchr(desc, ')'));
		int i;
		for (i = 0; i < strlen(temp - 1); i++)
		{
			temp[i] = temp[i + 1];
		}
		temp[i] = '\0';
		strcat(result, temp);
		if (strcmp(result, desc) != 0)
		{
			strcpy(temp, "Parameters in function calling doesn't match with declaration in function ");
			strcat(temp, args->func);
			throw(temp);
		}
	}
	return result;
}

void fill_func_proc_table(struct NFuncProc * func_proc)
{
	struct SemanticalElement* added = new SemanticalElement();
	char  desc[100];
	if (!func_proc->isFunc)
		func_proc->type = Void;

	strcpy(desc, make_descriptor_for_function(func_proc->arguments, func_proc->type));
	if (parent_class->methods->size() == 0)
	{
		func_proc->name = "main";
		strcpy(desc, "([Ljava/lang/String;)V");
	}
	added = addMethodToConstTable(curClass, func_proc->name, desc);
	struct MethodElement* method = new struct MethodElement();

	char name[5];
	method1 = current_method;
	method->desc = added->second->second;
	method->name = added->second->first;
	current_method = method;
	parent_class->methods->insert(parent_class->methods->end(), *method);
	if (func_proc->arguments != NULL)
		fill_func_args_table(func_proc->arguments);

	fill_stmt_list_table(func_proc->localVars);
	fill_stmt_list_table(func_proc->body);
	stmt_list_sem(func_proc->localVars);
	stmt_list_sem(func_proc->body);

	if (method1->name != NULL)
		current_method = method1;
}

void fill_func_args_table(struct NFuncArgs * args)
{
	if (args != NULL)
	{
		fill_var_list_table(args->first->variables, true);
		if (args->first->value != NULL)
		{
			fill_expr_table(args->first->value);
		}
	}

}

char* make_descriptor_for_function(struct NFuncArgs * arguments, enum NVarType type)
{
	char result[100]; 
	strcpy(result, "(");
	struct NVarListAssign * list = new NVarListAssign();
	struct NVariableList * varlist = new NVariableList();
	struct NVariable * var = new NVariable();
	if (arguments == NULL)
		strcat(result, "");
	else
	{
		list = arguments->first;
		while (list != NULL)
		{
			var = list->variables->first;
			while (var != NULL)
			{
				switch (var->type)
				{
				case Float:
				{
					strcat(result, "F");
					break;
				}
				case Integer:
				{
					strcat(result, "I");
					break;
				}
				case Root_Integer:
				{
					strcat(result, "I");
					break;
				}
				case Character:
				{
					strcat(result, "I");
					break;
				}
				case Boolean:
				{
					strcat(result, "I");
					break;
				}
				case String:
				{
					strcat(result, "Ljava/lang/String;");
					break;
				}
				case ENum:
				{
					strcat(result, "I");
					break;
				}
				}
				var = var->next;

			}
			list = list->next;
		}
	}
	strcat(result, ")");
	if (type != Void)
	{
		switch (type)
		{
		case Float:
		{
			strcat(result, "F");
			break;
		}
		case Integer:
		{
			strcat(result, "I");
			break;
		}
		case Root_Integer:
		{
			strcat(result, "I");
			break;
		}
		case Character:
		{
			strcat(result, "I");
			break;
		}
		case Boolean:
		{
			strcat(result, "I");
			break;
		}
		case String:
		{
			strcat(result, "Ljava/lang/String;");
			break;
		}
		case ENum:
		{
			strcat(result, "I");
			break;
		}
		}

	}
	else
		strcat(result, "V");

	return result;
}

void fill_case_table(struct NCaseStruct * Case)
{
	fill_when_list_table(Case->body);
}

void fill_when_list_table(struct NWhenList* list)
{
	if (list != NULL)
	{
		fill_when_table(list->first);
	}
}

void fill_when_table(struct NWhenStruct * When)
{
	if (When != NULL)
	{
		fill_expr_list_table(When->whenExpr);
		fill_stmt_list_table(When->body);
		fill_when_table(When->next);
	}
}