
/*  A Bison parser, made from c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y with Bison version GNU Bison version 1.24
  */

#define YYBISON 1  /* Identify Bison output.  */

#define	CONST_STRING	258
#define	CONST_INT	259
#define	CONST_BOOL	260
#define	CONST_DOUBLE	261
#define	CONST_CHAR	262
#define	NAME	263
#define	ABORT	264
#define	ABS	265
#define	ABSTRACT	266
#define	ACCEPT	267
#define	ALIASED	268
#define	ALL	269
#define	AND	270
#define	AT	271
#define	BEGGIN	272
#define	BODY	273
#define	CASE	274
#define	CONSTANT	275
#define	DECLARE	276
#define	DELAY	277
#define	DELTA	278
#define	DIGITS	279
#define	DO	280
#define	ELSE	281
#define	ELSIF	282
#define	END	283
#define	ENTRY	284
#define	EXCEPTION	285
#define	EXIT	286
#define	FOR	287
#define	FUNCTION	288
#define	GENERIC	289
#define	IF	290
#define	IN	291
#define	IS	292
#define	LIMITED	293
#define	LOOP	294
#define	MOD	295
#define	NEW	296
#define	NOT	297
#define	NOLL	298
#define	OF	299
#define	OR	300
#define	OTHERS	301
#define	OUT	302
#define	PRAGMA	303
#define	PRIVATE	304
#define	PROCEDURE	305
#define	FIRST	306
#define	LAST	307
#define	LENGTH	308
#define	PROTECTED	309
#define	RAISE	310
#define	RANGE	311
#define	RENAMES	312
#define	REQUEUE	313
#define	RETURN	314
#define	REVERSE	315
#define	SELECT	316
#define	SEPARATE	317
#define	SUBTYPE	318
#define	TERMINATE	319
#define	THEN	320
#define	TYPE	321
#define	UNTIL	322
#define	USE	323
#define	WHEN	324
#define	WHILE	325
#define	XOR	326
#define	GE	327
#define	LE	328
#define	NE	329
#define	ARRAYRANGE	330
#define	ASSIGN	331
#define	ENSUE	332
#define	GRADE	333
#define	REALTYPE	334
#define	ROOTINTTYPE	335
#define	INTTYPE	336
#define	CHARTYPE	337
#define	BOOLTYPE	338
#define	ENUMTYPE	339
#define	ARRAYTYPE	340
#define	FLOATTYPE	341
#define	STRINGTYPE	342
#define	RECORDTYPE	343
#define	FUNCTIONTYPE	344
#define	UMINUS	345

#line 1 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"

#include "tree_structures.h"
#include <stdio.h>
#include <malloc.h>

struct NStatementList *Root;
void yyerror(const char *s);
extern int yylex(void);

struct NExpression* create_str_expr(char*value);
struct NExpression* create_int_expr(int value);
struct NExpression* create_double_expr(double value);
struct NExpression* create_bool_expr(int value);
struct NExpression* create_char_expr(char value);
struct NExpression* create_empty_expr();
struct NExpression* create_func_call_expr(struct NFuncCall * call);
struct NExpression* create_name_expr(struct NName * name);
struct NExpression* create_binary_expression(struct NExpression* Left, struct NExpression* Right, enum NExprType type);
struct NExpression* create_binary_expression2(struct NExpression* Left, struct NExpression* Right, enum NExprType type, enum NVarType assign);
struct NExpression* create_unary_expression(struct NExpression* Left,enum NExprType type);
struct NVariable* create_variable(char*name,enum NVarType type,struct NExpression*expr);
struct NVariable* create_enum_variable(struct NExpression*exlist,struct NExprList*list);
struct NVariable* create_default_variable(char*name);
struct NExprList* create_expr_list(struct NExpression*first);
void add_expr_to_list(struct NExprList*list,struct NExpression*added);
struct NVariableList* create_var_list(char*first);
void add_var_to_list(struct NVariableList*list,char*added);
void change_varlist_const(struct NVariableList * list, int isConst);
struct NVarListAssign* create_var_list_assign(struct NExprList*list, struct NExpression* value, enum NVarType type,int  isConst);
struct NStatement * create_expr_statement(struct NExprList* exp);
struct NStatement * create_enum_statement(struct NVariable*exp);
struct NStatement * create_for_statement(struct NForLoop* exp);
struct NStatement * create_while_statement(struct NWhileLoop* exp);
struct NStatement * create_case_statement(struct NCaseStruct* exp);
struct NStatement * create_if_statement(struct NIfStruct* exp);
struct NStatement * create_func_statement(struct NFuncProc* exp);
struct NStatement * create_call_statement(struct NFuncCall* exp);

struct NStatement * create_arr_statement(struct NArrayAssign* arr);
struct NStatement * create_arr_var(struct NArrayVar* arr);
struct NStatement * create_list_statement(struct NVarListAssign* list);
struct NStatementList * create_statement_list(struct NStatement* exp);
void add_statement_to_list(struct NStatementList*list,struct NStatement*statement);

struct NName* create_name(char*name);
struct NNameList* create_name_list(struct NName* name);
void add_name_to_list (struct NNameList* list, struct NName*added);

struct NArrayVar* create_array_var(struct NExprList* name, struct NExprList* leftSize, struct NExprList* rightSize, enum NVarType type, struct NExprList * list);
struct NArrayAssign* create_array_assign1(struct NExpression*first_arr,struct NExpression* second_arr, struct NExprList*from_f, struct NExprList*to_f, struct NExprList*from_s, struct NExprList*to_s);
struct NArrayAssign* create_array_assign2(struct NExpression*first_arr, struct NExprList* from_f, struct NExprList*to_f, struct NExprList * expr);
void change_varlist_type(struct NVariableList* list, enum NVarType type,struct NExpression* value);

struct NWhenStruct* create_when(struct NExprList * whenExpr, struct NStatementList * body);
struct NWhenList* create_when_list(struct NWhenStruct* whenStr);
void add_when_to_list(struct NWhenList* list, struct NWhenStruct * added);
struct NElsif* create_elsif(struct NExpression * expr, struct NStatementList*body);
struct NElsifList * create_elsif_list(struct NElsif*elsif);
struct NIfStruct * create_if(struct NStatementList * if_stmt,struct NExpression * if_expr, struct NElsifList * elsif_list,struct  NStatementList * else_stmt);
struct NCaseStruct * create_case(struct NExprList*var, struct NWhenList*body);
void add_elsif_to_list(struct NElsifList* list, struct NElsif * elsif);
struct NWhileLoop * create_while(struct NExprList*expr, struct NStatementList*body, int doWh);
struct NForLoop * create_for(int inRev, struct NExprList* beg,struct NExprList* end, struct NExprList*expr,struct  NStatementList*body);
struct NFuncProc * create_func (struct NExpression*name,struct NFuncArgs*args,struct NStatementList * body,struct NStatementList*vars, struct NExprList * ret, enum NVarType type,int pr);
struct NFuncProc * create_proc (struct NExpression*name,struct NFuncArgs*args,struct NStatementList * body,struct NStatementList*vars,int pr);
struct NFuncArgs * create_args(struct NVarListAssign*expr);
void add_arg_to_list(struct NFuncArgs*list, struct NVarListAssign* added);
struct NFuncCall * create_func_call(enum NFuncType type, struct NExprList*args, struct NExpression*name);
struct NException * create_exc(struct NExcBodyList * body);
struct NStatement * create_exc_statement(struct NException * exc);
struct NStatement * create_return_statement(struct NExpression* return_val);
struct NExcBody * create_exc_body(struct NStatementList * body, struct NExprList*name);
void add_exc_to_list(struct NExcBodyList*list, struct NExcBody* added);
struct NExcBodyList*create_exc_body_list(struct NExcBody*body);
int listnumber = 0;
int namenumber = 0;

#line 78 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
typedef union{
	int ConstInt;
	double ConstDouble;
	char* ConstString;
	char * VarName;
	char ConstChar;
	int ConstBool;
	struct NStatement * Statement;
	struct NStatementList * StatementList;
	struct NExpression * Expression;
	struct NException * Exception;
	struct NNameList * NameList;
	struct NName * Name;
	struct NExcBody * ExcBody;
	struct NExcBodyList* BodyList;
	struct NFuncArgs * FuncArgs;
	struct NExprList *ExprList;
	struct NArrayVar *ArrayVar;
	struct NFuncProc * FuncProc;
	struct NArrayAssign * ArrayAssign;
	struct NVarListAssign * VarListAssign;
	struct NWhenList * WhenList;
	struct NElsifList * ElsifList;
	struct NVariable * Variable;
	struct NVariable * ENum;
	struct NForLoop *ForLoop;
	struct NFuncCall * FuncCall;
	struct NVariableList * VariableList;
	enum NVarType VarType;
	enum NArrType ArrType;
	struct NIfStruct * IfStruct;
	struct NElsif * Elseif;
	struct NWhenStruct * WhenStruct;
	struct NCaseStruct * CaseStruct;
	struct NWhileLoop * WhileLoop;
	struct NStatementList *Program;
} YYSTYPE;

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		327
#define	YYFLAG		-32768
#define	YYNTBASE	105

#define YYTRANSLATE(x) ((unsigned)(x) <= 345 ? yytranslate[x] : 128)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,    90,    99,   101,
   100,    96,    94,   103,    95,     2,    97,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,   102,   104,    93,
    91,    92,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
    36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
    46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
    56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
    66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
    76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
    86,    87,    88,    89,    98
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     4,     6,     8,    10,    12,    14,    16,    18,
    29,    44,    55,    69,    76,    78,    82,    88,    92,    98,
   103,   110,   115,   122,   129,   133,   139,   143,   147,   151,
   156,   160,   164,   168,   172,   176,   180,   184,   188,   192,
   196,   200,   203,   207,   210,   212,   214,   216,   218,   220,
   222,   224,   230,   236,   242,   248,   254,   260,   266,   272,
   278,   284,   289,   295,   300,   302,   304,   308,   311,   314,
   317,   320,   323,   326,   329,   332,   335,   338,   342,   344,
   347,   352,   354,   357,   365,   375,   382,   391,   396,   401,
   403,   406,   413,   420,   429,   440,   452,   463,   475,   482,
   490,   499,   507,   520,   534,   544,   555,   558,   564,   572,
   577,   579,   583,   588,   592,   596,   600
};

static const short yyrhs[] = {   116,
     0,    81,     0,    82,     0,    83,     0,    87,     0,    88,
     0,    84,     0,    86,     0,    85,     0,   114,   102,   107,
   101,   114,    75,   114,   100,    44,   106,     0,   114,   102,
   107,   101,   114,    75,   114,   100,    44,   106,    76,   101,
   114,   100,     0,   113,   101,   114,    75,   114,   100,    76,
   101,   114,   100,     0,   113,   101,   114,    75,   114,   100,
    76,   113,   101,   114,    75,   114,   100,     0,   113,   101,
   114,   100,    76,   114,     0,     8,     0,   114,   102,   106,
     0,   114,   102,   106,    76,   113,     0,   114,   102,   110,
     0,   114,   102,   110,    76,   113,     0,   114,   102,    20,
   106,     0,   114,   102,    20,   106,    76,   113,     0,   114,
   102,    20,   110,     0,   114,   102,    20,   110,    76,   113,
     0,    66,   113,    37,   101,   114,   100,     0,   113,    94,
   113,     0,   101,   113,    94,   113,   100,     0,   113,    95,
   113,     0,   113,    96,   113,     0,   113,    76,   113,     0,
   106,   101,   113,   100,     0,   113,    91,   113,     0,   113,
    78,   113,     0,   113,    97,   113,     0,   113,    93,   113,
     0,   113,    92,   113,     0,   113,    72,   113,     0,   113,
    73,   113,     0,   113,    74,   113,     0,   113,    71,   113,
     0,   113,    15,   113,     0,   113,    45,   113,     0,    42,
   113,     0,   113,    90,   113,     0,    95,   113,     0,     4,
     0,     3,     0,     5,     0,     6,     0,     7,     0,    43,
     0,   110,     0,   101,   113,    95,   113,   100,     0,   101,
   113,    96,   113,   100,     0,   101,   113,    93,   113,   100,
     0,   101,   113,    92,   113,   100,     0,   101,   113,    72,
   113,   100,     0,   101,   113,    73,   113,   100,     0,   101,
   113,    74,   113,   100,     0,   101,   113,    71,   113,   100,
     0,   101,   113,    15,   113,   100,     0,   101,   113,    45,
   113,   100,     0,   101,    42,   113,   100,     0,   101,   113,
    90,   113,   100,     0,   101,    95,   113,   100,     0,   127,
     0,   113,     0,   114,   103,   113,     0,   114,   104,     0,
   111,   104,     0,   108,   104,     0,   109,   104,     0,   119,
   104,     0,   125,   104,     0,   123,   104,     0,   124,   104,
     0,   122,   104,     0,   112,   104,     0,    59,   114,   104,
     0,   115,     0,   116,   115,     0,    27,   113,    65,   116,
     0,   117,     0,   118,   117,     0,    35,   113,    65,   116,
   118,    28,    35,     0,    35,   113,    65,   116,   118,    26,
   116,    28,    35,     0,    35,   113,    65,   116,    28,    35,
     0,    35,   113,    65,   116,    26,   116,    28,    35,     0,
    69,   114,    77,   116,     0,    69,    46,    77,   116,     0,
   120,     0,   121,   120,     0,    19,   114,    37,   121,    28,
    19,     0,    70,   114,    39,   116,    28,    39,     0,    39,
   116,    31,    69,   114,   104,    28,    39,     0,    32,   114,
    36,   114,    75,   114,    39,   116,    28,    39,     0,    32,
   114,    36,    60,   114,    75,   114,    39,   116,    28,    39,
     0,    50,   113,   101,   126,   100,    37,    17,   116,    28,
   113,     0,    50,   113,   101,   126,   100,    37,   116,    17,
   116,    28,   113,     0,    50,   113,    37,    17,    28,   113,
     0,    50,   113,    37,    17,   116,    28,   113,     0,    50,
   113,    37,   116,    17,   116,    28,   113,     0,    50,   113,
    37,   116,    17,    28,   113,     0,    33,   113,   101,   126,
   100,    59,   106,    37,    17,   116,    28,   113,     0,    33,
   113,   101,   126,   100,    59,   106,    37,   116,    17,   116,
    28,   113,     0,    33,   113,    59,   106,    37,    17,   116,
    28,   113,     0,    33,   113,    59,   106,    37,   116,    17,
   116,    28,   113,     0,    50,   113,     0,    50,   113,   101,
   126,   100,     0,    33,   113,   101,   126,   100,    59,   106,
     0,    33,   113,    59,   106,     0,   111,     0,   126,   104,
   111,     0,   113,   101,   114,   100,     0,   113,   101,   100,
     0,   113,    99,    51,     0,   113,    99,    52,     0,   113,
    99,    53,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
   239,   241,   242,   243,   244,   245,   246,   247,   250,   252,
   253,   256,   257,   258,   260,   263,   264,   265,   266,   267,
   268,   269,   270,   274,   276,   277,   278,   279,   280,   281,
   282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
   292,   293,   294,   295,   296,   297,   298,   299,   300,   301,
   302,   303,   304,   305,   306,   307,   308,   309,   310,   311,
   312,   313,   314,   315,   316,   318,   319,   321,   322,   323,
   324,   325,   326,   327,   328,   329,   330,   331,   334,   335,
   338,   340,   341,   343,   345,   346,   347,   350,   351,   354,
   355,   358,   360,   361,   364,   365,   368,   369,   371,   372,
   373,   374,   376,   377,   379,   380,   383,   384,   386,   387,
   390,   391,   393,   394,   395,   396,   397
};

static const char * const yytname[] = {   "$","error","$undefined.","CONST_STRING",
"CONST_INT","CONST_BOOL","CONST_DOUBLE","CONST_CHAR","NAME","ABORT","ABS","ABSTRACT",
"ACCEPT","ALIASED","ALL","AND","AT","BEGGIN","BODY","CASE","CONSTANT","DECLARE",
"DELAY","DELTA","DIGITS","DO","ELSE","ELSIF","END","ENTRY","EXCEPTION","EXIT",
"FOR","FUNCTION","GENERIC","IF","IN","IS","LIMITED","LOOP","MOD","NEW","NOT",
"NOLL","OF","OR","OTHERS","OUT","PRAGMA","PRIVATE","PROCEDURE","FIRST","LAST",
"LENGTH","PROTECTED","RAISE","RANGE","RENAMES","REQUEUE","RETURN","REVERSE",
"SELECT","SEPARATE","SUBTYPE","TERMINATE","THEN","TYPE","UNTIL","USE","WHEN",
"WHILE","XOR","GE","LE","NE","ARRAYRANGE","ASSIGN","ENSUE","GRADE","REALTYPE",
"ROOTINTTYPE","INTTYPE","CHARTYPE","BOOLTYPE","ENUMTYPE","ARRAYTYPE","FLOATTYPE",
"STRINGTYPE","RECORDTYPE","FUNCTIONTYPE","'&'","'='","'>'","'<'","'+'","'-'",
"'*'","'/'","UMINUS","'\\''","')'","'('","':'","','","';'","program","var_type",
"arr_type","array_var","array_assign","name","var_list_assign","enum_type","expression",
"expr_list","statement","statement_list","else_if","else_if_list","if_struct",
"when_struct","when_list","case_struct","while_loop","for_loop","func_proc",
"func_args","func_call",""
};
#endif

static const short yyr1[] = {     0,
   105,   106,   106,   106,   106,   106,   106,   106,   107,   108,
   108,   109,   109,   109,   110,   111,   111,   111,   111,   111,
   111,   111,   111,   112,   113,   113,   113,   113,   113,   113,
   113,   113,   113,   113,   113,   113,   113,   113,   113,   113,
   113,   113,   113,   113,   113,   113,   113,   113,   113,   113,
   113,   113,   113,   113,   113,   113,   113,   113,   113,   113,
   113,   113,   113,   113,   113,   114,   114,   115,   115,   115,
   115,   115,   115,   115,   115,   115,   115,   115,   116,   116,
   117,   118,   118,   119,   119,   119,   119,   120,   120,   121,
   121,   122,   123,   123,   124,   124,   125,   125,   125,   125,
   125,   125,   125,   125,   125,   125,   125,   125,   125,   125,
   126,   126,   127,   127,   127,   127,   127
};

static const short yyr2[] = {     0,
     1,     1,     1,     1,     1,     1,     1,     1,     1,    10,
    14,    10,    13,     6,     1,     3,     5,     3,     5,     4,
     6,     4,     6,     6,     3,     5,     3,     3,     3,     4,
     3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
     3,     2,     3,     2,     1,     1,     1,     1,     1,     1,
     1,     5,     5,     5,     5,     5,     5,     5,     5,     5,
     5,     4,     5,     4,     1,     1,     3,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     3,     1,     2,
     4,     1,     2,     7,     9,     6,     8,     4,     4,     1,
     2,     6,     6,     8,    10,    11,    10,    11,     6,     7,
     8,     7,    12,    13,     9,    10,     2,     5,     7,     4,
     1,     3,     4,     3,     3,     3,     3
};

static const short yydefact[] = {     0,
    46,    45,    47,    48,    49,    15,     0,     0,     0,     0,
     0,     0,    50,     0,     0,     0,     0,     2,     3,     4,
     7,     8,     5,     6,     0,     0,     0,     0,     0,    51,
     0,     0,    66,     0,    79,     1,     0,     0,     0,     0,
     0,    65,    66,     0,     0,     0,     0,     0,    42,   107,
     0,     0,     0,    44,     0,     0,     0,     0,    70,    71,
    69,    77,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    68,    80,    72,    76,    74,    75,    73,     0,
     0,     0,     0,     0,     0,     0,     0,     0,    78,     0,
     0,    42,    44,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,    40,    41,    39,    36,
    37,    38,    29,    32,    43,    31,    35,    34,    25,    27,
    28,    33,   115,   116,   117,   114,     0,     0,     9,    16,
     0,    18,    67,     0,     0,    90,     0,     0,     0,   110,
   111,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    62,    64,    40,    41,    39,    36,    37,    38,    43,    35,
    34,    25,    27,    28,    30,     0,   113,    20,    22,     0,
     0,     0,   113,     0,     0,     0,    91,     0,     0,     0,
     0,     0,     0,     0,     0,     0,    82,     0,     0,     0,
     0,     0,   108,     0,     0,    60,    61,    59,    56,    57,
    58,    63,    55,    54,    26,    52,    53,     0,     0,     0,
     0,    17,     0,    19,     0,     0,    92,     0,     0,     0,
     0,     0,   112,     0,     0,     0,    86,     0,     0,    83,
     0,    99,     0,     0,     0,     0,    24,    93,     0,    14,
    21,    23,     0,    89,    88,     0,     0,     0,     0,   109,
     0,     0,     0,    84,     0,   100,   102,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,    87,    81,     0,
    94,   101,     0,     0,     0,     0,     0,     0,     0,   105,
     0,     0,     0,    85,     0,     0,    66,     0,     0,     0,
     0,    95,   106,     0,     0,    97,     0,    12,     0,    10,
    96,     0,     0,    98,     0,     0,   103,     0,     0,     0,
   104,    13,     0,    11,     0,     0,     0
};

static const short yydefgoto[] = {   325,
    27,   141,    28,    29,    30,    31,    32,    33,    34,    35,
    36,   197,   198,    37,   146,   147,    38,    39,    40,    41,
   153,    42
};

static const short yypact[] = {  1914,
-32768,-32768,-32768,-32768,-32768,-32768,  2034,  2034,  2034,  2034,
  1914,  2034,-32768,  2034,  2034,  2034,  2034,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,  2034,  2082,   -80,   -76,   -54,-32768,
     9,    27,  2344,    -8,-32768,  1914,    39,    75,    83,    88,
    91,-32768,  2376,   -24,   -25,  2108,  2173,   546,    23,  2211,
   114,  2243,   -27,    23,  2034,  2034,  2408,  2034,-32768,-32768,
-32768,-32768,  2034,  2034,  2034,  2034,  2034,  2034,  2034,  2034,
  2034,  2034,  2034,  2034,  2034,  2034,  2034,  2034,   119,  1962,
    72,  2034,-32768,-32768,-32768,-32768,-32768,-32768,-32768,  1962,
   -32,  2013,   309,  1962,  1914,   -26,   603,  1962,-32768,   -19,
  1914,    77,    90,  2034,  2034,  2034,  2034,  2034,  2034,  2034,
  2034,  2034,  2034,  2034,  2034,  2275,  2584,  2532,  2532,    38,
    38,   178,  2376,    23,  2584,   178,    38,    38,    84,    84,
    62,    62,-32768,-32768,-32768,-32768,   -71,   157,-32768,     1,
   -12,    42,  2376,   -67,  1983,-32768,   -22,  2034,   -50,   131,
-32768,   -62,   -70,   475,  2034,   660,   717,   -69,  2034,   774,
-32768,-32768,  2543,  2472,  2502,    50,   247,   427,  2573,   278,
   332,   221,   340,   -55,-32768,  2034,-32768,   121,   135,  2034,
  2034,  2034,-32768,   130,     7,   196,-32768,   -49,  2034,   831,
   122,   160,  2034,  1914,  2034,   190,-32768,   173,   120,  2034,
   888,   945,   194,   -48,   188,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,   -10,  2034,  2034,
  2034,  2376,     6,  2376,  1914,  1914,-32768,  2034,   -17,  1914,
  1002,   309,-32768,   127,  1059,  2312,-32768,  1914,   197,-32768,
   205,  2376,  2034,  2034,  1116,  1173,-32768,-32768,   159,   139,
  2376,  2376,  2034,  1914,  1914,   -15,  1914,  1230,  1914,   210,
   213,  1914,  1287,-32768,   214,  2376,  2376,  2034,  1914,  1344,
  2133,    14,  1914,  1401,  2034,  1458,  1515,-32768,  1914,   219,
-32768,  2376,  1572,  1914,  2082,  2440,   211,  1629,   218,  2376,
  2034,  1914,  1686,-32768,  2034,  1743,  2408,    20,  1962,   309,
   223,-32768,  2376,  1800,  1914,  2376,  2034,-32768,   -61,   182,
-32768,  2034,  1857,  2376,  2034,   164,  2376,  2034,    26,  2034,
  2376,-32768,    64,-32768,   260,   266,-32768
};

static const short yypgoto[] = {-32768,
   -66,-32768,-32768,-32768,   -64,   -78,-32768,    -7,   104,   180,
   125,    69,-32768,-32768,   129,-32768,-32768,-32768,-32768,-32768,
   171,-32768
};


#define	YYLAST		2685


static const short yytable[] = {    43,
    43,    46,    47,   176,    49,   186,    50,    43,    52,    43,
    92,   101,    91,   315,   140,   151,   142,    54,    57,   151,
    58,   257,    70,   273,   189,   228,   150,    59,   177,   192,
   203,    82,   183,   193,   193,    82,   145,   183,   183,   191,
    82,    82,   155,    79,   217,    90,   145,   102,   103,    60,
   116,   247,    82,    82,    82,   117,   118,   119,   120,   121,
   122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
   132,   178,    43,   179,   143,    82,   180,    82,    82,     6,
   253,   159,    43,   226,    43,    82,    43,    82,   181,   249,
    43,   138,    82,    81,    82,    83,   163,   164,   165,   166,
   167,   168,   169,   170,   171,   172,   173,   174,    82,    82,
    44,    45,    61,   287,   233,    70,    82,   182,    51,   308,
    53,    79,    82,    90,   140,   322,   142,    70,    82,     6,
    62,    75,    76,    77,    78,    48,    79,    43,    90,    70,
    43,   138,    85,    75,    76,    77,    78,    43,    79,   209,
    90,    43,    18,    19,    20,    21,   139,    22,    23,    24,
    79,    70,    90,   324,     6,   260,    82,   190,    43,   133,
   134,   135,   222,    43,   224,    79,   161,    90,    86,    77,
    78,    43,    79,   137,    90,    43,    87,   236,    79,   162,
    90,    88,   242,   144,    89,   149,   220,   152,   238,   195,
   239,   152,    18,    19,    20,    21,   225,    22,    23,    24,
   221,    43,   251,   252,   227,    84,    82,    99,   232,   154,
    43,   157,    82,   241,   237,   160,   248,    84,   191,    82,
   246,   264,   265,   310,   271,   266,   267,    18,    19,    20,
    21,    82,    22,    23,    24,    43,   277,   278,   185,    66,
    67,   188,   281,   294,   300,    70,   302,   316,   199,   326,
   282,   311,   204,   286,   320,   327,   240,   290,   158,    73,
    74,    75,    76,    77,    78,   187,    79,   297,    90,   218,
   201,     0,     0,   303,   223,     0,     0,   306,     0,     0,
     0,    43,   229,     0,     0,     0,   234,     0,    70,   314,
     0,     0,     0,     0,   317,     0,     0,    43,     0,     0,
   321,     0,    43,     0,   231,     0,    77,    78,   235,    79,
   215,    90,   250,     0,    70,     0,   245,     0,     0,     0,
     0,   256,     0,    84,     0,     0,    84,     0,     0,    84,
    75,    76,    77,    78,     0,    79,   210,    90,     0,   254,
   255,     0,     0,     0,   258,    70,   272,     0,     0,     0,
     0,     0,   263,     0,     0,     0,     0,     0,     0,     0,
   270,    75,    76,    77,    78,     0,    79,   213,    90,     0,
    84,   274,     0,   276,     0,     0,   279,     0,   298,    18,
    19,    20,    21,   283,    22,    23,    24,   288,     0,     0,
     0,   293,   309,     0,     0,     0,     0,     0,   296,    70,
    84,     0,     0,     0,    84,     0,   304,    70,   319,     0,
     0,     0,     0,   323,    84,    75,    76,    77,    78,   313,
    79,   214,    90,    84,    84,    77,    78,    84,    79,   216,
    90,     0,    84,     0,     0,     0,     0,     0,     0,    84,
     0,     0,     0,    84,     0,    84,     0,     0,    84,     0,
     0,     0,    84,     0,     0,     0,     0,    84,     0,     0,
     0,     0,    84,     0,     0,    84,     0,     1,     2,     3,
     4,     5,     6,    84,     0,     0,     0,     0,     0,     0,
     0,     0,    84,     7,     0,     0,     0,     0,    66,    67,
   194,   195,   196,     0,    70,     0,     8,     9,     0,    10,
     0,     0,     0,    11,     0,     0,    12,    13,    73,    74,
    75,    76,    77,    78,    14,    79,   211,    90,     0,     0,
     0,     0,     0,    15,     0,     0,     0,     0,     0,     0,
    16,     0,     0,     0,    17,     0,     0,     0,     1,     2,
     3,     4,     5,     6,     0,    18,    19,    20,    21,     0,
    22,    23,    24,     0,     7,     0,     0,     0,     0,    25,
     0,     0,     0,     0,     0,    26,    96,     8,     9,     0,
    10,     0,     0,     0,    11,     0,     0,    12,    13,     0,
     0,     0,     0,     0,     0,    14,     0,     0,     0,     0,
     0,     0,     0,     0,    15,     1,     2,     3,     4,     5,
     6,    16,     0,     0,     0,    17,     0,     0,     0,   156,
     0,     7,     0,     0,     0,     0,    18,    19,    20,    21,
     0,    22,    23,    24,     8,     9,     0,    10,     0,     0,
    25,    11,     0,     0,    12,    13,    26,     0,     0,     0,
     0,     0,    14,     0,     0,     0,     0,     0,     0,     0,
     0,    15,     1,     2,     3,     4,     5,     6,    16,     0,
     0,     0,    17,     0,     0,     0,     0,     0,     7,     0,
     0,     0,     0,    18,    19,    20,    21,   200,    22,    23,
    24,     8,     9,     0,    10,     0,     0,    25,    11,     0,
     0,    12,    13,    26,     0,     0,     0,     0,     0,    14,
     0,     0,     0,     0,     0,     0,     0,     0,    15,     1,
     2,     3,     4,     5,     6,    16,     0,     0,     0,    17,
     0,     0,     0,   202,     0,     7,     0,     0,     0,     0,
    18,    19,    20,    21,     0,    22,    23,    24,     8,     9,
     0,    10,     0,     0,    25,    11,     0,     0,    12,    13,
    26,     0,     0,     0,     0,     0,    14,     0,     0,     0,
     0,     0,     0,     0,     0,    15,     1,     2,     3,     4,
     5,     6,    16,     0,     0,     0,    17,     0,     0,     0,
     0,     0,     7,     0,     0,     0,     0,    18,    19,    20,
    21,   205,    22,    23,    24,     8,     9,     0,    10,     0,
     0,    25,    11,     0,     0,    12,    13,    26,     0,     0,
     0,     0,     0,    14,     0,     0,     0,     0,     0,     0,
     0,     0,    15,     1,     2,     3,     4,     5,     6,    16,
     0,     0,     0,    17,     0,     0,     0,   230,     0,     7,
     0,     0,     0,     0,    18,    19,    20,    21,     0,    22,
    23,    24,     8,     9,     0,    10,     0,     0,    25,    11,
     0,     0,    12,    13,    26,     0,     0,     0,     0,     0,
    14,     0,     0,     0,     0,     0,     0,     0,     0,    15,
     1,     2,     3,     4,     5,     6,    16,     0,     0,     0,
    17,     0,     0,     0,     0,     0,     7,     0,     0,     0,
     0,    18,    19,    20,    21,   243,    22,    23,    24,     8,
     9,     0,    10,     0,     0,    25,    11,     0,     0,    12,
    13,    26,     0,     0,     0,     0,     0,    14,     0,     0,
     0,     0,     0,     0,     0,     0,    15,     1,     2,     3,
     4,     5,     6,    16,     0,     0,     0,    17,     0,     0,
     0,     0,     0,     7,     0,     0,     0,     0,    18,    19,
    20,    21,   244,    22,    23,    24,     8,     9,     0,    10,
     0,     0,    25,    11,     0,     0,    12,    13,    26,     0,
     0,     0,     0,     0,    14,     0,     0,     0,     0,     0,
     0,     0,     0,    15,     1,     2,     3,     4,     5,     6,
    16,     0,     0,     0,    17,     0,     0,     0,   259,     0,
     7,     0,     0,     0,     0,    18,    19,    20,    21,     0,
    22,    23,    24,     8,     9,     0,    10,     0,     0,    25,
    11,     0,     0,    12,    13,    26,     0,     0,     0,     0,
     0,    14,     0,     0,     0,     0,     0,     0,     0,     0,
    15,     1,     2,     3,     4,     5,     6,    16,     0,     0,
     0,    17,     0,     0,     0,     0,     0,     7,     0,     0,
     0,     0,    18,    19,    20,    21,   261,    22,    23,    24,
     8,     9,     0,    10,     0,     0,    25,    11,     0,     0,
    12,    13,    26,     0,     0,     0,     0,     0,    14,     0,
     0,     0,     0,     0,     0,     0,     0,    15,     1,     2,
     3,     4,     5,     6,    16,     0,     0,     0,    17,     0,
     0,     0,     0,     0,     7,     0,     0,     0,     0,    18,
    19,    20,    21,   268,    22,    23,    24,     8,     9,     0,
    10,     0,     0,    25,    11,     0,     0,    12,    13,    26,
     0,     0,     0,     0,     0,    14,     0,     0,     0,     0,
     0,     0,     0,     0,    15,     1,     2,     3,     4,     5,
     6,    16,     0,     0,     0,    17,     0,     0,     0,   269,
     0,     7,     0,     0,     0,     0,    18,    19,    20,    21,
     0,    22,    23,    24,     8,     9,     0,    10,     0,     0,
    25,    11,     0,     0,    12,    13,    26,     0,     0,     0,
     0,     0,    14,     0,     0,     0,     0,     0,     0,     0,
     0,    15,     1,     2,     3,     4,     5,     6,    16,     0,
     0,     0,    17,     0,     0,     0,     0,     0,     7,     0,
     0,     0,     0,    18,    19,    20,    21,   275,    22,    23,
    24,     8,     9,     0,    10,     0,     0,    25,    11,     0,
     0,    12,    13,    26,     0,     0,     0,     0,     0,    14,
     0,     0,     0,     0,     0,     0,     0,     0,    15,     1,
     2,     3,     4,     5,     6,    16,     0,     0,     0,    17,
     0,     0,     0,     0,     0,     7,     0,     0,     0,     0,
    18,    19,    20,    21,   280,    22,    23,    24,     8,     9,
     0,    10,     0,     0,    25,    11,     0,     0,    12,    13,
    26,     0,     0,     0,     0,     0,    14,     0,     0,     0,
     0,     0,     0,     0,     0,    15,     1,     2,     3,     4,
     5,     6,    16,     0,     0,     0,    17,     0,     0,     0,
   284,     0,     7,     0,     0,     0,     0,    18,    19,    20,
    21,     0,    22,    23,    24,     8,     9,     0,    10,     0,
     0,    25,    11,     0,     0,    12,    13,    26,     0,     0,
     0,     0,     0,    14,     0,     0,     0,     0,     0,     0,
     0,     0,    15,     1,     2,     3,     4,     5,     6,    16,
     0,     0,     0,    17,     0,     0,     0,     0,     0,     7,
     0,     0,     0,     0,    18,    19,    20,    21,   289,    22,
    23,    24,     8,     9,     0,    10,     0,     0,    25,    11,
     0,     0,    12,    13,    26,     0,     0,     0,     0,     0,
    14,     0,     0,     0,     0,     0,     0,     0,     0,    15,
     1,     2,     3,     4,     5,     6,    16,     0,     0,     0,
    17,     0,     0,     0,     0,     0,     7,     0,     0,     0,
     0,    18,    19,    20,    21,   291,    22,    23,    24,     8,
     9,     0,    10,     0,     0,    25,    11,     0,     0,    12,
    13,    26,     0,     0,     0,     0,     0,    14,     0,     0,
     0,     0,     0,     0,     0,     0,    15,     1,     2,     3,
     4,     5,     6,    16,     0,     0,     0,    17,     0,     0,
     0,   292,     0,     7,     0,     0,     0,     0,    18,    19,
    20,    21,     0,    22,    23,    24,     8,     9,     0,    10,
     0,     0,    25,    11,     0,     0,    12,    13,    26,     0,
     0,     0,     0,     0,    14,     0,     0,     0,     0,     0,
     0,     0,     0,    15,     1,     2,     3,     4,     5,     6,
    16,     0,     0,     0,    17,     0,     0,     0,     0,     0,
     7,     0,     0,     0,     0,    18,    19,    20,    21,   295,
    22,    23,    24,     8,     9,     0,    10,     0,     0,    25,
    11,     0,     0,    12,    13,    26,     0,     0,     0,     0,
     0,    14,     0,     0,     0,     0,     0,     0,     0,     0,
    15,     1,     2,     3,     4,     5,     6,    16,     0,     0,
     0,    17,     0,     0,     0,     0,     0,     7,     0,     0,
     0,     0,    18,    19,    20,    21,   301,    22,    23,    24,
     8,     9,     0,    10,     0,     0,    25,    11,     0,     0,
    12,    13,    26,     0,     0,     0,     0,     0,    14,     0,
     0,     0,     0,     0,     0,     0,     0,    15,     1,     2,
     3,     4,     5,     6,    16,     0,     0,     0,    17,     0,
     0,     0,   305,     0,     7,     0,     0,     0,     0,    18,
    19,    20,    21,     0,    22,    23,    24,     8,     9,     0,
    10,     0,     0,    25,    11,     0,     0,    12,    13,    26,
     0,     0,     0,     0,     0,    14,     0,     0,     0,     0,
     0,     0,     0,     0,    15,     1,     2,     3,     4,     5,
     6,    16,     0,     0,     0,    17,     0,     0,     0,     0,
     0,     7,     0,     0,     0,     0,    18,    19,    20,    21,
   307,    22,    23,    24,     8,     9,     0,    10,     0,     0,
    25,    11,     0,     0,    12,    13,    26,     0,     0,     0,
     0,     0,    14,     0,     0,     0,     0,     0,     0,     0,
     0,    15,     1,     2,     3,     4,     5,     6,    16,     0,
     0,     0,    17,     0,     0,     0,     0,     0,     7,     0,
     0,     0,     0,    18,    19,    20,    21,   312,    22,    23,
    24,     8,     9,     0,    10,     0,     0,    25,    11,     0,
     0,    12,    13,    26,     0,     0,     0,     0,     0,    14,
     0,     0,     0,     0,     0,     0,     0,     0,    15,     1,
     2,     3,     4,     5,     6,    16,     0,     0,     0,    17,
     0,     0,     0,     0,     0,     7,     0,     0,     0,     0,
    18,    19,    20,    21,   318,    22,    23,    24,     8,     9,
     0,    10,     0,     0,    25,    11,     0,     0,    12,    13,
    26,     0,     0,     0,     0,     0,    14,     0,     0,     0,
     0,     0,     0,     0,     0,    15,     1,     2,     3,     4,
     5,     6,    16,     0,     0,     0,    17,     0,     0,     0,
     0,     0,     7,     0,     0,     0,     0,    18,    19,    20,
    21,     0,    22,    23,    24,     8,     9,     0,    10,     0,
     0,    25,    11,     0,     0,    12,    13,    26,     0,     0,
     0,     0,     0,    14,     1,     2,     3,     4,     5,     6,
     0,     0,    15,     0,     0,     0,     0,     0,     0,    16,
     0,     0,     0,    17,     0,     1,     2,     3,     4,     5,
     6,     0,     0,     0,    18,    19,    20,    21,     0,    22,
    23,    24,     0,    12,    13,     0,     0,     0,    25,     0,
     0,     0,     0,     0,    26,     1,     2,     3,     4,     5,
     6,     0,     0,     0,    12,    13,     0,     0,   184,     0,
     0,     0,     0,     0,     0,     0,     1,     2,     3,     4,
     5,     6,    18,    19,    20,    21,     0,    22,    23,    24,
     0,     0,     0,     0,    12,    13,    25,     0,     0,     0,
     0,   136,    26,    18,    19,    20,    21,     0,    22,    23,
    24,     0,   148,     0,     0,    12,    13,    25,     0,     0,
     0,     0,     0,    26,     1,     2,     3,     4,     5,     6,
     0,     0,     0,    18,    19,    20,    21,     0,    22,    23,
    24,     0,     0,     0,     0,     0,     0,    25,     0,     0,
     0,     0,     0,    26,    18,    19,    20,    21,     0,    22,
    23,    24,    63,    55,    13,     0,     0,     0,    25,     0,
     0,     0,     0,     0,    26,     1,     2,     3,     4,     5,
     6,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    64,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    18,    19,    20,    21,    93,    22,    23,    24,
     0,     0,     0,     0,    12,    13,    56,     0,    65,    66,
    67,    68,    26,    69,     0,    70,     0,    63,     0,     0,
     0,     0,     0,     0,     0,     0,     0,    71,    72,    73,
    74,    75,    76,    77,    78,     0,    79,     0,    94,     0,
     0,     0,     0,    18,    19,    20,    21,    64,    22,    23,
    24,     0,     0,     0,     0,    63,     0,    25,     0,     0,
     0,     0,     0,   285,     0,     0,     0,    95,     0,     0,
     0,     0,     0,    65,    66,    67,    68,    97,    69,     0,
    70,     0,     0,     0,     0,    64,     0,    63,     0,     0,
     0,     0,    71,    72,    73,    74,    75,    76,    77,    78,
     0,    79,     0,    90,     0,     0,     0,     0,     0,   100,
     0,    65,    66,    67,    68,     0,    69,    64,    70,    63,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    71,    72,    73,    74,    75,    76,    77,    78,     0,    79,
     0,    98,     0,    65,    66,    67,    68,     0,    69,    64,
    70,     0,     0,     0,     0,     0,    63,     0,     0,     0,
     0,     0,    71,    72,    73,    74,    75,    76,    77,    78,
     0,    79,     0,    90,     0,    65,    66,    67,    68,     0,
    69,     0,    70,     0,     0,     0,    64,     0,    63,     0,
     0,     0,     0,     0,    71,    72,    73,    74,    75,    76,
    77,    78,     0,    79,   175,    90,   262,     0,     0,     0,
     0,     0,    65,    66,    67,    68,     0,    69,    64,    70,
    63,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    71,    72,    73,    74,    75,    76,    77,    78,     0,
    79,     0,    90,     0,    65,    66,    67,    68,     0,    69,
    64,    70,   104,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,    71,    72,    73,    74,    75,    76,    77,
    78,     0,    79,     0,    80,     0,    65,    66,    67,    68,
     0,    69,   105,    70,    63,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,    71,    72,    73,    74,    75,
    76,    77,    78,     0,    79,     0,    90,     0,   106,   107,
   108,   109,     0,    69,    64,    70,    63,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,   110,    72,   111,
   112,   113,   114,   115,    78,     0,    79,     0,    90,     0,
    65,    66,    67,    68,     0,    69,    63,    70,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,    71,
    72,    73,    74,    75,    76,    77,    78,     0,    79,     0,
   299,     0,     0,    66,    67,    68,    63,     0,     0,    70,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    71,    72,    73,    74,    75,    76,    77,    78,     0,
    79,   207,    90,    66,    67,    68,     0,     0,     0,    70,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    71,    72,    73,    74,    75,    76,    77,    78,     0,
    79,   208,    90,    66,    67,    68,     0,     0,     0,    70,
     0,     0,     0,     0,    66,    67,    68,     0,     0,     0,
    70,    71,    72,    73,    74,    75,    76,    77,    78,     0,
    79,     0,    90,    72,    73,    74,    75,    76,    77,    78,
     0,    79,   206,    90,    66,    67,    68,     0,     0,     0,
    70,     0,     0,     0,     0,    66,    67,    68,     0,     0,
     0,    70,     0,    72,    73,    74,    75,    76,    77,    78,
     0,    79,   212,    90,    72,    73,    74,    75,    76,    77,
    78,     0,    79,     0,    90
};

static const short yycheck[] = {     7,
     8,     9,    10,    75,    12,    28,    14,    15,    16,    17,
    36,    39,    37,    75,    81,    94,    81,    25,    26,    98,
   101,    39,    78,    39,    75,    75,    93,   104,   100,   100,
   100,   103,   100,   104,   104,   103,    69,   100,   100,   102,
   103,   103,    69,    99,   100,   101,    69,    55,    56,   104,
    58,   100,   103,   103,   103,    63,    64,    65,    66,    67,
    68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
    78,   138,    80,   138,    82,   103,    76,   103,   103,     8,
    75,   101,    90,    77,    92,   103,    94,   103,   101,   100,
    98,    20,   103,   102,   103,   104,   104,   105,   106,   107,
   108,   109,   110,   111,   112,   113,   114,   115,   103,   103,
     7,     8,   104,   100,   193,    78,   103,    76,    15,   100,
    17,    99,   103,   101,   191,   100,   191,    78,   103,     8,
   104,    94,    95,    96,    97,    11,    99,   145,   101,    78,
   148,    20,   104,    94,    95,    96,    97,   155,    99,   100,
   101,   159,    81,    82,    83,    84,    85,    86,    87,    88,
    99,    78,   101,   100,     8,   232,   103,    37,   176,    51,
    52,    53,   180,   181,   182,    99,   100,   101,   104,    96,
    97,   189,    99,    80,   101,   193,   104,   195,    99,   100,
   101,   104,   200,    90,   104,    92,    76,    94,    26,    27,
    28,    98,    81,    82,    83,    84,    77,    86,    87,    88,
    76,   219,   220,   221,    19,    36,   103,   104,    59,    95,
   228,    97,   103,   104,    35,   101,    39,    48,   102,   103,
    37,    35,    28,   300,    76,   243,   244,    81,    82,    83,
    84,   103,    86,    87,    88,   253,    37,    35,   145,    72,
    73,   148,    39,    35,    44,    78,    39,    76,   155,     0,
   268,    39,   159,   271,   101,     0,   198,   275,    98,    92,
    93,    94,    95,    96,    97,   147,    99,   285,   101,   176,
   156,    -1,    -1,   291,   181,    -1,    -1,   295,    -1,    -1,
    -1,   299,   189,    -1,    -1,    -1,   193,    -1,    78,   307,
    -1,    -1,    -1,    -1,   312,    -1,    -1,   315,    -1,    -1,
   318,    -1,   320,    -1,   190,    -1,    96,    97,   194,    99,
   100,   101,   219,    -1,    78,    -1,   202,    -1,    -1,    -1,
    -1,   228,    -1,   154,    -1,    -1,   157,    -1,    -1,   160,
    94,    95,    96,    97,    -1,    99,   100,   101,    -1,   225,
   226,    -1,    -1,    -1,   230,    78,   253,    -1,    -1,    -1,
    -1,    -1,   238,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
   246,    94,    95,    96,    97,    -1,    99,   100,   101,    -1,
   201,   257,    -1,   259,    -1,    -1,   262,    -1,   285,    81,
    82,    83,    84,   269,    86,    87,    88,   273,    -1,    -1,
    -1,   277,   299,    -1,    -1,    -1,    -1,    -1,   284,    78,
   231,    -1,    -1,    -1,   235,    -1,   292,    78,   315,    -1,
    -1,    -1,    -1,   320,   245,    94,    95,    96,    97,   305,
    99,   100,   101,   254,   255,    96,    97,   258,    99,   100,
   101,    -1,   263,    -1,    -1,    -1,    -1,    -1,    -1,   270,
    -1,    -1,    -1,   274,    -1,   276,    -1,    -1,   279,    -1,
    -1,    -1,   283,    -1,    -1,    -1,    -1,   288,    -1,    -1,
    -1,    -1,   293,    -1,    -1,   296,    -1,     3,     4,     5,
     6,     7,     8,   304,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,   313,    19,    -1,    -1,    -1,    -1,    72,    73,
    26,    27,    28,    -1,    78,    -1,    32,    33,    -1,    35,
    -1,    -1,    -1,    39,    -1,    -1,    42,    43,    92,    93,
    94,    95,    96,    97,    50,    99,   100,   101,    -1,    -1,
    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,
    66,    -1,    -1,    -1,    70,    -1,    -1,    -1,     3,     4,
     5,     6,     7,     8,    -1,    81,    82,    83,    84,    -1,
    86,    87,    88,    -1,    19,    -1,    -1,    -1,    -1,    95,
    -1,    -1,    -1,    -1,    -1,   101,    31,    32,    33,    -1,
    35,    -1,    -1,    -1,    39,    -1,    -1,    42,    43,    -1,
    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    59,     3,     4,     5,     6,     7,
     8,    66,    -1,    -1,    -1,    70,    -1,    -1,    -1,    17,
    -1,    19,    -1,    -1,    -1,    -1,    81,    82,    83,    84,
    -1,    86,    87,    88,    32,    33,    -1,    35,    -1,    -1,
    95,    39,    -1,    -1,    42,    43,   101,    -1,    -1,    -1,
    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    59,     3,     4,     5,     6,     7,     8,    66,    -1,
    -1,    -1,    70,    -1,    -1,    -1,    -1,    -1,    19,    -1,
    -1,    -1,    -1,    81,    82,    83,    84,    28,    86,    87,
    88,    32,    33,    -1,    35,    -1,    -1,    95,    39,    -1,
    -1,    42,    43,   101,    -1,    -1,    -1,    -1,    -1,    50,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,     3,
     4,     5,     6,     7,     8,    66,    -1,    -1,    -1,    70,
    -1,    -1,    -1,    17,    -1,    19,    -1,    -1,    -1,    -1,
    81,    82,    83,    84,    -1,    86,    87,    88,    32,    33,
    -1,    35,    -1,    -1,    95,    39,    -1,    -1,    42,    43,
   101,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    59,     3,     4,     5,     6,
     7,     8,    66,    -1,    -1,    -1,    70,    -1,    -1,    -1,
    -1,    -1,    19,    -1,    -1,    -1,    -1,    81,    82,    83,
    84,    28,    86,    87,    88,    32,    33,    -1,    35,    -1,
    -1,    95,    39,    -1,    -1,    42,    43,   101,    -1,    -1,
    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    59,     3,     4,     5,     6,     7,     8,    66,
    -1,    -1,    -1,    70,    -1,    -1,    -1,    17,    -1,    19,
    -1,    -1,    -1,    -1,    81,    82,    83,    84,    -1,    86,
    87,    88,    32,    33,    -1,    35,    -1,    -1,    95,    39,
    -1,    -1,    42,    43,   101,    -1,    -1,    -1,    -1,    -1,
    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,
     3,     4,     5,     6,     7,     8,    66,    -1,    -1,    -1,
    70,    -1,    -1,    -1,    -1,    -1,    19,    -1,    -1,    -1,
    -1,    81,    82,    83,    84,    28,    86,    87,    88,    32,
    33,    -1,    35,    -1,    -1,    95,    39,    -1,    -1,    42,
    43,   101,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    59,     3,     4,     5,
     6,     7,     8,    66,    -1,    -1,    -1,    70,    -1,    -1,
    -1,    -1,    -1,    19,    -1,    -1,    -1,    -1,    81,    82,
    83,    84,    28,    86,    87,    88,    32,    33,    -1,    35,
    -1,    -1,    95,    39,    -1,    -1,    42,    43,   101,    -1,
    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    59,     3,     4,     5,     6,     7,     8,
    66,    -1,    -1,    -1,    70,    -1,    -1,    -1,    17,    -1,
    19,    -1,    -1,    -1,    -1,    81,    82,    83,    84,    -1,
    86,    87,    88,    32,    33,    -1,    35,    -1,    -1,    95,
    39,    -1,    -1,    42,    43,   101,    -1,    -1,    -1,    -1,
    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    59,     3,     4,     5,     6,     7,     8,    66,    -1,    -1,
    -1,    70,    -1,    -1,    -1,    -1,    -1,    19,    -1,    -1,
    -1,    -1,    81,    82,    83,    84,    28,    86,    87,    88,
    32,    33,    -1,    35,    -1,    -1,    95,    39,    -1,    -1,
    42,    43,   101,    -1,    -1,    -1,    -1,    -1,    50,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,     3,     4,
     5,     6,     7,     8,    66,    -1,    -1,    -1,    70,    -1,
    -1,    -1,    -1,    -1,    19,    -1,    -1,    -1,    -1,    81,
    82,    83,    84,    28,    86,    87,    88,    32,    33,    -1,
    35,    -1,    -1,    95,    39,    -1,    -1,    42,    43,   101,
    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    59,     3,     4,     5,     6,     7,
     8,    66,    -1,    -1,    -1,    70,    -1,    -1,    -1,    17,
    -1,    19,    -1,    -1,    -1,    -1,    81,    82,    83,    84,
    -1,    86,    87,    88,    32,    33,    -1,    35,    -1,    -1,
    95,    39,    -1,    -1,    42,    43,   101,    -1,    -1,    -1,
    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    59,     3,     4,     5,     6,     7,     8,    66,    -1,
    -1,    -1,    70,    -1,    -1,    -1,    -1,    -1,    19,    -1,
    -1,    -1,    -1,    81,    82,    83,    84,    28,    86,    87,
    88,    32,    33,    -1,    35,    -1,    -1,    95,    39,    -1,
    -1,    42,    43,   101,    -1,    -1,    -1,    -1,    -1,    50,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,     3,
     4,     5,     6,     7,     8,    66,    -1,    -1,    -1,    70,
    -1,    -1,    -1,    -1,    -1,    19,    -1,    -1,    -1,    -1,
    81,    82,    83,    84,    28,    86,    87,    88,    32,    33,
    -1,    35,    -1,    -1,    95,    39,    -1,    -1,    42,    43,
   101,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    59,     3,     4,     5,     6,
     7,     8,    66,    -1,    -1,    -1,    70,    -1,    -1,    -1,
    17,    -1,    19,    -1,    -1,    -1,    -1,    81,    82,    83,
    84,    -1,    86,    87,    88,    32,    33,    -1,    35,    -1,
    -1,    95,    39,    -1,    -1,    42,    43,   101,    -1,    -1,
    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    59,     3,     4,     5,     6,     7,     8,    66,
    -1,    -1,    -1,    70,    -1,    -1,    -1,    -1,    -1,    19,
    -1,    -1,    -1,    -1,    81,    82,    83,    84,    28,    86,
    87,    88,    32,    33,    -1,    35,    -1,    -1,    95,    39,
    -1,    -1,    42,    43,   101,    -1,    -1,    -1,    -1,    -1,
    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,
     3,     4,     5,     6,     7,     8,    66,    -1,    -1,    -1,
    70,    -1,    -1,    -1,    -1,    -1,    19,    -1,    -1,    -1,
    -1,    81,    82,    83,    84,    28,    86,    87,    88,    32,
    33,    -1,    35,    -1,    -1,    95,    39,    -1,    -1,    42,
    43,   101,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    59,     3,     4,     5,
     6,     7,     8,    66,    -1,    -1,    -1,    70,    -1,    -1,
    -1,    17,    -1,    19,    -1,    -1,    -1,    -1,    81,    82,
    83,    84,    -1,    86,    87,    88,    32,    33,    -1,    35,
    -1,    -1,    95,    39,    -1,    -1,    42,    43,   101,    -1,
    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    59,     3,     4,     5,     6,     7,     8,
    66,    -1,    -1,    -1,    70,    -1,    -1,    -1,    -1,    -1,
    19,    -1,    -1,    -1,    -1,    81,    82,    83,    84,    28,
    86,    87,    88,    32,    33,    -1,    35,    -1,    -1,    95,
    39,    -1,    -1,    42,    43,   101,    -1,    -1,    -1,    -1,
    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    59,     3,     4,     5,     6,     7,     8,    66,    -1,    -1,
    -1,    70,    -1,    -1,    -1,    -1,    -1,    19,    -1,    -1,
    -1,    -1,    81,    82,    83,    84,    28,    86,    87,    88,
    32,    33,    -1,    35,    -1,    -1,    95,    39,    -1,    -1,
    42,    43,   101,    -1,    -1,    -1,    -1,    -1,    50,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,     3,     4,
     5,     6,     7,     8,    66,    -1,    -1,    -1,    70,    -1,
    -1,    -1,    17,    -1,    19,    -1,    -1,    -1,    -1,    81,
    82,    83,    84,    -1,    86,    87,    88,    32,    33,    -1,
    35,    -1,    -1,    95,    39,    -1,    -1,    42,    43,   101,
    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    59,     3,     4,     5,     6,     7,
     8,    66,    -1,    -1,    -1,    70,    -1,    -1,    -1,    -1,
    -1,    19,    -1,    -1,    -1,    -1,    81,    82,    83,    84,
    28,    86,    87,    88,    32,    33,    -1,    35,    -1,    -1,
    95,    39,    -1,    -1,    42,    43,   101,    -1,    -1,    -1,
    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    59,     3,     4,     5,     6,     7,     8,    66,    -1,
    -1,    -1,    70,    -1,    -1,    -1,    -1,    -1,    19,    -1,
    -1,    -1,    -1,    81,    82,    83,    84,    28,    86,    87,
    88,    32,    33,    -1,    35,    -1,    -1,    95,    39,    -1,
    -1,    42,    43,   101,    -1,    -1,    -1,    -1,    -1,    50,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,     3,
     4,     5,     6,     7,     8,    66,    -1,    -1,    -1,    70,
    -1,    -1,    -1,    -1,    -1,    19,    -1,    -1,    -1,    -1,
    81,    82,    83,    84,    28,    86,    87,    88,    32,    33,
    -1,    35,    -1,    -1,    95,    39,    -1,    -1,    42,    43,
   101,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    59,     3,     4,     5,     6,
     7,     8,    66,    -1,    -1,    -1,    70,    -1,    -1,    -1,
    -1,    -1,    19,    -1,    -1,    -1,    -1,    81,    82,    83,
    84,    -1,    86,    87,    88,    32,    33,    -1,    35,    -1,
    -1,    95,    39,    -1,    -1,    42,    43,   101,    -1,    -1,
    -1,    -1,    -1,    50,     3,     4,     5,     6,     7,     8,
    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,    66,
    -1,    -1,    -1,    70,    -1,     3,     4,     5,     6,     7,
     8,    -1,    -1,    -1,    81,    82,    83,    84,    -1,    86,
    87,    88,    -1,    42,    43,    -1,    -1,    -1,    95,    -1,
    -1,    -1,    -1,    -1,   101,     3,     4,     5,     6,     7,
     8,    -1,    -1,    -1,    42,    43,    -1,    -1,    46,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,     5,     6,
     7,     8,    81,    82,    83,    84,    -1,    86,    87,    88,
    -1,    -1,    -1,    -1,    42,    43,    95,    -1,    -1,    -1,
    -1,   100,   101,    81,    82,    83,    84,    -1,    86,    87,
    88,    -1,    60,    -1,    -1,    42,    43,    95,    -1,    -1,
    -1,    -1,    -1,   101,     3,     4,     5,     6,     7,     8,
    -1,    -1,    -1,    81,    82,    83,    84,    -1,    86,    87,
    88,    -1,    -1,    -1,    -1,    -1,    -1,    95,    -1,    -1,
    -1,    -1,    -1,   101,    81,    82,    83,    84,    -1,    86,
    87,    88,    15,    42,    43,    -1,    -1,    -1,    95,    -1,
    -1,    -1,    -1,    -1,   101,     3,     4,     5,     6,     7,
     8,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    45,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    81,    82,    83,    84,    59,    86,    87,    88,
    -1,    -1,    -1,    -1,    42,    43,    95,    -1,    71,    72,
    73,    74,   101,    76,    -1,    78,    -1,    15,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    90,    91,    92,
    93,    94,    95,    96,    97,    -1,    99,    -1,   101,    -1,
    -1,    -1,    -1,    81,    82,    83,    84,    45,    86,    87,
    88,    -1,    -1,    -1,    -1,    15,    -1,    95,    -1,    -1,
    -1,    -1,    -1,   101,    -1,    -1,    -1,    65,    -1,    -1,
    -1,    -1,    -1,    71,    72,    73,    74,    37,    76,    -1,
    78,    -1,    -1,    -1,    -1,    45,    -1,    15,    -1,    -1,
    -1,    -1,    90,    91,    92,    93,    94,    95,    96,    97,
    -1,    99,    -1,   101,    -1,    -1,    -1,    -1,    -1,    37,
    -1,    71,    72,    73,    74,    -1,    76,    45,    78,    15,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    90,    91,    92,    93,    94,    95,    96,    97,    -1,    99,
    -1,   101,    -1,    71,    72,    73,    74,    -1,    76,    45,
    78,    -1,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,
    -1,    -1,    90,    91,    92,    93,    94,    95,    96,    97,
    -1,    99,    -1,   101,    -1,    71,    72,    73,    74,    -1,
    76,    -1,    78,    -1,    -1,    -1,    45,    -1,    15,    -1,
    -1,    -1,    -1,    -1,    90,    91,    92,    93,    94,    95,
    96,    97,    -1,    99,   100,   101,    65,    -1,    -1,    -1,
    -1,    -1,    71,    72,    73,    74,    -1,    76,    45,    78,
    15,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    90,    91,    92,    93,    94,    95,    96,    97,    -1,
    99,    -1,   101,    -1,    71,    72,    73,    74,    -1,    76,
    45,    78,    15,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    90,    91,    92,    93,    94,    95,    96,
    97,    -1,    99,    -1,   101,    -1,    71,    72,    73,    74,
    -1,    76,    45,    78,    15,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    90,    91,    92,    93,    94,
    95,    96,    97,    -1,    99,    -1,   101,    -1,    71,    72,
    73,    74,    -1,    76,    45,    78,    15,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    90,    91,    92,
    93,    94,    95,    96,    97,    -1,    99,    -1,   101,    -1,
    71,    72,    73,    74,    -1,    76,    15,    78,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    90,
    91,    92,    93,    94,    95,    96,    97,    -1,    99,    -1,
   101,    -1,    -1,    72,    73,    74,    15,    -1,    -1,    78,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    90,    91,    92,    93,    94,    95,    96,    97,    -1,
    99,   100,   101,    72,    73,    74,    -1,    -1,    -1,    78,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    90,    91,    92,    93,    94,    95,    96,    97,    -1,
    99,   100,   101,    72,    73,    74,    -1,    -1,    -1,    78,
    -1,    -1,    -1,    -1,    72,    73,    74,    -1,    -1,    -1,
    78,    90,    91,    92,    93,    94,    95,    96,    97,    -1,
    99,    -1,   101,    91,    92,    93,    94,    95,    96,    97,
    -1,    99,   100,   101,    72,    73,    74,    -1,    -1,    -1,
    78,    -1,    -1,    -1,    -1,    72,    73,    74,    -1,    -1,
    -1,    78,    -1,    91,    92,    93,    94,    95,    96,    97,
    -1,    99,   100,   101,    91,    92,    93,    94,    95,    96,
    97,    -1,    99,    -1,   101
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (from, to, count)
     char *from;
     char *to;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *from, char *to, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 192 "bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#else
#define YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#endif

int
yyparse(YYPARSE_PARAM)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 1:
#line 239 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Program=yyvsp[0].StatementList; Root = yyvsp[0].StatementList;;
    break;}
case 2:
#line 241 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarType = Integer;;
    break;}
case 3:
#line 242 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarType = Character;;
    break;}
case 4:
#line 243 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarType = Boolean;;
    break;}
case 5:
#line 244 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarType = String;;
    break;}
case 6:
#line 245 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarType = Record;;
    break;}
case 7:
#line 246 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarType = ENum;;
    break;}
case 8:
#line 247 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarType = Float;;
    break;}
case 9:
#line 250 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ArrType = Array;;
    break;}
case 10:
#line 252 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ArrayVar=create_array_var(yyvsp[-9].ExprList,yyvsp[-5].ExprList,yyvsp[-3].ExprList, yyvsp[0].VarType,NULL);;
    break;}
case 11:
#line 253 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ArrayVar=create_array_var(yyvsp[-13].ExprList,yyvsp[-9].ExprList,yyvsp[-7].ExprList, yyvsp[-4].VarType,yyvsp[-1].ExprList);;
    break;}
case 12:
#line 256 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ArrayAssign=create_array_assign2(yyvsp[-9].Expression, yyvsp[-7].ExprList, yyvsp[-5].ExprList, yyvsp[-1].ExprList);;
    break;}
case 13:
#line 257 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ArrayAssign=create_array_assign1(yyvsp[-12].Expression, yyvsp[-5].Expression, yyvsp[-10].ExprList, yyvsp[-8].ExprList, yyvsp[-3].ExprList, yyvsp[-1].ExprList);;
    break;}
case 14:
#line 258 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ArrayAssign=create_array_assign2(yyvsp[-5].Expression, yyvsp[-3].ExprList, yyvsp[-3].ExprList, yyvsp[0].ExprList);;
    break;}
case 15:
#line 260 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Name=create_name(yyvsp[0].VarName);;
    break;}
case 16:
#line 263 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarListAssign=create_var_list_assign(yyvsp[-2].ExprList, NULL, yyvsp[0].VarType,0);;
    break;}
case 17:
#line 264 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarListAssign=create_var_list_assign(yyvsp[-4].ExprList,yyvsp[0].Expression,yyvsp[-2].VarType,0);;
    break;}
case 18:
#line 265 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarListAssign=create_var_list_assign(yyvsp[-2].ExprList, NULL, ENum,0);;
    break;}
case 19:
#line 266 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarListAssign=create_var_list_assign(yyvsp[-4].ExprList, yyvsp[0].Expression, ENum,0);;
    break;}
case 20:
#line 267 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarListAssign=create_var_list_assign(yyvsp[-3].ExprList, NULL, yyvsp[0].VarType,1);;
    break;}
case 21:
#line 268 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarListAssign=create_var_list_assign(yyvsp[-5].ExprList,yyvsp[0].Expression,yyvsp[-2].VarType,1);;
    break;}
case 22:
#line 269 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarListAssign=create_var_list_assign(yyvsp[-3].ExprList, NULL, ENum,1);;
    break;}
case 23:
#line 270 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.VarListAssign=create_var_list_assign(yyvsp[-5].ExprList, yyvsp[0].Expression, ENum,1);;
    break;}
case 24:
#line 274 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ENum=create_enum_variable(yyvsp[-4].Expression,yyvsp[-1].ExprList);;
    break;}
case 25:
#line 276 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,sum);;
    break;}
case 26:
#line 277 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,sum);;
    break;}
case 27:
#line 278 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,sub);;
    break;}
case 28:
#line 279 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,mul);;
    break;}
case 29:
#line 280 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,assign);;
    break;}
case 30:
#line 281 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression2(NULL,yyvsp[-1].Expression,func,yyvsp[-3].VarType);;
    break;}
case 31:
#line 282 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,equal);;
    break;}
case 32:
#line 283 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,grade);;
    break;}
case 33:
#line 284 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,Div);;
    break;}
case 34:
#line 285 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,less);;
    break;}
case 35:
#line 286 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,greater);;
    break;}
case 36:
#line 287 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,ge);;
    break;}
case 37:
#line 288 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,le);;
    break;}
case 38:
#line 289 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,ne);;
    break;}
case 39:
#line 290 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,xor);;
    break;}
case 40:
#line 291 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,and);;
    break;}
case 41:
#line 292 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,or);;
    break;}
case 42:
#line 293 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_unary_expression(yyvsp[0].Expression,not);;
    break;}
case 43:
#line 294 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-2].Expression,yyvsp[0].Expression,concat);;
    break;}
case 44:
#line 295 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_unary_expression(yyvsp[0].Expression,uminus);;
    break;}
case 45:
#line 296 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_int_expr(yyvsp[0].ConstInt);;
    break;}
case 46:
#line 297 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_str_expr(yyvsp[0].ConstString);;
    break;}
case 47:
#line 298 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_bool_expr(yyvsp[0].ConstBool);;
    break;}
case 48:
#line 299 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_double_expr(yyvsp[0].ConstDouble);;
    break;}
case 49:
#line 300 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_char_expr(yyvsp[0].ConstChar);;
    break;}
case 50:
#line 301 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_empty_expr();;
    break;}
case 51:
#line 302 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_name_expr(yyvsp[0].Name);;
    break;}
case 52:
#line 303 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,sub);;
    break;}
case 53:
#line 304 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,mul);;
    break;}
case 54:
#line 305 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,less);;
    break;}
case 55:
#line 306 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,greater);;
    break;}
case 56:
#line 307 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,ge);;
    break;}
case 57:
#line 308 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,le);;
    break;}
case 58:
#line 309 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,ne);;
    break;}
case 59:
#line 310 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,xor);;
    break;}
case 60:
#line 311 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,and);;
    break;}
case 61:
#line 312 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,or);;
    break;}
case 62:
#line 313 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_unary_expression(yyvsp[-1].Expression,not);;
    break;}
case 63:
#line 314 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_binary_expression(yyvsp[-3].Expression,yyvsp[-1].Expression,concat);;
    break;}
case 64:
#line 315 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_unary_expression(yyvsp[-1].Expression,uminus);;
    break;}
case 65:
#line 316 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Expression=create_func_call_expr(yyvsp[0].FuncCall);;
    break;}
case 66:
#line 318 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ExprList=create_expr_list(yyvsp[0].Expression);;
    break;}
case 67:
#line 319 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{add_expr_to_list(yyvsp[-2].ExprList,yyvsp[0].Expression);;
    break;}
case 68:
#line 321 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_expr_statement(yyvsp[-1].ExprList);;
    break;}
case 69:
#line 322 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_list_statement(yyvsp[-1].VarListAssign);;
    break;}
case 70:
#line 323 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_arr_var(yyvsp[-1].ArrayVar);;
    break;}
case 71:
#line 324 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_arr_statement(yyvsp[-1].ArrayAssign);;
    break;}
case 72:
#line 325 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_if_statement(yyvsp[-1].IfStruct);;
    break;}
case 73:
#line 326 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_func_statement(yyvsp[-1].FuncProc);;
    break;}
case 74:
#line 327 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_while_statement(yyvsp[-1].WhileLoop);;
    break;}
case 75:
#line 328 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_for_statement(yyvsp[-1].ForLoop);;
    break;}
case 76:
#line 329 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_case_statement(yyvsp[-1].CaseStruct);;
    break;}
case 77:
#line 330 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_enum_statement(yyvsp[-1].ENum);;
    break;}
case 78:
#line 331 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Statement=create_return_statement(yyvsp[-1].ExprList);;
    break;}
case 79:
#line 334 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.StatementList=create_statement_list(yyvsp[0].Statement);;
    break;}
case 80:
#line 335 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{add_statement_to_list(yyvsp[-1].StatementList,yyvsp[0].Statement);;
    break;}
case 81:
#line 338 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.Elseif=create_elsif(yyvsp[-2].Expression,yyvsp[0].StatementList);;
    break;}
case 82:
#line 340 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ElsifList=create_elsif_list(yyvsp[0].Elseif);;
    break;}
case 83:
#line 341 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{add_elsif_to_list(yyvsp[-1].ElsifList,yyvsp[0].Elseif);;
    break;}
case 84:
#line 344 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.IfStruct=create_if(yyvsp[-3].StatementList,yyvsp[-5].Expression, yyvsp[-2].ElsifList, NULL);;
    break;}
case 85:
#line 345 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.IfStruct=create_if(yyvsp[-5].StatementList,yyvsp[-7].Expression, yyvsp[-4].ElsifList, yyvsp[-2].StatementList);;
    break;}
case 86:
#line 346 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.IfStruct=create_if(yyvsp[-2].StatementList,yyvsp[-4].Expression, NULL, NULL);;
    break;}
case 87:
#line 347 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.IfStruct=create_if(yyvsp[-4].StatementList,yyvsp[-6].Expression, NULL, yyvsp[-2].StatementList);;
    break;}
case 88:
#line 350 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.WhenStruct=create_when(yyvsp[-2].ExprList,yyvsp[0].StatementList);;
    break;}
case 89:
#line 351 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.WhenStruct=create_when(NULL,yyvsp[0].StatementList);;
    break;}
case 90:
#line 354 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.WhenList=create_when_list(yyvsp[0].WhenStruct);;
    break;}
case 91:
#line 355 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{add_when_to_list(yyvsp[-1].WhenList,yyvsp[0].WhenStruct);;
    break;}
case 92:
#line 358 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.CaseStruct=create_case(yyvsp[-4].ExprList,yyvsp[-2].WhenList);;
    break;}
case 93:
#line 360 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.WhileLoop=create_while(yyvsp[-4].ExprList,yyvsp[-2].StatementList,0);;
    break;}
case 94:
#line 361 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.WhileLoop=create_while(yyvsp[-3].ExprList,yyvsp[-6].StatementList,1);;
    break;}
case 95:
#line 364 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ForLoop=create_for(0, yyvsp[-6].ExprList,yyvsp[-4].ExprList, yyvsp[-8].ExprList,yyvsp[-2].StatementList);;
    break;}
case 96:
#line 365 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.ForLoop=create_for(1, yyvsp[-6].ExprList,yyvsp[-4].ExprList, yyvsp[-9].ExprList,yyvsp[-2].StatementList);;
    break;}
case 97:
#line 368 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_proc(yyvsp[-8].Expression,yyvsp[-6].FuncArgs,yyvsp[-2].StatementList,NULL,0);;
    break;}
case 98:
#line 369 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_proc(yyvsp[-9].Expression,yyvsp[-7].FuncArgs,yyvsp[-2].StatementList,yyvsp[-4].StatementList,0);;
    break;}
case 99:
#line 371 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_proc(yyvsp[-4].Expression,NULL,NULL, NULL,0);;
    break;}
case 100:
#line 372 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_proc(yyvsp[-5].Expression,NULL,yyvsp[-2].StatementList, NULL,0);;
    break;}
case 101:
#line 373 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_proc(yyvsp[-6].Expression,NULL,yyvsp[-2].StatementList, yyvsp[-4].StatementList,0);;
    break;}
case 102:
#line 374 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_proc(yyvsp[-5].Expression,NULL,NULL, yyvsp[-3].StatementList,0);;
    break;}
case 103:
#line 376 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_func(yyvsp[-10].Expression,yyvsp[-8].FuncArgs,yyvsp[-2].StatementList,NULL, NULL,yyvsp[-5].VarType,0);;
    break;}
case 104:
#line 377 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_func(yyvsp[-11].Expression,yyvsp[-9].FuncArgs,yyvsp[-2].StatementList,yyvsp[-4].StatementList, NULL,yyvsp[-6].VarType,0);;
    break;}
case 105:
#line 379 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_func(yyvsp[-7].Expression,NULL,yyvsp[-2].StatementList, NULL, NULL,yyvsp[-5].VarType,0);;
    break;}
case 106:
#line 380 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_func(yyvsp[-8].Expression,NULL,yyvsp[-2].StatementList,yyvsp[-4].StatementList, NULL,yyvsp[-6].VarType,0);;
    break;}
case 107:
#line 383 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_proc(yyvsp[0].Expression,NULL,NULL, NULL,1);;
    break;}
case 108:
#line 384 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_proc(yyvsp[-3].Expression,yyvsp[-1].FuncArgs,NULL, NULL,1);;
    break;}
case 109:
#line 386 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_func(yyvsp[-5].Expression,yyvsp[-3].FuncArgs,NULL,NULL,NULL,yyvsp[0].VarType,1);;
    break;}
case 110:
#line 387 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncProc=create_func(yyvsp[-2].Expression,NULL,NULL,NULL,NULL,yyvsp[0].VarType,1);;
    break;}
case 111:
#line 390 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncArgs=create_args(yyvsp[0].VarListAssign);;
    break;}
case 112:
#line 391 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{add_arg_to_list(yyvsp[-2].FuncArgs,yyvsp[0].VarListAssign);;
    break;}
case 113:
#line 393 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncCall=create_func_call(func_proc, yyvsp[-1].ExprList, yyvsp[-3].Expression);;
    break;}
case 114:
#line 394 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncCall=create_func_call(func_proc, NULL, yyvsp[-2].Expression);;
    break;}
case 115:
#line 395 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncCall=create_func_call(first, create_expr_list(yyvsp[-2].Expression), yyvsp[-2].Expression);;
    break;}
case 116:
#line 396 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncCall=create_func_call(last, create_expr_list(yyvsp[-2].Expression), yyvsp[-2].Expression);;
    break;}
case 117:
#line 397 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"
{yyval.FuncCall=create_func_call(length, create_expr_list(yyvsp[-2].Expression), yyvsp[-2].Expression);;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 487 "bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 399 "c:\users\artem\documents\visual studio 2017\projects\adatojbc\parser.y"


void yyerror(const char *s)
{
    printf("%s",s);
}

struct NExpression* create_str_expr(char*value)
{
	char* buf=(char *)malloc(strlen(value)+1);
	strcpy(buf, value);
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_s;
	result->const_string = buf;
	result->next = NULL;
	printf("%s - cnst string\n",result->const_string);
    result->index = NULL;
	return result;
}

struct NStatement * create_arr_var(struct NArrayVar* arr)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->arr_var = arr;
	result->type = array_var;
	result->next = NULL;
	return result;
}

struct NName* create_name(char*name)
{
	char* buf=(char *)malloc(strlen(name)+1);
	strcpy(buf, name);
	struct NName * result = (struct NName*) malloc (sizeof(struct NName));
	result->next = NULL;
	result->name = buf;
	printf("%s - cn\n",result->name);
	return result;
}


struct NExpression* create_func_call_expr(struct NFuncCall * call)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression)); 
	result->type = func;
	result->next = NULL;
	result->call = call;
	result->index = NULL;
	return result;
}


struct NExpression* create_name_expr(struct NName * name)
{
	struct NNameList * temp = create_name_list(name);
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = name_list;
	result->next = NULL;
	result->list = temp;
	result->index = NULL;
	printf("%s - ce\n",result->list->first->name);
	return result;
}

struct NNameList* create_name_list(struct NName* name)
{
	struct NNameList * result = (struct NNameList*) malloc (sizeof(struct NNameList));
	result->first = name;
	
	namenumber++;
	result->number = namenumber;
	result->last = name;
	name->SList = result;
	printf("name-list %s\n",result->first->name);
	return result;
}

void add_name_to_list (struct NNameList*list, struct NName*added)
{
	printf("added name - %s\n",added->name);
	list->last->next = added;
	list->last = added;
	added->SList = list;
}

struct NExpression* create_int_expr(int value)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_i;
	result->list = NULL;
	result->left = NULL;
	result->right = NULL;
	result->next = NULL;
	result->index = NULL;
	result->const_int = value;
	return result;
}

struct NExpression* create_double_expr(double value)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_d;
	result->list = NULL;
	result->left = NULL;
	result->right = NULL;
	result->next = NULL;
	result->index = NULL;
	result->const_double = value;
	return result;
}

struct NExpression* create_bool_expr(int value)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_b;
	result->list = NULL;
	result->left = NULL;
	result->right = NULL;
	result->next = NULL;
	result->index = NULL;
	result->const_bool = value;
	return result;
}

struct NExpression* create_char_expr(char value)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = c_c;
	result->list = NULL;
	result->left = NULL;
	result->right = NULL;
	result->next = NULL;
	result->index = NULL;
	result->const_char = value;
	return result;
}

struct NExpression* create_empty_expr()
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->type = empty;
	result->left = NULL;
	result->right = NULL;
	result->index = NULL;
	result->list = NULL;
	result->next = NULL;
	return result;
}

struct NExpression* create_binary_expression(struct NExpression* Left,struct  NExpression* Right,enum NExprType type)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->left = Left;
	result->right = Right;
	result->next = NULL;
	result->type=type;
	printf("binary exp left\n");
	
	result->index = NULL;
	return result;
}

struct NExpression* create_binary_expression2(struct NExpression* Left, struct NExpression* Right, enum NExprType type, enum NVarType valueType)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	struct NName*name;
	struct NExpression* expr;
	struct NFuncCall* call;
	
    if(valueType == Float)
	name = create_name("Float");
	else name = create_name("Integer");
	expr = create_name_expr(name);
	return create_func_call_expr(create_func_call(func_proc,create_expr_list(Right),expr));
}

struct NExpression* create_unary_expression(struct NExpression* Left, enum NExprType type)
{
	struct NExpression * result = (struct NExpression*) malloc (sizeof(struct NExpression));
	result->left = Left;
	result->next = NULL;
	result->index = NULL;
	result->type=type;
	return result;
}

struct NVariable* create_variable(char*name,enum NVarType type,struct NExpression*expr)
{
	char* buf=(char *)malloc(strlen(name)+1);
	strcpy(buf, name);
	struct NVariable * result = (struct NVariable*) malloc (sizeof(struct NVariable));
	result->var_name = buf;
	printf("var name %s\n ",name);
	result->type=type;
	result->value=expr;
	result->list = NULL;
	result->next = NULL;
	return result;
}

struct NVariable* create_enum_variable(struct NExpression* exlist,struct NExprList*list)
{
	struct NVariable * result = (struct NVariable*) malloc (sizeof(struct NVariable));
	struct NNameList * names = create_name_list(create_name(list->first->list->first->name));
	struct NExpression * temp = list->first->next;
	while(temp!=NULL)
	{
		add_name_to_list(names,temp->list->first);
		temp = temp->next;
	}
	result->var_name = exlist->list->first->name;
	result->type=ENum;
	result->next = NULL;
	result->value = NULL;
	result->list=names;
	return result;
}

struct NVariable* create_default_variable(char*name)
{
	char* buf=(char *)malloc(strlen(name)+1);
	strcpy(buf, name);
	struct NVariable * result = (struct NVariable*) malloc (sizeof(struct NVariable));
	result->var_name = buf;
	result->next = NULL;
	return result;
}

struct NExprList* create_expr_list(struct NExpression*first)
{
	struct NExprList * result = (struct NExprList*) malloc (sizeof(struct NExprList));
	result->first = first;
	result->last = first;
	if(first->type == name_list)
	{
	printf("expr_list %s\n",result->first->list->first->name);
	}
	return result;
}

void add_expr_to_list(struct NExprList*list,struct NExpression*added)
{
	list->last->next = added;
	list->last = added;
}

struct NVariableList* create_var_list(char*first)
{
	char* buf=(char *)malloc(strlen(first)+1);
	strcpy(buf, first);
	struct NVariableList * result = (struct NVariableList*) malloc (sizeof(struct NVariableList));
	struct NVariable* var = create_variable(buf,Root_Integer,NULL);
	result->first = var;
	result->last = var;
	var->next = NULL;
	return result;
}

void add_var_to_list(struct NVariableList*list,char*added)
{
	char* buf=(char *)malloc(strlen(added)+1);
	strcpy(buf, added);
    struct NVariable* var = create_variable(buf,Root_Integer,NULL);
	list->last->next = var;
	list->last = var;
}

struct NArrayVar* create_array_var(struct NExprList* name, struct NExprList* leftSize, struct NExprList* rightSize, enum NVarType type,struct NExprList * list)
{
	struct NArrayVar * result = (struct NArrayVar*) malloc (sizeof(struct NArrayVar));
	result->arr_name = name->first->list->first->name;
	result->elem_type = type;
	result->leftSizeExpr = leftSize;
	result->rightSizeExpr = rightSize;
	if (leftSize->first->type==uminus)
		result->leftSize = -(leftSize->first->left->const_int);
	else
		result->leftSize = leftSize->first->const_int;
	if (rightSize->first->type==uminus)
		result->rightSize = -(rightSize->first->left->const_int);
	else
		result->rightSize = rightSize->first->const_int;
	result->expression = list;
	printf ("array var\n");
	return result;
}

struct NArrayAssign* create_array_assign1(struct NExpression*first_arr,struct NExpression* second_arr, struct NExprList* from_f, struct NExprList* to_f, struct NExprList*from_s, struct NExprList*to_s)
{
	struct NArrayAssign * result = (struct NArrayAssign*) malloc (sizeof(struct NArrayAssign));
	result->left = first_arr->list->first->name;
	result->right = second_arr->list->first->name;
	result->expr = NULL;
	result->from_left = from_f->first->const_int;
	result->to_left = to_f->first->const_int;;
	result->from_right = from_s->first->const_int;;
	result->to_right = to_s->first->const_int;;
	printf ("array assign\n");
	return result;
}


struct NArrayAssign* create_array_assign2(struct NExpression*first_arr, struct NExprList*from_f, struct NExprList* to_f,struct  NExprList * expr)
{
	struct NArrayAssign * result = (struct NArrayAssign*) malloc (sizeof(struct NArrayAssign));
	result->left = first_arr->list->first->name;
	result->right = NULL;
	result->expr = expr;
	result->from_left = from_f->first->const_int;
	result->to_left = to_f->first->const_int;
	result->from_right = -1;
	result->to_right = -1;
	return result;
}

struct NVarListAssign* create_var_list_assign(struct NExprList*list, struct NExpression* value, enum NVarType type, int isConst)
{
	struct NVarListAssign * result = (struct NVarListAssign*) malloc (sizeof(struct NVarListAssign));
	struct NName*n;
	struct NVariableList* varlist;
	struct NExpression * temp = list->first;
	printf("varlist assign %s \n",list->first->list->first->name);
	n=list->first->list->first;
	varlist = create_var_list(list->first->list->first->name);
	temp=temp->next;
	while(temp!=NULL)
	{

	    add_var_to_list(varlist,temp->list->first->name);
		temp=temp->next;
	} 
	change_varlist_type(varlist,type,value);
	change_varlist_const(varlist,isConst);
	result->variables = varlist;
	result->value = value;
	result->next = NULL;
	return result;
}

void change_varlist_type(struct NVariableList * list, enum NVarType type,struct NExpression* value)
{
	struct NVariable * temp;
	if(list!=NULL)
	{
	    temp = list->first;
		do
		{
			temp->type = type;
			temp->value = value;
			temp=temp->next;
		} while(temp!=NULL);
	}
}

void change_varlist_const(struct NVariableList * list, int isConst)
{
	struct NVariable * temp;
	if(list!=NULL)
	{
	    temp = list->first;
		do
		{
			temp->isConstant = isConst;
			temp=temp->next;
		} while(temp!=NULL);
	}
}

struct NStatement * create_expr_statement(struct NExprList* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = expr;
	result->next = NULL;
	result->expressions = exp;
	printf("expr_stmt \n");
	return result;
}

struct NStatement * create_exc_statement(struct NException * exc)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = exception;
	result->next = NULL;
	result->except = exc;
	return result;
}

struct NStatement * create_for_statement(struct NForLoop* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = for_;
	result->next = NULL;
	result->for_loop = exp;
	return result;
}
struct NStatement * create_while_statement(struct NWhileLoop* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = while_;
	result->next = NULL;
	result->while_loop = exp;
	return result;
}

struct NStatement * create_enum_statement(struct NVariable*exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = en;
	result->next = NULL;
	result->enu = exp;
	return result;
}

struct NStatement * create_return_statement(struct NExprList* return_val)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = return_;
	result->next = NULL;
	result->return_st=(struct NReturn*) malloc (sizeof(struct NReturn));
	result->return_st->returnValue=return_val->first;
	return result;
}

struct NStatement * create_case_statement(struct NCaseStruct* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = case_;
	result->next = NULL;
	result->case_st = exp;
	return result;
}
struct NStatement * create_if_statement(struct NIfStruct* exp)
{
	struct NStatement * 
	result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = if_;
	result->next = NULL;
	result->if_struct = exp;
	printf ("if_statement\n");
	return result;
}
struct NStatement * create_func_statement(struct NFuncProc* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = func_pr;
	result->next = NULL;
	result->func_proc = exp;
	return result;
}
struct NStatement * create_call_statement(struct NFuncCall* exp)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = func_call;
	result->next = NULL;
	result->func_call = exp;
	return result;
}

struct NStatement * create_arr_statement(struct NArrayAssign* arr)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = array_assign;
	result->next = NULL;
	result->array_assign = arr;
	return result;
}

struct NStatement * create_list_statement(struct NVarListAssign* list)
{
	struct NStatement * result = (struct NStatement*) malloc (sizeof(struct NStatement));
	result->type = var_list_assign;
	result->next = NULL;
	result->list = list;
	return result;
}

struct NStatementList * create_statement_list(struct NStatement* exp)
{
	
	struct NStatementList * result = (struct NStatementList*) malloc (sizeof(struct NStatementList));
	result->first = exp;
	listnumber++;
	result->number = listnumber;
	result->last = exp;
	exp->Slist = result;
	printf("stmt list\n");
	return result;
}

void add_statement_to_list(struct NStatementList*list,struct NStatement*statement)
{
	statement->Slist = list;
	list->last->next = statement;
	list->last = statement;
}

struct NElsif* create_elsif(struct NExpression * expr, struct NStatementList*body)
{
	struct NElsif * result = (struct NElsif*) malloc (sizeof(struct NElsif));
	result->elsifExpr = expr;
	result->body = body;
	result->next = NULL;
	return result;
}

struct NElsifList* create_elsif_list(struct NElsif*elsif)
{
	struct NElsifList * result = (struct NElsifList*) malloc (sizeof(struct NElsifList));
	result->first = elsif;
	result->last = elsif;
	return result;
}

void add_elsif_to_list(struct NElsifList* list, struct NElsif * elsif)
{
	
	list->last->next = elsif;
	list->last = elsif;
}

struct NIfStruct * create_if(struct NStatementList * if_stmt,struct NExpression * if_expr, struct NElsifList * elsif_list, struct NStatementList * else_stmt)
{
	struct NIfStruct * result = (struct NIfStruct*) malloc (sizeof(struct NIfStruct));
	result->ifStmt = if_stmt;
	result->ifExpr=if_expr;
	result->elsifList = elsif_list;
	result->elseStmt = else_stmt;
	return result;
}

struct NWhenStruct* create_when(struct NExprList * whenExpr, struct NStatementList * body)
{
	struct NWhenStruct * result = (struct NWhenStruct*) malloc (sizeof(struct NWhenStruct));
	result->whenExpr = whenExpr;
	result->next = NULL;
	result->body = body;
	return result;
}

struct NWhenList* create_when_list(struct NWhenStruct* whenStr)
{
	struct NWhenList * result = (struct NWhenList*) malloc (sizeof(struct NWhenList));
	result->first = whenStr;
	result->last = whenStr;
	return result;
}

void add_when_to_list(struct NWhenList* list, struct NWhenStruct * added)
{
	list->last->next = added;
	list->last = added;
}

struct NCaseStruct * create_case(struct NExprList*var, struct NWhenList*body)
{
	struct NCaseStruct * result = (struct NCaseStruct*) malloc (sizeof(struct NCaseStruct));
	result->caseVar = var->first->list;
	result->body = body;
	return result;
}

struct NWhileLoop * create_while(struct NExprList*expr, struct NStatementList*body, int doWh)
{
	struct NWhileLoop * result = (struct NWhileLoop*) malloc (sizeof(struct NWhileLoop));
	result->condition = expr->first;
	result->body = body;
	result->doWhile=doWh;
	return result;
}

struct NForLoop * create_for(int inRev,struct  NExprList* beg,struct NExprList* end, struct NExprList*expr,struct  NStatementList*body)
{
	struct NForLoop * result = (struct NForLoop*) malloc (sizeof(struct NForLoop));
	result->inReverse = inRev;
	result->begin = beg->first;
	result->end = end->first;
	result->condition = expr->first->list;
	result->body=body;
	return result;
}

struct NFuncProc * create_func (struct NExpression*name,struct NFuncArgs*args,struct NStatementList * body,struct NStatementList*vars, struct NExprList * ret, enum NVarType type,int pr)
{
	struct NFuncProc * result = (struct NFuncProc*) malloc (sizeof(struct NFuncProc));
	result->isFunc = 1;	
	if(ret!=NULL)
	result->returnValue = ret->first;
	else result->returnValue = NULL;
	result->name = name->list->first->name;
	result->arguments = args;
	result->body = body;
	result->localVars = vars;
	result->isPrototype = pr;
	result->type = type;
	printf("Func created\n");
	return result;
}

struct NFuncProc * create_proc (struct NExpression*name,struct NFuncArgs*args,struct NStatementList * body,struct NStatementList*vars,int pr)
{
	struct NFuncProc * result = (struct NFuncProc*) malloc (sizeof(struct NFuncProc));
	result->isFunc = 0;
	result->name=name->list->first->name;
	result->arguments = args;
	result->body = body;
	result->type = Root_Integer;
	result->isPrototype = pr;
	result->localVars = vars;
	result->returnValue = NULL;
	return result;
}

struct NFuncArgs * create_args(struct NVarListAssign*expr)
{
	struct NFuncArgs * result = (struct NFuncArgs*) malloc (sizeof(struct NFuncArgs));
	result->first = expr;
	result->first->next = NULL;
	result->last = expr;
	return result;
}

void add_arg_to_list(struct NFuncArgs*list, struct NVarListAssign* added)
{
	list->last->next = added;
	list->last = added;
}

struct NFuncCall * create_func_call(enum NFuncType type, struct NExprList*args, struct NExpression*name)
{
	struct NFuncCall * result = (struct NFuncCall*) malloc (sizeof(struct NFuncCall));
	if(type == length)
	{
		result->func = "LENGTH";
	}
	else if(type == first)
	{
		result->func = "FIRST";
	}
	else if(type == last)
	{
		result->func = "LAST";
	}
	else
	{
		result->func = name->list->first->name;
	}
	result->type = type;
	result->arguments = args;
		return result;
}

struct NException * create_exc(struct NExcBodyList * body)
{
	struct NException * result = (struct NException*) malloc (sizeof(struct NException));
	result->list = body;
	return result;
}

struct NExcBody * create_exc_body(struct NStatementList * body, struct NExprList*name)
{
	struct NExcBody * result = (struct NExcBody*) malloc (sizeof(struct NExcBody));
	if(name == NULL)
	{
	result->name = "Others";
	}
	else 
	{
	result->name = name->first->list->first->name;
	}
	result->body = body;
	return result;
}

void add_exc_to_list(struct NExcBodyList*list, struct NExcBody* added)
{
	list->last->next = added;
	list->last = added;
}

struct NExcBodyList*create_exc_body_list(struct NExcBody*body)
{
	struct NExcBodyList * result = (struct NExcBodyList*) malloc (sizeof(struct NExcBodyList));
	result->first = body;
	result->last = body;
	return result;
}