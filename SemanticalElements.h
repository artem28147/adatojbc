#include "tree_structures.h"
#include <string>
#include <vector>



enum SemanticalConst
{   
    CONST_Utf8 = 1, 
    CONST_Integer = 3, 
	CONST_Float = 4,
	CONST_Class = 7, 
    CONST_String = 8,
	CONST_Fieldref = 9,              
    CONST_Methodref = 10, 
    CONST_NameAndType = 12
};


struct SemanticalElement
{
	int id;
	std::string str;
	float const_float;
	int const_int;
	struct SemanticalElement* first;
	struct SemanticalElement* second;
	enum SemanticalConst type;

	SemanticalElement()
    {
		first = NULL;
		second = NULL;
        id = 0;
        const_int = -1;
		const_float = -1;
		str.clear();
    }
};

struct ClassElement
{
	struct SemanticalElement* classname;
	struct SemanticalElement* parentname;
	std::vector<struct FieldElement> * fields;
	std::vector<struct MethodElement> * methods;

	ClassElement()
	{
		classname = NULL;
		parentname = NULL;
		fields = new std::vector<struct FieldElement>;
		methods = new std::vector<struct MethodElement>;
	}
};

struct FieldElement
{
	struct SemanticalElement* name;
	struct SemanticalElement* desc;
	NExprList * leftSizeExpr;
	NExprList * rightSizeExpr;
	bool is_constant;

	FieldElement()
	{
		name = NULL;
		desc = NULL;
		is_constant = false;
		leftSizeExpr = NULL;
		rightSizeExpr = NULL;
	}
};

struct LocalElement
{
	int id;
	std::string name;
	enum NVarType type;
	enum NVarType elem_type;
	int leftSize;
	int rightSize;
	NExprList * leftSizeExpr;
	NExprList * rightSizeExpr;
	bool isConst;
	struct NVariable * var;
	LocalElement()
	{
		id = 0;
		name.clear();
		type = Root_Integer;
		isConst = false;
	}
};

struct MethodElement
{
	struct SemanticalElement* name;
	struct SemanticalElement* desc;
	int minNum;
	int maxNum;
	std::vector<struct LocalElement>* localvars;

	MethodElement()
	{
		name = NULL;
		desc = NULL;
		minNum = 0;
		maxNum = 0;
		localvars = new std::vector<struct LocalElement>;
	}
};

