%{
 #include <stdlib.h>
 #include <conio.h>
 #include <string.h>
 #include <stdio.h>
 #include <math.h>
 #include "parser_tab.h"

char var2[100]="";
char var[1000] = "";
char ex[10]="";
char digit[100]="";
char power[3]="";
char * dig;
long digitr;
int i,k;
int j,n;

extern YYSTYPE yylval;

%}
%option noyywrap
%option never-interactive
%option caseless
%x STRING
%x comment
%%

\" {strcpy(var,""); BEGIN(STRING);}
<STRING>[^"]* {strcat(var,yytext);}
<STRING>(\"\")* {strncat(var,yytext,strlen(yytext)/2);}
<STRING>\" {yylval.ConstString = (char *)malloc(strlen(var) + 1); strcpy(yylval.ConstString, var); BEGIN(INITIAL); return CONST_STRING;}

"--" {BEGIN(comment);}
<comment>[^\n]* 
<comment>"\n" {BEGIN(INITIAL);}

" "|"	"|"\n"|"\r" {BEGIN(INITIAL);}

ABORT {return ABORT;}
ABS {return ABS;}
ABSTRACT {return ABSTRACT;}
ACCEPT {return ACCEPT;}
ALIASED {return ALIASED;}
AND {return AND;}
AT {return AT;}
BEGIN {return BEGGIN;}
BODY {return BODY;}
CASE {return CASE;}
CONSTANT {return CONSTANT;}
DECLARE {return DECLARE;}
DELAY {return DELAY;}
DELTA {return DELTA;}
DIGITS {return DIGITS;}
DO {return DO;}
ELSE {return ELSE;}
ELSIF {return ELSIF;}
END {return END;}
ENTRY {return ENTRY;}
EXCEPTION {return EXCEPTION;}
EXIT {return EXIT;}
FALSE {yylval.ConstBool = 0; return CONST_BOOL;}
FIRST {return FIRST;}
FOR {return FOR;}
FUNCTION {return FUNCTION;}
GENERIC {return GENERIC;}
IF {return IF;}
IN {return IN;}
IS {return IS;}
LAST {return LAST;}
LENGTH {return LENGTH;}
LIMITED {return LIMITED;}
LOOP {return LOOP;}
MOD {return MOD;}
NEW {return NEW;}
NOT {return NOT;}
NULL {return NOLL;}
OF {return OF;}
OR {return OR;}
OTHERS {return OTHERS;}
OUT {return OUT;}
PRAGMA {return PRAGMA;}
PRIVATE {return PRIVATE;}
PROCEDURE {return PROCEDURE;}
PROTECTED {return PROTECTED;}
RAISE {return RAISE;}
RANGE {return RANGE;}
RENAMES {return RENAMES;}
REQUEUE {return REQUEUE;}
RETURN {return RETURN;}
REVERSE {return REVERSE;}
SELECT {return SELECT;}
SEPARATE {return SEPARATE;}
SUBTYPE {return SUBTYPE;}
TERMINATE {return TERMINATE;}
THEN {return THEN;}
TRUE {yylval.ConstBool = 1; return CONST_BOOL;}
TYPE {return TYPE;}
UNTIL {return UNTIL;}
USE {return USE;}
WHEN {return WHEN;}
WHILE {return WHILE;}
XOR {return XOR;}

REAL {return REALTYPE;}
FLOAT {return FLOATTYPE;}
ROOT_REAL {return REALTYPE;}
ROOT_INTEGER {return ROOTINTTYPE;}
INTEGER {return INTTYPE;}
CHARACTER {return CHARTYPE;}
BOOLEAN {return BOOLTYPE;}
ARRAY {return ARRAYTYPE;}
STRING {return STRINGTYPE;}
RECORD {return RECORDTYPE;}

"/" {return (unsigned char) '/';}
"\+" {return (unsigned char) '+';}
"|" {return (unsigned char) '|';}
"=" {return (unsigned char) '=';}
":=" {return ASSIGN;}
"\\=" {return NE;}
"<=" {return LE;}
">=" {return GE;}
"=>" {return ENSUE;}
"&" {return (unsigned char) '&';}
"-" {return (unsigned char) '-';}
"\\" {return (unsigned char) '\\';}
"\*" {return (unsigned char) '*';}
"\*\*" {return GRADE;}
">" {return (unsigned char) '>';}
"<" {return (unsigned char) '<';}
"'" {return (unsigned char) '\'';}


['](.)['] {strcpy(var,"");strcpy(var,yytext); yylval.ConstChar = var[0]; return CONST_CHAR;}
[a-zA-Z](_?[a-zA-Z0-9])* {yylval.VarName=(char *)malloc(strlen(yytext)+1);  strcpy(yylval.VarName, yytext); return NAME;}
[0-9]{1,}_?[0-9]*(E[+-]{0,1}[0-9]*)* {strcpy(var,""); strcpy(var2,""); strcpy(ex,""); strcpy(var,yytext);n=0; j=0; i=0;k=0; for(i=0;i<strlen(var);i++){if((var[i]!='_')&&(var[i]!='E'&&var[i]!='e')){var2[j] = var[i];j++;}else if((var[i]=='E'||var[i]=='e')){for (k = i+1;k<strlen(var);k++){ex[n] = var[k];n++;} i = strlen(var);} }ex[n] = '\0'; var2[j] = '\0'; if(strlen(ex)>0){ j = strtol(ex, NULL, 10); yylval.ConstInt = atoi(var2)*pow(10,j);} else yylval.ConstInt = atoi(var2); return CONST_INT; }
[0-9]{1,}[#][0-9](_?[0-9A-F]{1,})*[#] {strcpy(var,""); strcpy(digit,""); dig = NULL; strcpy(power,"");strcpy(var,yytext); i=0; while(var[i]!='#'){power[i]=var[i];i++;};power[i]='\0'; j = 0;i++; while(var[i]!='#'){if(var[i]!='_'){digit[j]=var[i];j++;}i++;};digit[j]='\0'; digitr = strtol(digit,&dig,atoi(power));strcpy(var,yytext); yylval.ConstInt = (int)digitr; return CONST_INT;}
([0-9]){1,}[.]([0-9]{1,}){1,}(E[+-]{0,1}[0-9]*)* {strcpy(var,""); strcpy(var2,""); strcpy(ex,""); strcpy(var,yytext);n=0; j=0; i=0;k=0; for(i=0;i<strlen(var);i++){if((var[i]!='_')&&(var[i]!='E'&&var[i]!='e')){var2[j] = var[i];j++;}else if((var[i]=='E'||var[i]=='e')){for (k = i+1;k<strlen(var);k++){ex[n] = var[k];n++;} i = strlen(var);} }ex[n] = '\0'; var2[j] = '\0';  if(strlen(ex)>0){ j = strtol(ex, NULL, 10);yylval.ConstDouble = atof(var2)*pow(10,j);} else yylval.ConstDouble = atof(var2); return CONST_DOUBLE;}
"." {return (unsigned char) '.';}
"," {return (unsigned char) ',';}
":" {return (unsigned char) ':';}
";" {return (unsigned char) ';';}
".." {return ARRAYRANGE;}
"(" {return (unsigned char) '(';}
")" {return (unsigned char) ')';}
. {printf("found unexpected character %s\n",yytext);}
"\\n" {printf("found \\n character");}

<<EOF>> {return 0;}
%%