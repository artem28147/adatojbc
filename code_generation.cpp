#include <stdio.h>
#include <fstream>
#include "SemanticalFunctions.h"
#include "code_generation.h"

std::ofstream file_of_class; // ���� ������
std::vector<std::vector<char>> code_of_methods; //���� ��� ��� �������
std::vector<char> all_code; //���� ���� ���
std::string file_name; //��� �����
MethodElement* cur_method; //������� ����� � ������� �������
int methodCounter = -1;
int loopCounter = -1;
char* loopVarName;
std::vector<char*> loopVarNames;
bool arrInitializing = false;
bool returnCreated = false;
union u4
{
	unsigned long int number;
	char bytes[4];
};

union u2
{
	unsigned long int number;
	char bytes[2];
};

union s4
{
	signed long int number;
	float flnumber;
	char bytes[4];
};

union s2
{
	signed long int number;
	float flnumber;
	char bytes[2];
};
std::vector<NVarType> doArgListFromDesc(MethodElement mel) 
{
	std::vector<NVarType> args;
	for (int i = 0; i < mel.desc->str.size(); i++)
	{
		if (mel.desc->str.at(i) == ')')
			break;
		else if (mel.desc->str.at(i) == 'I')
		{
			args.push_back(Integer);
		}
		else if (mel.desc->str.at(i) == 'B')
		{
			args.push_back(Float);
		}
		else if (mel.desc->str.at(i) == 'C')
		{
			args.push_back(Character);
		}
		else if (mel.desc->str.at(i) == 'F')
		{
			args.push_back(Float);
		}
		else if (mel.desc->str.at(i) == 'L')
		{
			args.push_back(String);
		}
	}
	return args;
}
void code_const_table()
{
	int i;
	for (i = 0; i < table_of_const.size(); i++)
	{
		switch (table_of_const[i]->type)
		{
		case CONST_Utf8:
		{
			code_number(1, 1);
			code_utf8(table_of_const[i]);
			break;
		}
		case CONST_Integer:
		{
			code_number(3, 1);
			code_integer(table_of_const[i]);
			break;
		}
		case CONST_Float:
		{
			code_number(4, 1);
			code_float(table_of_const[i]);
			break;
		}
		case CONST_Class:
		{
			code_number(7, 1);
			code_class(table_of_const[i]);
			break;
		}
		case CONST_String:
		{
			code_number(8, 1);
			code_string(table_of_const[i]);
			break;
		}
		case CONST_Fieldref:
		{
			code_number(9, 1);
			code_fieldref(table_of_const[i]);
			break;
		}
		case CONST_Methodref:
		{
			code_number(10, 1);
			code_methodref(table_of_const[i]);
			break;
		}
		case CONST_NameAndType:
		{
			code_number(12, 1);
			code_name_and_type(table_of_const[i]);
			break;
		}
		}
	}
}

void code_field_table()
{
	for (int i = 0; i < parent_class->fields->size(); i++)
	{
		unsigned long int number;
		code_number(0x0008, 2);
		number = parent_class->fields->at(i).name->id;
		code_number(number, 2);
		number = parent_class->fields->at(i).desc->id;
		code_number(number, 2);
		number = 0;
		code_number(number, 2);
	}
}

void code_method_table()
{

	for (int i = 0; i < parent_class->methods->size(); i++)
	{
		unsigned long int number;
		if (parent_class->methods->size() != code_of_methods.size())
		{
			code_of_methods.insert(code_of_methods.end(), std::vector<char>());
		}

		code_number(0x0009, 2);
		number = parent_class->methods->at(i).name->id;
		code_number(number, 2);
		number = parent_class->methods->at(i).desc->id;
		code_number(number, 2);
		number = 1;
		code_number(number, 2);
		//1000 ���. ����������
		code_method_class(code_of_methods[i], 1000);
	}
}

void code_to_file(std::vector<char> & byte_code)
{
	char* arr;
	std::ofstream classFile("byteCode.class", std::ios::binary | std::ios::out);

	if (byte_code.size() != 0)
	{
		file_of_class.write(&byte_code[0], byte_code.size());
	}

}

void code_method_class(std::vector<char> & byte_code, int index)
{
	unsigned long int number;
	number = table_of_const.at(table_of_const.size() - 1)->id;
	code_number(number, 2);
	number = 12 + byte_code.size();
	code_number(number, 4);
	number = 1000;
	code_number(number, 2);
	number = index;
	code_number(number, 2);
	number = byte_code.size();
	code_number(number, 4);
	for (int i = 0; i < byte_code.size(); i++)
	{
		all_code.insert(all_code.end(), byte_code[i]);
	}
	number = 0;
	code_number(number, 2);
	code_number(number, 2);
}



void code_class_table()
{

}

void code_methodref(SemanticalElement* element)
{
	int number;
	number = element->first->id;
	code_number(number, 2);
	number = element->second->id;
	code_number(number, 2);
}

void code_float(SemanticalElement* element)
{
	float number;
	number = element->const_float;
	code_float_number(number, 4);
}

void code_fieldref(SemanticalElement* element)
{
	int number;
	number = element->first->id;
	code_number(number, 2);
	number = element->second->id;
	code_number(number, 2);
}

void code_class(SemanticalElement* element)
{
	int number;
	number = element->first->id;
	code_number(number, 2);
}

void code_name_and_type(SemanticalElement* element)
{
	int number;
	number = element->first->id;
	code_number(number, 2);
	number = element->second->id;
	code_number(number, 2);
}

void code_string(SemanticalElement* element)
{
	int number;
	number = element->first->id;
	code_number(number, 2);
}


void code_integer(SemanticalElement* element)
{
	int number;
	number = element->const_int;
	code_number(number, 4);
}

void code_utf8(SemanticalElement* element)
{
	int number;
	char* bytes = (char*)malloc(element->str.length());
	number = element->str.length();
	code_number(number, 2);
	strcpy(bytes, element->str.c_str());
	for (int i = 0; i < element->str.length(); i++)
	{
		number = bytes[i];
		code_number(number, 1);
	}
}


void reverse_bytes(char* bytes, int size)
{
	char temp;
	for (int i = 0; i < size / 2; i++)
	{
		temp = bytes[i];
		bytes[i] = bytes[size - 1 - i];
		bytes[size - 1 - i] = temp;
	}
}

union u2 make_reversed_u2(unsigned long int number)
{
	union u2 changer;
	changer.number = number;
	reverse_bytes(changer.bytes, 2);
	return changer;
}

union u4 make_reversed_u4(unsigned long int number)
{
	union u4 changer;
	changer.number = number;
	reverse_bytes(changer.bytes, 4);
	return changer;
}

union s2 make_reversed_s2(signed long int number)
{
	union s2 changer;
	changer.number = number;
	reverse_bytes(changer.bytes, 2);
	return changer;
}

union s2 make_reversed_float_s2(float number)
{
	union s2 changer;
	changer.flnumber = number;
	reverse_bytes(changer.bytes, 2);
	return changer;
}

union s4 make_reversed_s4(signed long int number)
{
	union s4 changer;
	changer.number = number;
	reverse_bytes(changer.bytes, 4);
	return changer;
}

union s4 make_reversed_float_s4(float number)
{
	union s4 changer;
	changer.flnumber = number;
	reverse_bytes(changer.bytes, 4);
	return changer;
}

void code_number(unsigned long int number, int size)
{
	int i;
	if (size == 1)
	{
		all_code.insert(all_code.end(), (char)number);
	}
	else if (size == 2)
	{
		union u2 temp = make_reversed_u2(number);
		for (i = 0; i < 2; i++)
		{
			all_code.insert(all_code.end(), temp.bytes[i]);
		}
	}
	else if (size == 4)
	{
		union u4 temp = make_reversed_u4(number);
		for (i = 0; i < 4; i++)
		{
			all_code.insert(all_code.end(), temp.bytes[i]);
		}
	}
}

void code_signed_number(long int number, int size)
{
	int i;
	if (size == 1)
	{
		all_code.insert(all_code.end(), (char)number);
	}
	else if (size == 2)
	{
		union s2 temp = make_reversed_s2(number);
		for (i = 0; i < 2; i++)
		{
			all_code.insert(all_code.end(), temp.bytes[i]);
		}
	}
	else if (size == 4)
	{
		union s4 temp = make_reversed_s4(number);
		for (i = 0; i < 4; i++)
		{
			all_code.insert(all_code.end(), temp.bytes[i]);
		}
	}
}

void code_float_number(float number, int size)
{
	int i;
	if (size == 1)
	{
		all_code.insert(all_code.end(), (char)number);
	}
	else if (size == 2)
	{
		union s2 temp = make_reversed_float_s2(number);
		for (i = 0; i < 2; i++)
		{
			all_code.insert(all_code.end(), temp.bytes[i]);
		}
	}
	else if (size == 4)
	{
		union s4 temp = make_reversed_float_s4(number);
		for (i = 0; i < 4; i++)
		{
			all_code.insert(all_code.end(), temp.bytes[i]);
		}
	}
}

void write_code_to_class_file(unsigned long int number, int size)
{

	int i;
	if (size == 1)
	{
		char temp = (char)number;
		file_of_class.write(&temp, 4);
	}
	else if (size == 2)
	{
		union u2 temp = make_reversed_u2(number);
		file_of_class.write(&temp.bytes[0], 2);
	}
	else if (size == 4)
	{
		union u4 temp = make_reversed_u4(number);
		file_of_class.write(&temp.bytes[0], 4);
	}
}

void generate_expr_code(NExpression*expr)
{
	NExprType currentType = expr->type;
	// add currentType=assign 
	if (currentType == greater || currentType == equal || currentType == less || currentType == ne || currentType == ge || currentType == le)
	{
		NVarType *arrVarType = NULL;
		generate_expr_code(expr->left);
		generate_expr_code(expr->right);
		switch (currentType)
		{
		case greater:
		{
			if ((arrVarType != NULL && (*arrVarType == Integer || *arrVarType == Character || *arrVarType == Boolean))
				|| (arrVarType == NULL && ((expr->valueType == Integer || expr->valueType == c_i || expr->valueType == Boolean || expr->valueType == Character))))
			{
				code_number(IF_ICMPGT, 1);
			}
			else if ((arrVarType != NULL && (*arrVarType == Float || *arrVarType == c_d)) || (arrVarType == NULL && (expr->valueType == Float || expr->valueType == c_d)))
			{
				code_number(FCMPG, 1);
				code_number(ICONST_1, 1);
				code_number(IF_ICMPEQ, 1);
			}
			code_number(7, 2);
			code_number(ICONST_0, 1);
			code_number(GO_TO, 1);
			code_number(4, 2);
			code_number(ICONST_1, 1);
			break;
		}
		case equal:
		{
			if ((arrVarType != NULL && (*arrVarType == Integer || *arrVarType == Character || *arrVarType == Boolean))
				|| (arrVarType == NULL && ((expr->valueType == Integer || expr->valueType == c_i || expr->valueType == Boolean || expr->valueType == Character))))
			{
				code_number(IF_ICMPEQ, 1);
			}
			else if ((arrVarType != NULL && (*arrVarType == Float || *arrVarType == c_d)) || (arrVarType == NULL && (expr->valueType == Float || expr->valueType == c_d)))
			{
				code_number(FCMPG, 1);
				code_number(ICONST_0, 1);
				code_number(IF_ICMPEQ, 1);
			}
			code_number(7, 2);
			code_number(ICONST_0, 1);
			code_number(GO_TO, 1);
			code_number(4, 2);
			code_number(ICONST_1, 1);
			break;

		}
		case less:
		{
			if ((arrVarType != NULL && (*arrVarType == Integer || *arrVarType == Character || *arrVarType == Boolean))
				|| (arrVarType == NULL && ((expr->valueType == Integer || expr->valueType == c_i || expr->valueType == Boolean || expr->valueType == Character))))
			{
				code_number(IF_ICMPLT, 1);
			}
			else if ((arrVarType != NULL && (*arrVarType == Float || *arrVarType == c_d)) || (arrVarType == NULL && (expr->valueType == Float || expr->valueType == c_d)))
			{
				code_number(FCMPG, 1);
				code_number(ICONST_M1, 1);
				code_number(IF_ICMPEQ, 1);
			}
			code_number(7, 2);
			code_number(ICONST_0, 1);
			code_number(GO_TO, 1);
			code_number(4, 2);
			code_number(ICONST_1, 1);
			break;
		}
		case ne:
		{
			if ((arrVarType != NULL && (*arrVarType == Integer || *arrVarType == Character || *arrVarType == Boolean))
				|| (arrVarType == NULL && ((expr->valueType == Integer || expr->valueType == c_i || expr->valueType == Boolean || expr->valueType == Character))))
			{
				code_number(IF_ICMPNE, 1);
			}
			else if ((arrVarType != NULL && (*arrVarType == Float || *arrVarType == c_d)) || (arrVarType == NULL && (expr->valueType == Float || expr->valueType == c_d)))
			{
				code_number(FCMPG, 1);
				code_number(ICONST_0, 1);
				code_number(IF_ICMPNE, 1);
			}
			code_number(7, 2);
			code_number(ICONST_0, 1);
			code_number(GO_TO, 1);
			code_number(4, 2);
			code_number(ICONST_1, 1);
			break;
		}
		case ge:
		{
			if ((arrVarType != NULL && (*arrVarType == Integer || *arrVarType == Character || *arrVarType == Boolean))
				|| (arrVarType == NULL && ((expr->valueType == Integer || expr->valueType == c_i || expr->valueType == Boolean || expr->valueType == Character))))
			{
				code_number(IF_ICMPGE, 1);
			}
			else if ((arrVarType != NULL && (*arrVarType == Float || *arrVarType == c_d)) || (arrVarType == NULL && (expr->valueType == Float || expr->valueType == c_d)))
			{
				code_number(FCMPG, 1);
				code_number(ICONST_1, 1);
				code_number(IF_ICMPEQ, 1);
				code_number(7, 2);
				code_number(ICONST_0, 1);
				code_number(GO_TO, 1);
				code_number(7, 2);
				code_number(ICONST_1, 1);
				code_number(GO_TO, 1);
				code_number(14, 2);
				code_number(POP, 1);
				generate_expr_code(expr->left);
				generate_expr_code(expr->right);
				code_number(FCMPG, 1);
				code_number(ICONST_0, 1);
				code_number(IF_ICMPEQ, 1);

			}
			code_number(7, 2);
			code_number(ICONST_0, 1);
			code_number(GO_TO, 1);
			code_number(4, 2);
			code_number(ICONST_1, 1);
			break;
		}
		case le:
		{
			if ((arrVarType != NULL && (*arrVarType == Integer || *arrVarType == Character || *arrVarType == Boolean))
				|| (arrVarType == NULL && ((expr->valueType == Integer || expr->valueType == c_i || expr->valueType == Boolean || expr->valueType == Character))))
			{
				code_number(IF_ICMPLE, 1);
			}
			else if ((arrVarType != NULL && (*arrVarType == Float || *arrVarType == c_d)) || (arrVarType == NULL && (expr->valueType == Float || expr->valueType == c_d)))
			{
				code_number(FCMPG, 1);
				code_number(ICONST_M1, 1);
				code_number(IF_ICMPEQ, 1);
				code_number(7, 2);
				code_number(ICONST_0, 1);
				code_number(GO_TO, 1);
				code_number(7, 2);
				code_number(ICONST_1, 1);
				code_number(GO_TO, 1);
				code_number(14, 2);
				code_number(POP, 1);
				generate_expr_code(expr->left);
				generate_expr_code(expr->right);
				code_number(FCMPG, 1);
				code_number(ICONST_0, 1);
				code_number(IF_ICMPEQ, 1);
			}
			code_number(7, 2);
			code_number(ICONST_0, 1);
			code_number(GO_TO, 1);
			code_number(4, 2);
			code_number(ICONST_1, 1);
			break;
		}
		}

	}
	switch (currentType)
	{
	case name_list:
	{
		generate_name_list_code(expr->list);
		break;
	}
	case assign:
	{
		if (loopVarName != NULL && expr->left->type != func && strcmp(loopVarName, expr->left->list->first->name) == 0)
		{
			throw ("You can't assign to loop variable");
		}
		else
		{
			LocalElement * el;
			//��� ��������
			if (expr->left->type == func)
			{
				bool arrayIsLocal = false;
				bool arrayIsGlobal = false;
				std::vector<struct LocalElement> *vars;
				int i;
				for (i = 0; i < table_of_classes[1].methods->size(); i++)
				{
					if (strcmp(table_of_classes[1].methods->at(i).name->str.c_str(), current_method->name->str.c_str()) == 0)
						vars = table_of_classes[1].methods->at(i).localvars;
				}
				for (i = 0; i < vars->size(); i++)
				{
					if (strcmp(expr->left->call->func, vars->at(i).name.c_str()) == 0 && NVarType::Array)
					{
						arrayIsLocal = true;
						break;
					}
				}
				if (arrayIsLocal)
				{
					code_number(ALOAD, 1);
					code_number(vars->at(i).id, 1);
				}
				else
				{
					for (i = 0; i < table_of_classes[1].fields->size(); i++)
					{
						if (strcmp(table_of_classes[1].fields->at(i).name->str.c_str(), expr->left->call->func) == 0)
						{
							arrayIsGlobal = true;
							break;
						}
					}
				}
				if (arrayIsGlobal)
				{
					code_number(GETSTATIC, 1);
					code_number(table_of_classes[1].fields->at(i).desc->id + 2, 2);
				}
				generate_expr_list_code(expr->left->call->arguments);
				if (arrayIsGlobal)
					generate_expr_list_code(table_of_classes[1].fields->at(i).leftSizeExpr);
				if (arrayIsLocal)
					generate_expr_list_code(vars->at(i).leftSizeExpr);
				code_number(ISUB, 1);
				generate_expr_code(expr->right);
				if (expr->valueType == Integer)
					code_number(IASTORE, 1);
				else if (expr->valueType == Float)
					code_number(FASTORE, 1);
				else if (expr->valueType == Boolean)
					code_number(IASTORE, 1);
				else if (expr->valueType == String)
					code_number(AASTORE, 1);
			}
			//��� ���������
			else
			{
				generate_expr_code(expr->right);
				LocalElement *lel = NULL;
				FieldElement *lfel = NULL;
				LocalElement *rel = NULL;
				FieldElement *rfel = NULL;
				lfel = is_in_field_vars(expr->left->list->first->name);
				if (lfel != NULL)
				{
					if (lfel->desc->str == "[I")
					{
						code_number(INVOKESTATIC, 1);
						code_number(70, 2);
					}
					else if (lfel->desc->str == "[F")
					{
						code_number(INVOKESTATIC, 1);
						code_number(74, 2);
					}
					else if(lfel->desc->str == "[Ljava/lang/String;")
					{
						code_number(INVOKESTATIC, 1);
						code_number(78, 2);
					}
					code_number(PUTSTATIC, 1);
					code_number(lfel->desc->id + 2, 2);
				}
				else
				{
					if (expr->valueType == Integer || expr->valueType == Boolean || expr->valueType == ENum || expr->valueType == Character)
					{
						code_number(ISTORE, 1);
					}
					else if (expr->valueType == Float)
					{
						code_number(FSTORE, 1);
					}
					else
					{
						code_number(ASTORE, 1);
					}
					el = is_in_local_vars(expr->left->list->first->name);
					code_number(el->id, 1);
				}
			}
			break;
		}
	}
	case array_call:
	{

		break;
	}
	case func:
	{
		//���� �� ������ �������� �������
		if (!isArray(current_method->name->str.c_str(), expr->call->func))
		{
			//�������� �������
			if (expr->call->type == length)
				code_number(ARRAYLENGTH, 1);
			else if (expr->call->type == first || expr->call->type == last)
			{
				bool arrayIsLocal = false;
				bool arrayIsGlobal = false;
				std::vector<struct LocalElement> *vars;
				int i;
				for (i = 0; i < table_of_classes[1].methods->size(); i++)
				{
					if (strcmp(table_of_classes[1].methods->at(i).name->str.c_str(), current_method->name->str.c_str()) == 0)
						vars = table_of_classes[1].methods->at(i).localvars;
				}
				for (i = 0; i < vars->size(); i++)
				{
					if (strcmp(expr->call->arguments->first->list->first->name, vars->at(i).name.c_str()) == 0 && NVarType::Array)
					{
						arrayIsLocal = true;
						break;
					}
				}
				if (arrayIsLocal)
				{
					if (expr->call->type == first)
						generate_expr_list_code(vars->at(i).leftSizeExpr);
					if (expr->call->type == last)
						generate_expr_list_code(vars->at(i).rightSizeExpr);
				}
				else
				{
					for (i = 0; i < table_of_classes[1].fields->size(); i++)
					{
						if (strcmp(table_of_classes[1].fields->at(i).name->str.c_str(), expr->call->arguments->first->list->first->name) == 0)
						{
							arrayIsGlobal = true;
							break;
						}
					}
				}
				if (arrayIsGlobal)
				{
					if (expr->call->type == first)
						generate_expr_list_code(table_of_classes[1].fields->at(i).leftSizeExpr);
					if (expr->call->type == last)
						generate_expr_list_code(table_of_classes[1].fields->at(i).rightSizeExpr);
				}
			}
			else
			{
				std::vector<struct LocalElement> *vars;
				int i;
				for (i = 0; i < table_of_classes[1].methods->size(); i++)
				{
					if (strcmp(table_of_classes[1].methods->at(i).name->str.c_str(), current_method->name->str.c_str()) == 0)
						break;
				}
				generate_expr_list_code(expr->call->arguments);
				if (strcmp(expr->call->func, "Float") == 0)
					code_number(I2F, 1);
				else if (strcmp(expr->call->func, "Integer") == 0)
					code_number(F2I, 1);
				else
				{
					code_number(INVOKESTATIC, 1);
					code_number(findMethodRef(expr->call->func), 2);
				}
			}
		}
		else
		{
			//��� ��� ������ �������� �������
			std::vector<struct LocalElement> *vars;
			int i;
			bool arrayIsLocal = false;
			bool arrayIsGlobal = false;
			for (i = 0; i < table_of_classes[1].methods->size(); i++)
			{
				if (strcmp(table_of_classes[1].methods->at(i).name->str.c_str(), current_method->name->str.c_str()) == 0)
					vars = table_of_classes[1].methods->at(i).localvars;
			}
			for (i = 0; i < vars->size(); i++)
			{
				if (strcmp(expr->call->func, vars->at(i).name.c_str()) == 0 && NVarType::Array)
				{
					arrayIsLocal = true;
					break;
				}
			}
			if (arrayIsLocal)
			{
				code_number(ALOAD, 1);
				code_number(vars->at(i).id, 1);
			}
			else
			{
				for (i = 0; i < table_of_classes[1].fields->size(); i++)
				{
					if (strcmp(table_of_classes[1].fields->at(i).name->str.c_str(), expr->call->func) == 0)
					{
						arrayIsGlobal = true;
						break;
					}
				}
			}
			if (arrayIsGlobal)
			{
				code_number(GETSTATIC, 1);
				code_number(table_of_classes[1].fields->at(i).desc->id + 2, 2);
			}
			generate_expr_list_code(expr->call->arguments);
			if (arrayIsGlobal)
				generate_expr_list_code(table_of_classes[1].fields->at(i).leftSizeExpr);
			if (arrayIsLocal)
				generate_expr_list_code(vars->at(i).leftSizeExpr);
			code_number(ISUB, 1);
			if (expr->valueType == Integer || expr->valueType == c_i || expr->valueType == Boolean)
				code_number(IALOAD, 1);
			if (expr->valueType == Float || expr->valueType == c_d)
				code_number(FALOAD, 1);
			if (expr->valueType == String || expr->valueType == c_s)
				code_number(AALOAD, 1);
		}
		break;
	}
	case sum:
	{
		generate_expr_code(expr->left);
		generate_expr_code(expr->right);
		if (expr->valueType == Integer || expr->valueType == c_i)
			code_number(IADD, 1);
		if (expr->valueType == Float || expr->valueType == c_d)
			code_number(FADD, 1);
		break;
	}
	case mul:
	{
		generate_expr_code(expr->left);
		generate_expr_code(expr->right);
		if (expr->valueType == Integer || expr->valueType == c_i)
			code_number(IMUL, 1);
		if (expr->valueType == Float || expr->valueType == c_d)
			code_number(FMUL, 1);
		break;
	}
	case Div:
	{
		generate_expr_code(expr->left);
		generate_expr_code(expr->right);
		if (expr->valueType == Integer || expr->valueType == c_i)
			code_number(IDIV, 1);
		if (expr->valueType == Float || expr->valueType == c_d)
			code_number(FDIV, 1);
		break;
	}
	case sub:
	{
		generate_expr_code(expr->left);
		generate_expr_code(expr->right);
		if (expr->valueType == Integer || expr->valueType == c_i)
			code_number(ISUB, 1);
		if (expr->valueType == Float || expr->valueType == c_d)
			code_number(FSUB, 1);
		break;
	}
	case grade:
	{
		generate_expr_code(expr->left);
		for (int i = 0; i < expr->right->const_int; i++)
		{
			code_number(DUP, 1);
			code_number(IMUL, 1);
		}
		break;
	}
	case not:
	{
		generate_expr_code(expr->left);
		code_number(ICONST_0, 1);
		code_number(IF_ICMPEQ, 1);
		code_number(7, 2);
		code_number(ICONST_0, 1);
		code_number(GO_TO, 1);
		code_number(4, 2);
		code_number(ICONST_1, 1);
		break;
	}
	case xor:
	{
		generate_expr_code(expr->left);
		generate_expr_code(expr->right);
		code_number(IXOR, 1);
		code_number(ICONST_1, 1);
		code_number(IF_ICMPEQ, 1);
		code_number(7, 2);
		code_number(ICONST_0, 1);
		code_number(GO_TO, 1);
		code_number(4, 2);
		code_number(ICONST_1, 1);
		break;
	}
	case and:
	{
		generate_expr_code(expr->left);
		generate_expr_code(expr->right);
		code_number(IMUL, 1);
		code_number(ICONST_1, 1);
		code_number(IF_ICMPEQ, 1);
		code_number(7, 2);
		code_number(ICONST_0, 1);
		code_number(GO_TO, 1);
		code_number(4, 2);
		code_number(ICONST_1, 1);
		break;
	}
	case or :
	{
		generate_expr_code(expr->left);
		generate_expr_code(expr->right);
		code_number(IADD, 1);
		code_number(ICONST_1, 1);
		code_number(IF_ICMPGE, 1);
		code_number(7, 2);
		code_number(ICONST_0, 1);
		code_number(GO_TO, 1);
		code_number(4, 2);
		code_number(ICONST_1, 1);
		break;
	}
	case concat:
	{

		break;
	}
	case uminus:
	{
		if (expr->valueType == Integer || expr->valueType == c_i)
		{
			code_number(BIPUSH, 1);
			code_number(0, 1);
			generate_expr_code(expr->left);
			code_number(ISUB, 1);
		}
		else
		{
			code_number(FCONST_0, 1);
			generate_expr_code(expr->left);
			code_number(FSUB, 1);
		}
		break;
	}
	case c_i:
	{
		code_number(LDC, 1);
		code_number(expr->ident, 1);
		break;
	}
	case c_d:
	{
		code_number(LDC_W, 1);
		code_number(expr->ident, 2);
		break;
	}
	case c_s:
	{
		code_number(LDC, 1);
		code_number(expr->ident, 1);
		break;
	}
	case c_c:
	{
		code_number(LDC, 1);
		code_number(expr->ident, 1);
		break;
	}
	case c_b:
	{
		if (expr->const_bool)
			code_number(ICONST_1, 1);
		else
			code_number(ICONST_0, 1);
		break;
	}
	}

	if (expr->next != NULL && !arrInitializing)
	{
		generate_expr_code(expr->next);
	}
}

void generate_expr_list_code(NExprList*list)
{
	if (list != NULL)
	{
		generate_expr_code(list->first);
	}
}

void generate_varlist_code(NVariableList*list)
{
	if (list != NULL)
	{
		generate_var_code(list->first);
	}
}

void generate_var_code(NVariable*var)
{
	if (var->value != NULL)
	{
		generate_expr_code(var->value);
		LocalElement * local = is_in_local_vars(var->var_name);
		if (local != NULL)
		{
			code_number(ASTORE, 1);
			code_number(local->id, 1);
		}
	}
	if (var->next != NULL)
	{
		generate_var_code(var->next);
	}
}

void generate_name_code(NName*name)
{
	if (name != NULL)
	{
		int i;
		for (i = 0; i < loopVarNames.size(); i++)
		{
			if (loopVarName != NULL && strcmp(name->name, loopVarNames[i]) == 0)
				break;
		}
		if (loopVarName != NULL && i < loopVarNames.size() && strcmp(name->name, loopVarNames[i]) == 0)
		{
			code_number(ILOAD, 1);
			code_number(200 + i, 1);
		}
		else
		{
			FieldElement *field_elem = NULL;
			LocalElement*elem = is_in_local_vars(name->name);
			if (elem == NULL)
				field_elem = is_in_field_vars(name->name);
			if (field_elem != NULL)
			{
				code_number(GETSTATIC, 1);
				code_number(field_elem->desc->id + 2, 2);
			}
			else if (elem->var->type == Integer || elem->var->type == Root_Integer || elem->var->type == Character)
			{
				code_number(ILOAD, 1);
				code_number(elem->id, 1);
			}
			else if (elem->var->type == Boolean)
			{
				code_number(ILOAD, 1);
				code_number(elem->id, 1);
			}
			else if (elem->var->type == Float)
			{
				code_number(FLOAD, 1);
				code_number(elem->id, 1);
			}
			else if (elem->var->type == String)
			{
				code_number(ALOAD, 1);
				code_number(elem->id, 1);
			}
			else if (elem->var->type == Array)
			{
				code_number(ALOAD, 1);
				code_number(elem->id, 1);
			}
		}
		generate_name_code(name->next);
	}
}

void generate_name_list_code(NNameList*list)
{
	if (list != NULL)
	{
		generate_name_code(list->first);
	}
}
int sizeIf;
int sizeElsif;
int sizeElse;
std::vector<int> jumpHolderPos;
std::vector<int> posTo;
int elsifCount = 0;
void generate_if_code(NIfStruct * If)
{
	union s2 temp;
	//1. ������� GenerateCodeForExpression ��� ���������� ��������� ��������� � ��������� ��� ���������� �� ���� (0 � ����); 
	generate_expr_code(If->ifExpr);
	//2. ������������� ������� ifeq ��� �������� � ������ ������� �������, ��������� ����� ���� ������� ��� ��������� ����� �������� �����;
	code_number(IFEQ, 1);
	jumpHolderPos.push_back(all_code.size());
	code_number(0, 2);
	//3. ������������� ���, ������������� ��� ������ �������, ��������� ������� GenerateCodeForBlock;  

	generate_stmt_list_code(If->ifStmt);

	generate_elsif_list_code(If->elsifList);

	for (int i = 0; i<elsifCount; i++)
		posTo.push_back(all_code.size());

	for (int i = 1; i+1 < jumpHolderPos.size(); i++)
	{
		if (i % 2 == 0)
			posTo[i - 1] = jumpHolderPos[i + 1]-1;
		else 
			posTo[i - 1] = jumpHolderPos[i]+2;
	}

	if (If->elseStmt != NULL)
	{
		//4. ���� ������������ ����� "�����", �� ������������� �������� ������������ �������� � ��������� ��� ����� ��� ����������� ��������� �����; 
		code_number(GO_TO, 1);
		jumpHolderPos.push_back(all_code.size());
		code_number(0, 2);
	}

	//5. ��������� ����� ������� �� ������ 2 �� ���������� ���;

	if (If->elseStmt == NULL && If->elsifList == NULL)
		posTo.push_back(all_code.size());
	if (If->elseStmt != NULL)
	{
		if (If->elsifList==NULL)
			posTo.push_back(all_code.size());
		//6. ���� ������������ ����� "�����", �� ������������� ��� ��� ��� ��������� GenerateCodeForBlock � ��������� ����� �������� �� ������ 4 �� ���������� ���.
		generate_stmt_list_code(If->elseStmt);
		posTo.push_back(all_code.size());

		if (If->elsifList != NULL)
		{
			posTo[posTo.size() - 2] = jumpHolderPos[jumpHolderPos.size() - 1]+2;
		}
	}

	//if (If->elsifList != NULL  && If->elseStmt == NULL)
		//posTo[posTo.size() - 3] = posTo[posTo.size() - 1];

	for (int i = 0; i < jumpHolderPos.size(); i++)
	{
		temp = make_reversed_s2(posTo[i] + 1 - jumpHolderPos[i]);
		for (int j = 0; j < 2; j++)
			all_code.at(jumpHolderPos[i] + j) = temp.bytes[j];
	}
}

void generate_elsif_list_code(NElsifList*list)
{
	if (list != NULL)
		generate_elsif_code(list->first);
}
bool newElsif = false;
void generate_elsif_code(NElsif*elsif)
{
	if (elsif != NULL)
	{
		elsifCount++;
		code_number(GO_TO, 1);
		jumpHolderPos.push_back(all_code.size());
		code_number(0, 2);
		posTo.push_back(all_code.size());
		generate_expr_code(elsif->elsifExpr);
		code_number(IFEQ, 1);
		jumpHolderPos.push_back(all_code.size());
		code_number(0, 2);
		generate_stmt_list_code(elsif->body);
		if (!newElsif)
			posTo.push_back(all_code.size());

		if (elsif->next != NULL)
		{
			newElsif = true;
			generate_elsif_code(elsif->next);
		}
		newElsif = false;
	}
}
void generate_stmt_list_code(NStatementList*list)
{
	if (list != NULL)
		generate_stmt_code(list->first);
}

void generate_stmt_code(NStatement*stmt)
{
	switch (stmt->type)
	{
	case array_var:
	{
		std::vector<struct LocalElement> *vars;
		int arrId;
		NExpression *arrExpr;
		if (stmt->arr_var->expression != NULL)
			arrExpr = stmt->arr_var->expression->first;
		for (arrId = 0; arrId < table_of_classes[1].methods->size(); arrId++)
		{
			if (strcmp(table_of_classes[1].methods->at(arrId).name->str.c_str(), current_method->name->str.c_str()) == 0)
				vars = table_of_classes[1].methods->at(arrId).localvars;
		}
		for (arrId = 0; arrId < vars->size(); arrId++)
		{
			if (strcmp(stmt->arr_var->arr_name, vars->at(arrId).name.c_str()) == 0 && NVarType::Array)
				break;
		}
		bool arrayIsLocal = false;
		bool arrayIsGlobal = false;
		int i;
		for (i = 0; i < table_of_classes[1].methods->size(); i++)
		{
			if (strcmp(table_of_classes[1].methods->at(i).name->str.c_str(), current_method->name->str.c_str()) == 0)
				vars = table_of_classes[1].methods->at(i).localvars;
		}
		for (i = 0; i < vars->size(); i++)
		{
			if (strcmp(stmt->arr_var->arr_name, vars->at(i).name.c_str()) == 0 && NVarType::Array)
			{
				arrayIsLocal = true;
				arrId = i;
				break;
			}
		}
		if (!arrayIsLocal)
		{
			for (i = 0; i < table_of_classes[1].fields->size(); i++)
			{
				if (strcmp(table_of_classes[1].fields->at(i).name->str.c_str(), stmt->arr_var->arr_name) == 0)
				{
					arrayIsGlobal = true;
					arrId = table_of_classes[1].fields->at(i).desc->id;
					break;
				}
			}
		}
		generate_expr_list_code(stmt->arr_var->rightSizeExpr);
		generate_expr_list_code(stmt->arr_var->leftSizeExpr);
		code_number(ISUB, 1);
		code_number(BIPUSH, 1);
		code_number(1, 1);
		code_number(IADD, 1);
		if (stmt->arr_var->elem_type != String)
		{
			code_number(NEWARRAY, 1);
			if (stmt->arr_var->elem_type == Integer || stmt->arr_var->elem_type == Root_Integer || stmt->arr_var->elem_type == Boolean)
				code_number(10, 1);
			else if (stmt->arr_var->elem_type == Float)
				code_number(6, 1);
			else if (stmt->arr_var->elem_type == Character)
				code_number(5, 1);
		}
		else
		{
			code_number(ANEWARRAY, 1);
			code_signed_number(4, 2);
		}
		if (arrayIsLocal)
		{
			code_number(ASTORE, 1);
			code_number(vars->at(arrId).id, 1);
		}
		else
		{
			code_number(PUTSTATIC, 1);
			code_number(arrId + 2, 2);
		}
		//���� ������� ������������� ��������
		if (stmt->arr_var->expression != NULL && arrayIsLocal)
		{
			arrInitializing = true;
			for (int i = 0; i <= stmt->arr_var->rightSize - stmt->arr_var->leftSize; i++)
			{
				if (arrExpr == NULL)
					throw ("array size is bigger than initializing pool!");
				code_number(ALOAD, 1);
				code_number(vars->at(arrId).id, 1);
				code_number(BIPUSH, 1);
				code_number(i, 1);
				generate_expr_code(arrExpr);
				if (stmt->arr_var->elem_type == Integer || stmt->arr_var->elem_type == Root_Integer || stmt->arr_var->elem_type == Character)
					code_number(IASTORE, 1);
				else if (stmt->arr_var->elem_type == Float)
					code_number(FASTORE, 1);
				else if (stmt->arr_var->elem_type == String)
					code_number(AASTORE, 1);
				if (arrExpr->next != NULL && i == stmt->arr_var->rightSize - stmt->arr_var->leftSize)
					throw ("array size is lower than initializing pool!");
				arrExpr = arrExpr->next;
			}
			arrInitializing = false;
		}
		else if (stmt->arr_var->expression != NULL && arrayIsGlobal)
		{
			arrInitializing = true;
			for (int i = 0; i <= stmt->arr_var->rightSize - stmt->arr_var->leftSize; i++)
			{
				if (arrExpr == NULL)
					throw ("array size is bigger than initializing pool!");
				code_number(GETSTATIC, 1);
				code_number(arrId + 2, 2);
				code_number(BIPUSH, 1);
				code_number(i, 1);
				generate_expr_code(arrExpr);
				if (stmt->arr_var->elem_type == Integer || stmt->arr_var->elem_type == Root_Integer || stmt->arr_var->elem_type == Character)
					code_number(IASTORE, 1);
				else if (stmt->arr_var->elem_type == Float)
					code_number(FASTORE, 1);
				else if (stmt->arr_var->elem_type == Boolean)
					code_number(IASTORE, 1);
				else if (stmt->arr_var->elem_type == String)
					code_number(AASTORE, 1);
				if (arrExpr->next != NULL && i == stmt->arr_var->rightSize - stmt->arr_var->leftSize)
					throw ("array size is lower than initializing pool!");
				arrExpr = arrExpr->next;
			}
			arrInitializing = false;
		}
		break;
	}
	case array_assign:
	{
		int x = 1;
		break;
	}
	case var_list_assign:
	{
		generate_varlist_assign_code(stmt->list);
		break;
	}
	case expr:
	{
		generate_expr_list_code(stmt->expressions);
		break;
	}

	case func_call:
	{
		break;
	}
	case if_:
	{
		generate_if_code(stmt->if_struct);
		break;
	}
	case case_:
	{
		break;
	}
	case for_:
	{
		generate_for_code(stmt->for_loop);
		break;
	}
	case while_:
	{
		generate_while_code(stmt->while_loop);
		break;
	}
	case func_pr:
	{
		for (char el : all_code)
			code_of_methods[methodCounter].push_back(el);
		all_code.clear();
		generate_func_proc_code(stmt->func_proc);
		break;
	}
	case en:
	{

		break;
	}
	case return_:
	{
		generate_expr_code(stmt->return_st->returnValue);
		if (stmt->return_st->returnValue->valueType == Integer || stmt->return_st->returnValue->valueType == Character || stmt->return_st->returnValue->valueType == Boolean)
			code_number(IRETURN, 1);
		else if(stmt->return_st->returnValue->valueType == Float)
			code_number(FRETURN, 1);
		else	
		code_number(ARETURN, 1);
		returnCreated = true;
	}
	}
	if (stmt->next != NULL)
	{
		generate_stmt_code(stmt->next);
	}
}

void generate_varlist_assign_code(NVarListAssign * assign)
{
	NVariable*var = assign->variables->first;
	while (var != NULL)
	{
		LocalElement *el = NULL;
		FieldElement *fel = NULL;
		el = is_in_local_vars(var->var_name);
		if (el == NULL)
			fel = is_in_field_vars(var->var_name);
		if (fel != NULL && var->value != NULL)
		{
			generate_expr_code(var->value);
			code_number(PUTSTATIC, 1);
			code_number(fel->desc->id + 2, 2);
		}
		else if (var->type == Integer && var->value != NULL)
		{
			generate_expr_code(var->value);
			code_number(ISTORE, 1);
			code_number(ISTORE, 1);
			code_number(el->id, 1);
		}
		else if ((var->type == Float) && var->value != NULL)
		{
			generate_expr_code(var->value);
			code_number(FSTORE, 1);
			code_number(is_in_local_vars(var->var_name)->id, 1);
		}
		else if (var->type == Boolean&& var->value != NULL)
		{
			generate_expr_code(var->value);
			code_number(ISTORE, 1);
			el = is_in_local_vars(var->var_name);
			code_number(el->id, 1);
		}
		else if (var->type == Character&& var->value != NULL)
		{
			generate_expr_code(var->value);
			code_number(ISTORE, 1);
			el = is_in_local_vars(var->var_name);
			code_number(el->id, 1);
		}
		else if (var->type == String&& var->value != NULL)
		{
			generate_expr_code(var->value);
			code_number(ASTORE, 1);
			el = is_in_local_vars(var->var_name);
			code_number(el->id, 1);
		}
		else if (var->type == Array&& var->value != NULL)
		{
		}
		var = var->next;
	}
}

void generate_while_code(NWhileLoop * While)
{
	int size;	//����� ������������ ��������
	int size2;
	int startCondition;
	union s2 temp;
	//1. ������������� ��� ��� �������� ������������ �������� � ��������� �� ����� ��� ���������� ����� �������� � ����������;
	if (!While->doWhile)
	{
		code_number(GO_TO, 1);
		code_number(0, 2);
	}
	size = all_code.size();
	//2. ������������� ��� ���� �����, ��������� GenerateCodeForBlock;
	generate_stmt_list_code(While->body);
	size2 = all_code.size();

	//3. ������������� ��� ���������� ��������� ���������, ��������� GenerateCodeForExpression, 
	generate_expr_code(While->condition);

	//-----��������� ������� � ������ ���, ����� �� �������� �� ������ ���������� ���������;
	if (!While->doWhile)
	{
		temp = make_reversed_s2(size2 - size + 3);
		for (int i = 0; i < 2; i++)
			all_code.at(size - 2 + i) = temp.bytes[i];
	}
	//4. ������������� ��� �������� ifne ��� �������� � ������ ���� �����.
	if (!While->doWhile)
	{
		code_number(IFNE, 1);
		temp = make_reversed_s2(size + 1 - all_code.size());
		for (int i = 0; i < 2; i++)
			all_code.push_back(temp.bytes[i]);
	}
	else
	{
		code_number(IFEQ, 1);
		temp = make_reversed_s2(size + 1 - all_code.size());
		for (int i = 0; i < 2; i++)
			all_code.push_back(temp.bytes[i]);
	}

}
void generate_for_code(NForLoop* For)
{
	loopVarName = (char*)malloc(sizeof(char) * strlen(For->condition->first->name) + 1);
	loopVarName = For->condition->first->name;
	loopVarNames.push_back(loopVarName);
	loopCounter++;
	int size;	//����� ������������ ��������
	int size2;
	int startCondition;
	union s2 temp;
	if (For->inReverse == false)
	{
		//������ � ���������� ����� ����� �������
		generate_expr_code(For->begin);
		code_number(ISTORE, 1);
		code_number(200 + loopCounter, 1);
		// ������������� ��� ��� �������� ������������ �������� � ��������� �� ����� ��� ���������� ����� �������� � ����������;
		code_number(GO_TO, 1);
		code_number(0, 2);
		size = all_code.size();

		//������������� ��� ���� �����, ��������� GenerateCodeForBlock;
		generate_stmt_list_code(For->body);
		code_number(ILOAD, 1);
		code_number(200 + loopCounter, 1);
		code_number(ICONST_1, 1);
		code_number(IADD, 1);
		code_number(ISTORE, 1);
		code_number(200 + loopCounter, 1);

		size2 = all_code.size();

		//3. ������������� ��� ���������� ��������� ���������

		code_number(ILOAD, 1);
		code_number(200 + loopCounter, 1);
		generate_expr_code(For->end);
		code_number(IF_ICMPLE, 1);
		code_number(7, 2);
		code_number(ICONST_0, 1);
		code_number(GO_TO, 1);
		code_number(4, 2);
		code_number(ICONST_1, 1);

		//-----��������� ������� � ������ ���, ����� �� �������� �� ������ ���������� ���������;
		temp = make_reversed_s2(size2 - size + 3);
		for (int i = 0; i < 2; i++)
			all_code.at(size - 2 + i) = temp.bytes[i];



		//4. ������������� ��� �������� ifne ��� �������� � ������ ���� �����.
		code_number(IFNE, 1);
		temp = make_reversed_s2(size + 1 - all_code.size());
		for (int i = 0; i < 2; i++)
			all_code.push_back(temp.bytes[i]);
	}
	else
	{
		//������ � ���������� ����� ������ �������
		generate_expr_code(For->end);
		code_number(ISTORE, 1);
		code_number(200 + loopCounter, 1);
		// ������������� ��� ��� �������� ������������ �������� � ��������� �� ����� ��� ���������� ����� �������� � ����������;
		code_number(GO_TO, 1);
		code_number(0, 2);
		size = all_code.size();

		//������������� ��� ���� �����, ��������� GenerateCodeForBlock;
		generate_stmt_list_code(For->body);
		code_number(ILOAD, 1);
		code_number(200 + loopCounter, 1);
		code_number(ICONST_1, 1);
		code_number(ISUB, 1);
		code_number(ISTORE, 1);
		code_number(200 + loopCounter, 1);

		size2 = all_code.size();

		//3. ������������� ��� ���������� ��������� ���������

		code_number(ILOAD, 1);
		code_number(200 + loopCounter, 1);
		generate_expr_code(For->begin);
		code_number(IF_ICMPGE, 1);
		code_number(7, 2);
		code_number(ICONST_0, 1);
		code_number(GO_TO, 1);
		code_number(4, 2);
		code_number(ICONST_1, 1);

		//-----��������� ������� � ������ ���, ����� �� �������� �� ������ ���������� ���������;
		temp = make_reversed_s2(size2 - size + 3);
		for (int i = 0; i < 2; i++)
			all_code.at(size - 2 + i) = temp.bytes[i];

		//4. ������������� ��� �������� ifne ��� �������� � ������ ���� �����.
		code_number(IFNE, 1);
		temp = make_reversed_s2(size + 1 - all_code.size());
		for (int i = 0; i < 2; i++)
			all_code.push_back(temp.bytes[i]);
	}
	loopVarName = NULL;
	if (loopVarNames.size() > 0)
	{
		loopVarName = (char*)malloc(sizeof(char)* strlen(loopVarNames.at(loopVarNames.size() - 1) + 1));
		strcpy(loopVarName, loopVarNames.at(loopVarNames.size() - 1));
	}
	loopVarNames.erase(loopVarNames.begin() + loopVarNames.size() - 1);
	loopCounter--;
}


void generate_arr_var_code(NArrayVar * var)
{
	if (var != NULL)
	{
		generate_expr_list_code(var->expression);
		LocalElement * local = is_in_local_vars(var->arr_name);
		if (local != NULL)
		{
			code_number(ASTORE, 1);
			code_number(local->id, 1);
		}
	}
}
std::vector<char> empty_code;
void generate_func_proc_code(NFuncProc*func)
{
	code_of_methods.insert(code_of_methods.end(), empty_code);
	if (methodCounter >= 0)
	{
		for (char el : all_code)
			code_of_methods[methodCounter].push_back(el);
		all_code.clear();
	}
	methodCounter++;
	method1 = current_method;
	current_method = is_in_methods(func->name);

	generate_stmt_list_code(func->localVars);
	if (func->name == "main")
		methodCounter = 0;
	generate_stmt_list_code(func->body);
	if (func->isFunc && !returnCreated)
	{
		throw("return statement should be in all functions");
	}
	returnCreated = false;
	if (!func->isFunc)
		code_number(_RETURN, 1);
	for (char el : all_code)
		code_of_methods[methodCounter].push_back(el);
	all_code.clear();
	current_method = method1;
}

void generate_enum_code(NVariable * var)
{
	generate_var_code(var);
	generate_name_list_code(var->list);
}

void generate_byte_code(std::vector<struct SemanticalElement*>& constT, std::vector<struct ClassElement>& classT, std::string file)
{
	table_of_const = constT;
	table_of_classes = classT;
	file_name = file;
	generate_stmt_list_code(Root);
	generate_class_file();
}

void generate_class_file()
{
	all_code.clear();
	char file_name[20];
	unsigned long int number;
	strcpy(file_name, "Main_Class");
	strcat(file_name, ".class");
	file_of_class = std::ofstream(file_name, std::ios::out | std::ios::binary);
	//����������� ��������� �����
	number = 0xCAFEBABE;
	code_number(number, 4);
	number = 0;
	code_number(number, 2);
	number = 52;
	code_number(number, 2);
	number = table_of_const.size() + 1;
	code_number(number, 2);
	code_const_table();
	number = 0x01;
	code_number(number, 2);
	number = parent_class->classname->id;
	code_number(number, 2);
	//�������� Object
	number = parent_class->parentname->id;
	code_number(number, 2);
	//����������
	number = 0;
	code_number(number, 2);
	//���� ������
	number = parent_class->fields->size();;
	code_number(number, 2);
	code_field_table();
	//������ ������
	number = parent_class->methods->size();
	code_number(number, 2);
	code_method_table();
	// �������� � ����
	number = 0;
	code_number(number, 2);
	write_byte_code(all_code);
	file_of_class.close();
}

void write_byte_code(std::vector<char> & code)
{
	std::ofstream myFile("Main_Class.class", std::ios::out | std::ios::binary);

	if (code.size() != 0)
	{
		file_of_class.write(&code[0], code.size());
	}
}