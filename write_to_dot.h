#include <stdio.h>
#include <stdlib.h>
#include "tree_structures.h"

void write_tree(NStatementList*Root);

char* write_type(enum NVarType type);

void write_var(struct NVariable*var);

void write_array_var(struct NArrayVar*arr);

void write_expr_list(struct NExprList*list);

void write_exception(struct NException * exc);

void write_exc_body(struct NExcBody * body);

void write_if(struct NIfStruct* if_);

void write_elsif_list(struct NElsifList* list);

void write_elsif(struct NElsif*elsif);

void write_when(struct NWhenStruct* when_);

void write_when_list(struct NWhenList*list);

void write_case(struct NCaseStruct* case_);

void write_varlist_assign(struct NVarListAssign*assign);

void write_func_proc(struct NFuncProc* func_proc);

void write_args(struct NFuncArgs*args);

void write_varlist(struct NVariableList*list);

void write_arr_assign(struct NArrayAssign*assign);

void write_func_call(struct NFuncCall*call);

void write_expr(struct NExpression*expr, char*nameExprList);

void write_stmt_list(struct NStatementList*list);

void write_stmt(struct NStatement*stmt);

void write_while(struct NWhileLoop*loop);

void write_for(struct NForLoop*for_);

void write_names(struct NNameList*list);

void write_name(struct NName*name);