#ifndef TREE_STRUCTS
#define TREE_STRUCTS




enum NExprType{name_list,brackets,  assign, func, array_call, sum, mul,Div,sub,grade,greater, equal, less,not,ne,ge,le,xor,and,or,concat,uminus,c_i,c_d,c_s,c_c,c_b,empty};
enum NVarType{Integer, Root_Integer,Character, Boolean, String,Record,ENum,Function,Procedure, Float, Array, Void, FloatArray, StringArray, CharacterArray, BooleanArray, IntegerArray};
enum NStmtType{array_var,variable,array_assign,var_list_assign,expr,func_call,if_, case_, for_, while_, func_pr,en,exception, return_};
enum NFuncType {func_proc,length,first,last,a_call};
//��������� ��� ����������
struct NVariable
{
	struct NVariable * next;//��������� �� ���� ������� ������, ���� ���������� ����� � ������
	struct NVariable * prev; //��������� �� ���������� ������� ������
	char*var_name;
	enum NVarType type;
	struct NExpression * value;
	struct NNameList * list; //��� ������������� ����
	int isConstant;
};

struct NNameList
{
	int number;
	struct NName * first;
	struct NName * last;
};

struct NName
{
	struct NNameList * SList;
	char* name;
	struct NName * next;
	enum NVarType type;
	struct NVariable * var;
};


struct NArrayVar
{
	char*arr_name;
	enum NVarType elem_type;
	int leftSize;
	int rightSize;
	struct NExprList * leftSizeExpr;
	struct NExprList * rightSizeExpr;
	struct NExprList * expression;

};

struct NExprList
{
	struct NExpression * first;
	struct NExpression * last;
};

struct NException
{
	struct NExcBodyList*list;
};

struct NExcBodyList
{
	struct NExcBody * first;
	struct NExcBody * last;
};

struct NExcBody
{
	char* name;
	struct NExcBody * next;
	struct NStatementList*body;
};

struct NIfStruct
{
	struct NStatementList * ifStmt;
	struct NExpression * ifExpr;
	struct NElsifList * elsifList;
	struct NStatementList * elseStmt;
};

struct NElsifList
{
	struct NElsif * first;
	struct NElsif * last;
};

struct NElsif
{
	struct NElsif * next;
	struct NExpression * elsifExpr;
	struct NStatementList * body;
};

struct NWhenStruct
{	
	struct NWhenStruct * next;
	struct NExprList * whenExpr;
	struct NStatementList * body;
};

struct NWhenList
{
	struct NWhenStruct * first;
	struct NWhenStruct * last;
};

struct NCaseStruct
{
	struct NNameList * caseVar;
	struct NWhenList * body;
};

struct NVarListAssign
{
	struct NVariableList* variables;
	struct NExpression*value;
	struct NVarListAssign * next;
};

struct NFuncProc
{
	int isFunc;
	char * name;
	int isPrototype;
	enum NVarType type;
	struct NFuncArgs * arguments;
	struct NStatementList * body;
	struct NStatementList * localVars;
	struct NExpression * returnValue;
};

struct NFuncArgs
{
	struct NVarListAssign*first;
	struct NVarListAssign*last;
};

//��������� ��� ������ ����������
struct NVariableList
{
	struct NVariable * first;
	struct NVariable * last;
};

struct NArrayAssign
{
	char*left;
	char*right;
	struct NExprList*expr;
	int from_left;
	int to_left;
	int from_right;
	int to_right;
};

struct NFuncCall
{
	char * func;
	enum NFuncType type;
	struct NExprList * arguments;
};


//��������� ��� ���������
struct NExpression
{
	struct NNameList * list;
	struct NExpression * next;
	enum NExprType type;
	int ident;
	struct NFuncCall * call;
	enum NVarType valueType;
	int const_int;
	double const_double;
	char* const_string;
	int const_bool;
	char const_char;
	struct NExpression * left;
	struct NExpression * right;
	struct NExpression *  index; //��� �������
};

//��������� ��� ����� statement
struct NStatementList 
{
	int number;
	struct NStatement * first;
	struct NStatement * last;
};

//��������� ��� statement
struct NStatement
{
	struct NStatementList * Slist;
	struct NStatement * next;
	enum NStmtType type;
	struct NExprList * expressions;
	struct NVariable * enu;
	struct NArrayVar * arr_var;
	struct NVarListAssign * list;
	struct NArrayAssign * array_assign;
	struct NIfStruct * if_struct;
	struct NFuncProc * func_proc;
	struct NFuncCall * func_call;
	struct NWhileLoop * while_loop;
	struct NForLoop * for_loop;
	struct NCaseStruct * case_st;
	struct NException* except;
	struct NReturn* return_st;
};

struct NReturn {
	struct NExpression * returnValue;
};

//��������� ��� ����� while
struct NWhileLoop
{
	int doWhile;
	struct NExpression * condition;
	struct NStatementList * body;
};

//��������� ��� ����� for
struct NForLoop
{
	int inReverse;
	struct NExpression* begin;
	struct NExpression* end;
	struct NNameList * condition;
	struct NStatementList * body;
};

struct NProgram
{
	struct NStatementList *stmt;
};

#endif