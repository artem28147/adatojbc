#include "stdafx.h"
#include "parser_tab.h"
#include "write_to_dot.h"
#include "SemanticalFunctions.h"
#include "code_generation.h"
#include <conio.h>
#include <process.h>

extern "C" struct NStatementList *Root;
extern "C"  YYSTYPE yylval;
extern "C" int yyparse();
extern "C" int yylex();
extern "C" FILE *yyin, *yyout;
int main(int argc, char *argv[])
{
	unsigned int size;
	yyin = fopen(argv[1], "r");
	yyparse();
	try
	{
		fill();
		semantics();
		generate_byte_code(table_of_const, table_of_classes, std::string(argv[1]));
	}
	catch (char* error)
	{
		printf("%s", error);
		getch();
		fclose(yyin);
		return 0;
	}
	print_table_of_consts();
	//write_tree(Root);
	fclose(yyin);
	//_spawnl(_P_WAIT,"graphviz\\bin\\dot.exe","dot.exe","-Tpng","-o","tree.png","tree.gv",NULL);
	return 0;
}

