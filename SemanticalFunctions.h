#include "SemanticalElements.h"
#include "tree_structures.h"
#include "write_to_dot.h"

extern "C" struct NStatementList *Root;

extern std::vector<struct SemanticalElement*> table_of_const;
extern std::vector<struct ClassElement> table_of_classes;
extern struct MethodElement* current_method;
extern struct ClassElement* parent_class;
extern struct MethodElement* method1;

char * translate_descriptor(enum SemanticalConst type);
char * translate_value_to_str(struct SemanticalElement * element);

void fill()throw (char*);
void semantics()throw(char *);


void print_table_of_consts();
void print_table_of_classes();
void print_table_of_methods(std::vector<struct MethodElement> * methodtable, std::string name);
void print_table_of_field_vars(std::vector<struct FieldElement> * fieldtable, std::string name);
void print_table_of_local_vars(std::vector<struct LocalElement>* localvars, std::string name);

struct SemanticalElement* addUtf8ToConstTable (std::string name);
struct SemanticalElement* addIntToConstTable (int name);
struct SemanticalElement* addFloatToConstTable (float name);
struct SemanticalElement* addNameTypeToConstTable (std::string name, std::string desc);
struct SemanticalElement* addMethodToConstTable (struct SemanticalElement* classname, std::string name, std::string desc) throw(char*);
struct SemanticalElement* addFieldToTableConst (struct SemanticalElement* classname, std::string name, std::string desc) throw(char*);
struct SemanticalElement* addNameTypeToConstTable (std::string name, std::string desc);
struct SemanticalElement* addStringToConstTable(std::string name);

void add_var_to_table (struct NVariable* var);
void add_to_local_table(struct NVariable* var) throw (char*);
void add_to_class_table(struct ClassElement* added) throw(char*);
void add_to_field_table(struct FieldElement* element) throw (char*);
void add_to_method_table (struct MethodElement* element) throw (char*);


void first_sem_stmt_list(struct NStatementList* list);
void first_sem_stmt(struct NStatement * stmt);


void stmt_sem(struct NStatement * stmt);
void stmt_list_sem(struct NStatementList* list);
void var_sem(struct NVariable * var);
void expr_list_sem(struct NExprList * list);
void expr_sem(struct NExpression * expr)throw(char *);
void while_sem(struct NWhileLoop* loop);
void if_sem(struct NIfStruct * If);
void elsif_list_sem(struct NElsifList * list);
void elsif_sem(struct NElsif *elsif);
void when_sem(struct NWhenStruct * whenS);
void when_list_sem(struct NWhenList * list);
void name_sem(struct NName*name)throw(char *);
void name_list_sem(struct NNameList * list);
void func_sem(struct NFuncProc * func);
void for_sem(struct NForLoop*loop);
bool match_types(struct NExpression * expr);
char * make_desc_from_args(struct NFuncCall * args, char * Name) throw(char*);
char* sem_type(enum NVarType type);
struct LocalElement* is_in_local_vars(char*name);
struct MethodElement* is_in_methods(char*name);
struct FieldElement* is_in_field_vars(char*name);
bool variable_exists(char*name);
enum NVarType return_type(struct NName*name);
void types_matches( struct NName* first, struct NName* second)throw(char *);
void operation_types_matches(struct NExpression* expr) throw(char*);
enum NVarType make_type_from_description(struct SemanticalElement * element);
char* make_descriptor_for_function(struct NFuncArgs * arguments, enum NVarType type);

//������� ��� ���������� ������
void fill_name_list_table (struct NName * list);
void fill_name_table(struct NName* added)throw(char*);
void fill_var_table(struct NVariable* var, bool isLocal);
void fill_var_list_table(struct NVariableList * list,bool isLocal);
void fill_expr_table (struct NExpression* expr) throw(char *);
void fill_expr_list_table (struct NExprList* list) throw(char *);
void fill_stmt_table(struct NStatement * stmt);
void fill_stmt_list_table(struct NStatementList * list);
void fill_if_table(struct NIfStruct*If);
void fill_elsif_list_table(struct NElsifList*list);
void fill_elsif_table(struct NElsif * elsif);
void fill_for_table(struct NForLoop* for_)throw(char*);
void fill_while_table(struct NWhileLoop* while_);
void fill_array_var_table(struct NArrayVar * variable)throw (char*);
void fill_array_assign_table(struct NArrayAssign * assign);
void fill_func_call_table(struct NFuncCall * call)throw (char*);
void fill_func_proc_table(struct NFuncProc * func_proc);
void fill_func_args_table(struct NFuncArgs * args);
void fill_case_table(struct NCaseStruct * Case);
void fill_when_list_table(struct NWhenList* list);
void fill_when_table(struct NWhenStruct * When);
void fill_enum_table(struct NVariable * var);

unsigned long int findMethodRef(char * name);
unsigned long int findType(SemanticalConst type);
bool isArray(const char * method, char * name);
NVarType getArrayType(const char * method, char * name);