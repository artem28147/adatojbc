typedef union{
	int ConstInt;
	double ConstDouble;
	char* ConstString;
	char * VarName;
	char ConstChar;
	int ConstBool;
	struct NStatement * Statement;
	struct NStatementList * StatementList;
	struct NExpression * Expression;
	struct NException * Exception;
	struct NNameList * NameList;
	struct NName * Name;
	struct NExcBody * ExcBody;
	struct NExcBodyList* BodyList;
	struct NFuncArgs * FuncArgs;
	struct NExprList *ExprList;
	struct NArrayVar *ArrayVar;
	struct NFuncProc * FuncProc;
	struct NArrayAssign * ArrayAssign;
	struct NVarListAssign * VarListAssign;
	struct NWhenList * WhenList;
	struct NElsifList * ElsifList;
	struct NVariable * Variable;
	struct NVariable * ENum;
	struct NForLoop *ForLoop;
	struct NFuncCall * FuncCall;
	struct NVariableList * VariableList;
	enum NVarType VarType;
	enum NArrType ArrType;
	struct NIfStruct * IfStruct;
	struct NElsif * Elseif;
	struct NWhenStruct * WhenStruct;
	struct NCaseStruct * CaseStruct;
	struct NWhileLoop * WhileLoop;
	struct NStatementList *Program;
} YYSTYPE;
#define	CONST_STRING	258
#define	CONST_INT	259
#define	CONST_BOOL	260
#define	CONST_DOUBLE	261
#define	CONST_CHAR	262
#define	NAME	263
#define	ABORT	264
#define	ABS	265
#define	ABSTRACT	266
#define	ACCEPT	267
#define	ALIASED	268
#define	ALL	269
#define	AND	270
#define	AT	271
#define	BEGGIN	272
#define	BODY	273
#define	CASE	274
#define	CONSTANT	275
#define	DECLARE	276
#define	DELAY	277
#define	DELTA	278
#define	DIGITS	279
#define	DO	280
#define	ELSE	281
#define	ELSIF	282
#define	END	283
#define	ENTRY	284
#define	EXCEPTION	285
#define	EXIT	286
#define	FOR	287
#define	FUNCTION	288
#define	GENERIC	289
#define	IF	290
#define	IN	291
#define	IS	292
#define	LIMITED	293
#define	LOOP	294
#define	MOD	295
#define	NEW	296
#define	NOT	297
#define	NOLL	298
#define	OF	299
#define	OR	300
#define	OTHERS	301
#define	OUT	302
#define	PRAGMA	303
#define	PRIVATE	304
#define	PROCEDURE	305
#define	FIRST	306
#define	LAST	307
#define	LENGTH	308
#define	PROTECTED	309
#define	RAISE	310
#define	RANGE	311
#define	RENAMES	312
#define	REQUEUE	313
#define	RETURN	314
#define	REVERSE	315
#define	SELECT	316
#define	SEPARATE	317
#define	SUBTYPE	318
#define	TERMINATE	319
#define	THEN	320
#define	TYPE	321
#define	UNTIL	322
#define	USE	323
#define	WHEN	324
#define	WHILE	325
#define	XOR	326
#define	GE	327
#define	LE	328
#define	NE	329
#define	ARRAYRANGE	330
#define	ASSIGN	331
#define	ENSUE	332
#define	GRADE	333
#define	REALTYPE	334
#define	ROOTINTTYPE	335
#define	INTTYPE	336
#define	CHARTYPE	337
#define	BOOLTYPE	338
#define	ENUMTYPE	339
#define	ARRAYTYPE	340
#define	FLOATTYPE	341
#define	STRINGTYPE	342
#define	RECORDTYPE	343
#define	FUNCTIONTYPE	344
#define	UMINUS	345


extern YYSTYPE yylval;
